﻿
go
create database OnlineLearning
go

go
use OnlineLearning
go


create table Role(
	role_id int primary key identity(1,1),
	role_name varchar(100) not null,
)

create table Account(
	account_id int primary key identity(1,1),
	role_id int foreign key references Role(role_id),
	username varchar (300) not null unique,
	password varchar (300) not null,
	email varchar (255) not null unique,
	date_register date default GETDATE(),
	account_status bit default(1),
)

create table Country(
	country_id int primary key identity(1,1),
	country_name nvarchar(200),
) 

create table Information(
	infor_id int not null unique,
	country_id int FOREIGN KEY REFERENCES Country(country_id),	
	display_name nvarchar(255),
	full_name nvarchar (255),
	address nvarchar (255),
	phone int,
	gender nvarchar(100),
	DOB date,
	infor_image nvarchar(255),
)

ALTER TABLE Information
ADD CONSTRAINT FK_information_Account FOREIGN KEY(infor_id) 
    REFERENCES Account(account_id);


create table Slide (
	slide_id int primary key identity (1,1),
	date_create_slide  date default GETDATE(),
	slide_title nvarchar (300) not null,
	slide_description nvarchar (300) not null,
	slide_status bit default(1),
)

create table ImgOfSlide(
	id int primary key identity(1,1),
	slide_id int foreign key references Slide(slide_id),
	img nvarchar(max) not null,
	img_status bit default(1),
)
    

create table Blogs(
	id int primary key identity(1,1),
	account_id int foreign key references Account(account_id),
	title nvarchar(300) not null,
	detail nvarchar(max) not null,
	category nvarchar(300) not null,
	thumbnail nvarchar(300) not null,
	date_create_blog  date default GETDATE(),
	blog_status bit default(1),
)

create table Post(
	id int primary key identity(1,1),
	account_id int foreign key references Account(account_id),
	title nvarchar(300) not null,
	detail nvarchar(max) not null,
	category nvarchar(300) not null,
	thumbnail nvarchar(300) not null,
	date_create_post  date default GETDATE(),
	post_status bit default(1),
)


create table Course(
	course_id int primary key identity(1,1),
	course_title nvarchar(200) not null,
	course_description nvarchar(max) not null,
	price int not null,
	course_image nvarchar(max) not null,
	date_create_course  date default GETDATE(),
	course_status bit default(0),
)
create table Requirement(
	course_id int foreign key references Course(course_id),
	content nvarchar(max),
)

create table Account_Course(
	account_id int foreign key references Account(account_id),
	course_id int foreign key references Course(course_id),
	lesson_unlock int default(1),
	dateCreate date default GETDATE()
)

create table lesson(
	lesson_id int primary key identity(1,1),
	course_id int foreign key references course(course_id),	
	lesson_unit int not null, 
	lesson_title nvarchar(300) not null,
	lesson_description nvarchar(max) not null,
	lesson_content nvarchar(max) not null,
	lesson_time nvarchar(100) ,
	lesson_video nvarchar(max) not null,
	date_create_lesson  date default GETDATE(),
	lesson_status bit default(0),
)

create table [Subject] (
	id int primary key identity(1,1),
	name nvarchar(max)
)

create table QuizLevel(
	[level] int primary key identity(1,1),
	name nvarchar(32) not null
)

create table QuizType(
	[type] int primary key identity(1,1),
	name nvarchar(128) not null
)



create table Quiz(
	id int primary key identity(1,1),
	[subject] int foreign key references [Subject](id),
	title varchar(max) not null,
	[level] int foreign key references QuizLevel([level]),
	content varchar(max) not null,
	[length] int, --giây
	passRate float not null default(0.5),
	max_point float default(10),
	[type] int foreign key references QuizType([type])
)


create table Quiz_Lesson(
	lessonId int foreign key references lesson(lesson_id),
	quizId int foreign key references Quiz(id)
)

create table Question(
	id int primary key identity(1,1),
	content nvarchar(max),
	explanation nvarchar(max),
	[subject] int foreign key references [Subject](id)
)

create table Question_Lesson(
	questionId int foreign key references Question(id),
	lessonId int foreign key references lesson(lesson_id)
)


create table QuestionMedia(
	questionId int foreign key references Question(id),
	path varchar(max) not null
)


create table Quiz_Question(
	quizId int foreign key references Quiz(id),
	questionId int foreign key references Question(id),
	[status] bit default(1)
)

create table QuestionAnswer(
	id int primary key identity(1,1),
	question_id int foreign key references Question(id),
	correct bit default(1),
	content_answer NVARCHAR(max),
)


create table Quiz_Handle(
	taker varchar(300) not null references Account(username),
    Question_id INT foreign key references Question(id),
	answer_id INT foreign key references QuestionAnswer(id),
	submitTime datetime default(CURRENT_TIMESTAMP),
	marked bit default(0)
)




create table QuizHistory(
	id int  primary key identity(1,1),
	taker varchar(300) not null references Account(username),
	Quiz_id int foreign key references Quiz(id), 
	point float default(0),
	takeTime datetime default(CURRENT_TIMESTAMP),
	finishTime datetime default(CURRENT_TIMESTAMP)
)
create table QuizHistoryDetail(
	id int references QuizHistory(id),
	question_id int references Question(id),
	marked bit,
	answer_id int, --không ref Answer_Question vì nó sẽ thể hiện cho cả câu hỏi chưa trả lời => id = -1
)
Go





insert into role
values('customer')
insert into role
values('admin')
insert into role
values('expert')
insert into Account(role_id,username,password,email)
values(2,'sa','123456','email@gmail.com')
insert into country
values('vietnam')
insert into country
values('trung quoc')
insert into country
values('nhat ban')
insert into country
values('han quoc')
insert into information(infor_id)
values(1)
insert into Account(role_id,username,password,email)
values(3,'thang2001','123456','thang2001@gmail.com')
insert into information(infor_id,full_name,infor_image,phone)
values(2,'luu duc thang','images.jfif',0372288946)




-- Insert Course
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status]) VALUES
           (N'[Khóa học lập trình C# Cơ bản]'
           ,1200000
           ,'https://f.howkteam.vn/Upload/cke/images/1_LOGO%20SHOW%20WEB/1_C%23_AutoC%23/1_C%23%20C%C4%83n%20b%E1%BA%A3n/Thumbnail.jpg'
		   ,N'Chào mừng các bạn đến với khóa học LẬP TRÌNH HTTP REQUEST VỚI C#, khóa học đầu tiên trong cụm khóa học về Auto C# của Howkteam.com. Mục đích hướng tới là các bạn có thể làm auto bằng C# mà không cần dùng tới các ngôn ngữ như AutoIT, C++,…'
		   ,'true')

INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'[Khóa học lập trình Java căn bản]'
           ,1200000
           ,'https://i.ytimg.com/vi/jIQmebw9VaA/maxresdefault.jpg',N'Với mục đích giới thiệu đến mọi người về Ngôn ngữ Java -  một ngôn ngữ lập trình khá mới mẻ so với C, C++, Java, PHP ở Việt Nam. Thông qua khóa học LẬP TRÌNH JAVA CƠ BẢN ĐẾN HƯỚNG ĐỐI TƯỢNG, Kteam sẽ hướng dẫn các bạn kiến thức cơ bản của Java. Để từ đó, có được nền tảng cho phép bạn tiếp tục tìm hiểu những kiến thức tuyệt vời khác của Java hoặc là một ngôn ngữ khác.',
		   'true')

INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'[Khóa học lập trình Python cơ bản]'
           ,1200000
           ,'https://f.howkteam.vn/Upload/cke/images/1_LOGO%20SHOW%20WEB/3_Python/1_Python%20c%C6%A1%20b%E1%BA%A3n/Python%20c%C6%A1%20b%E1%BA%A3n_0.png',
		   N'Với mục đích giới thiệu đến mọi người NGÔN NGỮ PYTHON, một ngôn ngữ lập trình khá mới mẻ so với C, C++, Java, PHP ở Việt Nam. Thông qua khóa học LẬP TRÌNH PYTHON CƠ BẢN, Kteam sẽ hướng dẫn các bạn kiến thức cơ bản của Python. Để từ đó, có được nền tảng cho phép bạn tiếp tục tìm hiểu những kiến thức tuyệt vời khác của Python hoặc là một ngôn ngữ khác.',
		   'true')

INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status]) VALUES
           (N'[SQL Server]'
           ,1000000
           ,'https://i.ytimg.com/vi/2fanjSYVElY/maxresdefault.jpg',
		   N'Với hệ thống nhỏ, chúng ta hoàn toàn có thể lưu trữ dữ liệu bằng file để khi tắt ứng dụng dữ liệu chúng ta vẫn còn được lưu giữ. Nhưng với hệ thống lớn, truy vấn tìm kiếm, thao tác với dữ liệu trên file không còn dễ dàng và hiệu quả nữa. Và SQL ra đời để giải quyết vấn đề đó.',
		   'true')

INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'[HTML,CSS]'
           ,1000000
           ,'https://i.ytimg.com/vi/zwsPND378OQ/maxresdefault.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
---------------
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Tiếng Anh'
           ,8000000
           ,'https://ejoy-english.com/blog/wp-content/uploads/2018/09/4360304_cover.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Tiếng Trung'
           ,1000000
           ,'https://admin.kalzen.com/upload/tiengtrung/images/%E1%BA%A2nh%20b%C3%ACa%20web%20TTTD.png',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'[HTML,CSS]'
           ,1000000
           ,'https://i.ytimg.com/vi/zwsPND378OQ/maxresdefault.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Tiếng Hàn'
           ,1000000
           ,'https://cla.hust.edu.vn/xmedia/2016/01/hoc-tieng-han-online-cho-nguoi-viet-cfl.png',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N' Tiếng Nhật'
           ,1000000
           ,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3R-fRan3CaA6aXjdH7VBQTvSjNQxk4L1Qwy7F3RMpCm7r9qgh5sBo1e1nUSP9pMjFfmc&usqp=CAU',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Lập trình'
           ,1000000
           ,'https://tuhocdohoa.vn/wp-content/uploads/2019/11/L%E1%BA%ACP-TR%C3%8CNH-L%C3%80-G%C3%8C.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Thiết kế đồ họa'
           ,1000000
           ,'https://mlc.edu.vn/wp-content/uploads/2020/10/nganh-thiet-ke-do-hoa.png',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Thiết kế – lập trình web'
           ,1000000
           ,'https://wiki.tino.org/wp-content/uploads/2019/12/2thiet-ke-web-lap-trinh-web.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Mỹ thuật đa phương tiện'
           ,1000000
           ,'https://www.arena-multimedia.vn/wp-content/uploads/tin-bao-chi/hinhbaipr-hinh01-2.png',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Tin học văn phòng'
           ,1000000
           ,'https://daykembatnha.edubit.vn/data/sites/5f2fdd6806ed3f8a17cd5528/files/tinhocvanphon.png',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Kinh doanh – Khởi nghiệp – Startup'
           ,1000000
           ,'https://emc.vn/shared/Service/2019.12/starup.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Kinh doanh – bán hàng online'
           ,1000000
           ,'https://doopage.com/assets/uploads/2018/09/ban-hang-tren-mang-1.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Online Marketing'
           ,1000000
           ,'https://nanoweb.vn/data/media/1/files/qua-tet-573.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'Kỹ năng thuyết trình'
           ,1000000
           ,'https://letrinhnhuquynh.com/wp-content/uploads/2019/10/K%C4%A9-n%C4%83ng-thuy%E1%BA%BFt-tr%C3%ACnh-l%C3%A0-g%C3%AC-Thuy%E1%BA%BFt-tr%C3%ACnh-T%E1%BB%90T-c%E1%BA%A7n-k%E1%BB%B9-n%C4%83ng-n%C3%A0o.jpg',
		   N'Biết cách xây dựng giao diện web với HTML, CSS. Biết cách phân tích giao diện website. Biết cách đặt tên class CSS theo chuẩn BEM
. Biết cách làm giao diện web responsive. Làm chủ Flexbox khi dựng bố cục website. Sở hữu 2 giao diện web khi học xong khóa học. Biết cách tự tạo động lực cho bản thân. Biết cách tự học những kiến thức mới. Học được cách làm UI chỉn chu, kỹ tính. Nhận chứng chỉ khóa học do F8 cấp.',
		   'true')

		   
-----------------------
insert into Account(role_id,username,password,email)
values(1,'minh','minh','minhnthe163336@gmail.com')
insert into information(infor_id)
values(3)
INSERT INTO [dbo].[Course]([course_title],[price],[course_image],[course_description],[course_status])  VALUES
           (N'[Test_Course_01]'
           ,69
           ,'https://i.redd.it/2pc4x399mjx51.jpg'
		   ,N'TEST_COURSE_DESC'
		   ,'true')
insert into Account_Course (Account_id, Course_id)
values(2, 6)
insert into Account(role_id,username,password,email)
values(1,'abc','123456','thang2001N@gmail.com')
insert into information(infor_id,full_name,infor_image,phone)
values(4,'luu duc thang','images.jfif',0372288946)
insert into Account_Course (Account_id, Course_id)
values(4, 1)
insert into Account_Course (Account_id, Course_id)
values(4, 2)
insert into Account_Course (Account_id, Course_id)
values(4, 3)
insert into Account_Course (Account_id, Course_id)
values(4, 4)
insert into Account_Course (Account_id, Course_id)
values(4, 5)
insert into Account_Course (Account_id, Course_id)
values(4, 6)
insert into Account_Course (Account_id, Course_id)
values(4, 7)
insert into Account_Course (Account_id, Course_id)
values(4, 8)
insert into Account_Course (Account_id, Course_id)
values(4, 9)
insert into Account_Course (Account_id, Course_id)
values(4, 10)
insert into Account_Course (Account_id, Course_id)
values(4, 11)
insert into Account_Course (Account_id, Course_id)
values(4, 12)
insert into Account_Course (Account_id, Course_id)
values(4, 13)
insert into Account_Course (Account_id, Course_id)
values(4, 14)
insert into Account_Course (Account_id, Course_id)
values(4, 15)
insert into Account_Course (Account_id, Course_id)
values(4, 16)
insert into Account_Course (Account_id, Course_id)
values(4, 17)




INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,1
			,N'C# là gì'
			,N'Trong khóa học C# CƠ BẢN này chúng ta sẽ cùng tìm hiểu về một ngôn ngữ lập trình hiện đại khá mạnh mẽ – Đó là C#.
			 Trước tiên hãy xem thử ngôn ngữ chúng ta chuẩn bị tìm hiểu có những đặc trưng gì nhé!'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Sơ lược về ngôn ngữ C#.
				+ Những đặc trưng của ngôn ngữ C#.
				+ Tại sao lại lựa chọn ngôn ngữ C#'
			,N'https://www.youtube.com/watch?v=9kohr6pMwag')

			
INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,2
			,N'Cấu trúc lệnh cơ bản trong C#'
			,N'Để viết được một chương trình phần mềm đầu tiên, chúng ta sẽ làm quen với cấu trúc lệnh cơ bản của C#'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Cấu trúc cơ bản của một chương trình trong C#.
				+ Giải thích ý nghĩa một số từ khóa được sử dụng trong chương trình đầu tiên.
				+ Cách viết comment trong C#.
				+ Ví dụ chương trình đầu tiên bằng C#.'
			,N'https://www.youtube.com/watch?v=FhAIc0tlyaQ')
INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,3
			,N'Cấu trúc lệnh cơ bản trong C#'
			,N'Để viết được một chương trình phần mềm đầu tiên, chúng ta sẽ làm quen với cấu trúc lệnh cơ bản của C#'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Cấu trúc cơ bản của một chương trình trong C#.
				+ Giải thích ý nghĩa một số từ khóa được sử dụng trong chương trình đầu tiên.
				+ Cách viết comment trong C#.
				+ Ví dụ chương trình đầu tiên bằng C#.'
			,N'https://www.youtube.com/watch?v=FhAIc0tlyaQ')
INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,4
			,N'Cấu trúc lệnh cơ bản trong C#'
			,N'Để viết được một chương trình phần mềm đầu tiên, chúng ta sẽ làm quen với cấu trúc lệnh cơ bản của C#'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Cấu trúc cơ bản của một chương trình trong C#.
				+ Giải thích ý nghĩa một số từ khóa được sử dụng trong chương trình đầu tiên.
				+ Cách viết comment trong C#.
				+ Ví dụ chương trình đầu tiên bằng C#.'
			,N'https://www.youtube.com/watch?v=FhAIc0tlyaQ')
INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,5
			,N'Cấu trúc lệnh cơ bản trong C#'
			,N'Để viết được một chương trình phần mềm đầu tiên, chúng ta sẽ làm quen với cấu trúc lệnh cơ bản của C#'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Cấu trúc cơ bản của một chương trình trong C#.
				+ Giải thích ý nghĩa một số từ khóa được sử dụng trong chương trình đầu tiên.
				+ Cách viết comment trong C#.
				+ Ví dụ chương trình đầu tiên bằng C#.'
			,N'https://www.youtube.com/watch?v=FhAIc0tlyaQ')
INSERT INTO [dbo].[lesson]([course_id],[lesson_unit],[lesson_title],[lesson_description],[lesson_content], [lesson_video]) VALUES
			(1
			,6
			,N'Cấu trúc lệnh cơ bản trong C#'
			,N'Để viết được một chương trình phần mềm đầu tiên, chúng ta sẽ làm quen với cấu trúc lệnh cơ bản của C#'
			,N'Trong bài học này, chúng ta sẽ cùng tìm hiểu các vấn đề:
				+ Cấu trúc cơ bản của một chương trình trong C#.
				+ Giải thích ý nghĩa một số từ khóa được sử dụng trong chương trình đầu tiên.
				+ Cách viết comment trong C#.
				+ Ví dụ chương trình đầu tiên bằng C#.'
			,N'https://www.youtube.com/watch?v=FhAIc0tlyaQ')

--insert requiment
INSERT INTO Requirement(course_id,content)
values(1,'1')
INSERT INTO Requirement(course_id,content)
values(1,N'đã thành thạo c cơ bản')
INSERT INTO Requirement(course_id,content)
values(1,N'biết một số thuộc tính cơ bản của C#')
INSERT INTO Requirement(course_id,content)
values(1,N'từng học qua một số ngôn ngữ cơ bản ')
INSERT INTO Requirement(course_id,content)
values(1,N'vận dụng được một số chức năng của netbean')
INSERT INTO Requirement(course_id,content)
values(1,N'làm quen với')
INSERT INTO Requirement(course_id,content)
values(2,'2')
INSERT INTO Requirement(course_id,content)
values(3,'3')
INSERT INTO Requirement(course_id,content)
values(4,'4')
INSERT INTO Requirement(course_id,content)
values(5,'5')
INSERT INTO Requirement(course_id,content)
values(6,'6')


INSERT INTO [dbo].[Post]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail])
     VALUES
            (1 ,'Change method payment','New method payment have been added','Notify','https://th.bing.com/th/id/R.475e914fd4b9fc903cd3f68d63947e26?rik=1zz0JOwlYryixg&riu=http%3a%2f%2fwww.anarsolutions.com%2fwp-content%2fuploads%2f2016%2f12%2fMaintenance.jpg&ehk=HTDmNFR80ISIFmQUDrPbxiEx3VpNvo891rgAdMsbITM%3d&risl=&pid=ImgRaw&r=0');
GO
INSERT INTO [dbo].[Post]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail])
     VALUES
            (1 ,'Notify about lesson','New lesson have been added','Notify','https://th.bing.com/th/id/R.475e914fd4b9fc903cd3f68d63947e26?rik=1zz0JOwlYryixg&riu=http%3a%2f%2fwww.anarsolutions.com%2fwp-content%2fuploads%2f2016%2f12%2fMaintenance.jpg&ehk=HTDmNFR80ISIFmQUDrPbxiEx3VpNvo891rgAdMsbITM%3d&risl=&pid=ImgRaw&r=0');
GO
INSERT INTO [dbo].[Post]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail])
     VALUES
            (1 ,'Notify about course','New course have been added','Notify','https://th.bing.com/th/id/R.475e914fd4b9fc903cd3f68d63947e26?rik=1zz0JOwlYryixg&riu=http%3a%2f%2fwww.anarsolutions.com%2fwp-content%2fuploads%2f2016%2f12%2fMaintenance.jpg&ehk=HTDmNFR80ISIFmQUDrPbxiEx3VpNvo891rgAdMsbITM%3d&risl=&pid=ImgRaw&r=0');
GO

INSERT INTO [dbo].[Slide]
           ([slide_title]
           ,[slide_description]
           ,[slide_status])
     VALUES
           ('test slide','111111','true')
   

INSERT INTO [dbo].[ImgOfSlide]
           ([slide_id]
           ,[img])
     VALUES
           (1
           ,'https://aptech.vn/wp-content/uploads/2021/05/lap-trinh-.net.png
')



INSERT INTO [dbo].[ImgOfSlide]
           ([slide_id]
           ,[img])
     VALUES
           (1
           ,'https://aptech.vn/wp-content/uploads/2021/05/lap-trinh-c.png
')

INSERT INTO [dbo].[Blogs]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail]
           ,[date_create_blog])
     VALUES
           (2
           ,N'Thêm khóa học mới'
           ,N'Hệ thống đã thêm khóa học mới '
           ,N'Notify'
           ,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwypbhZpABwVnxahgQqTzsB7gqo5GTpfZBSA&usqp=CAU
'
		   ,getdate())
GO


INSERT INTO [dbo].[Blogs]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail]
           ,[date_create_blog])
     VALUES
           (1
           ,N'Cập nhật hệ thống'
           ,N'Hệ thống sẽ tiến hành bảo trì trong 6 tiếng nữa'
           ,N'Notify'
           ,'https://kynguyenlamdep.com/wp-content/uploads/2022/06/avatar-cute-vui-nhon.jpg
'
		   ,getdate())
GO

INSERT INTO [dbo].[Blogs]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail]
           ,[date_create_blog])
     VALUES
           (3
           ,N'Thắc mắc về khóa học'
           ,N'Khóa học của em bị lỗi thì em cần phải làm gì và khắc phục ra sao'
           ,N'Ask'
           ,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWnmFy-zvvU-n802fJxIgEn9PPQ6i7hjKW4g&usqp=CAU
'
		   ,getdate())
GO

INSERT INTO [dbo].[Blogs]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail]
           ,[date_create_blog])
     VALUES
           (1
           ,N'Thông báo về tài khoản'
           ,N'Hệ thống đã cập nhật chức năng đổi mật khẩu cho mỗi tài khoản'
           ,N'Ask'
           ,'https://iigvietnam.com/wp-content/uploads/2021/03/iig-thong-bao.png
'
		   ,getdate())
GO

INSERT INTO [dbo].[Blogs]
           ([account_id]
           ,[title]
           ,[detail]
           ,[category]
           ,[thumbnail]
           ,[date_create_blog])	
     VALUES
           (3
           ,N'Hỏi về tài khoản'
           ,N'Em muốn đổi mật khẩu và email mới thì cần phải làm gì'
           ,N'Ask'
           ,'https://iigvietnam.com/wp-content/uploads/2021/03/iig-thong-bao.png
'
		   ,getdate())
GO


insert into [Subject] values ('Subject 1'), ('Subject 2'), ('Subject 3');
insert into [QuizLevel] values ('Easy'), ('Normal'), ('Hard');
insert into [QuizType] values ('Exam'), ('Casual'), ('Simulation');
insert into [Quiz] values (1, 'Quiz No.1, Getting familiar with code', 1, 'a Description', 1800, 0.6, 10, 2);
insert into [Question] values	('Which is an int ?', 'int mean Integer dummy', 1),
								('Can a char be considered as int', 'Yes', 1),
								('Which of these answer satisfy that: a | 1 = 3', '0b11 | 1 = 0b11 and 0b10 | 1 = 0b111', 2),
								('What are people writing code called', 'PROGRAMMer Duh', 2),
								('Which of these are not a programming language ?', 'Banana is not. Might be soon, but not now', 3);
insert into [QuestionAnswer] values (1, 0, 'a'), (1, 0, '0.1'), (1, 1, '89'), (1, 0, 'trues'),
									(2, 1, 'Yes'), (2, 0, 'No'),
									(3, 0, '7'), (3, 0, '4'), (3, 1, '3'), (3, 1, '2'),
									(4, 1, 'Programmer'), (4, 0, 'Doctor'), (4, 0, 'Computer'),
									(5, 0, 'Java'), (5, 1, 'Banana'), (5, 0, 'Ruby'), (5, 0, 'C');
insert into [Quiz_Question] (quizId, questionId) values (1, 1), (1, 2), (1, 3), (1, 4), (1, 5);


insert into Account(role_id,username,password,email,date_register)
values(3,'thang2002','123456','thang2002@gmail.com','2022-07-20')
insert into information(infor_id,full_name,infor_image,phone)
values(5,'luu duc thang','images.jfif',0372288946)

insert into Account(role_id,username,password,email,date_register)
values(3,'thang2003','123456','thang2003@gmail.com','2021-07-20')
insert into information(infor_id,full_name,infor_image,phone)
values(6,'luu duc thang','images.jfif',0372288946)