go
create database OnlineLearning
go
drop database OnlineLearning

go
use OnlineLearning
go
-- drop database OnlineLearning

create table role(
	id int primary key identity(1,1),
	name varchar(100) not null,
)


create table Account(
	id int primary key identity(1,1),
	role_id int foreign key references role(id),
	username varchar (300) not null unique,
	password varchar (300) not null,
	email varchar (255),
)

create table country(
	id int primary key identity(1,1),
	name varchar(200),
)
create table status(
	id int primary key identity(1,1),
	name varchar(200),
)
 
create table information(
	id int not null unique,
	country_id int FOREIGN KEY REFERENCES country(id),
	status_ID int FOREIGN KEY REFERENCES country(id),
	displayname nvarchar(255),
	full_name nvarchar (255),
	address nvarchar (255),
	phone int,
	gender nvarchar(255),
	DOB date ,
	avatar varchar(255),
)



ALTER TABLE information
ADD CONSTRAINT FK_information_Account FOREIGN KEY(id) 
    REFERENCES Account(id);


create table Course(
	id int primary key identity(1,1),
	name varchar(200) not null,
	money int not null,
	video varchar(max) not null,
)


create table Account_Course(
	Account_id int foreign key references Account(id),
	Course_id int foreign key references Course(id),
)


create table comment(
	id int primary key identity(1,1),
	Course_id int foreign key references Course(id),
	Account_id int foreign key references Account(id),
	contents varchar(max) not null,
)


insert into role
values('admin')
insert into role
values('student')

insert into country
values('vietnam')
insert into country
values('trung quoc')
insert into country
values('nhat ban')


