/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */
// 1. ytplayer code: https://developers.google.com/youtube/player_parameters#IFrame_Player_API
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;
function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
        height: '450',
        width: '100%',
        videoId: '${key_video_id}'
    });
}
// 2. get player.playerInfo.currentTime
var list_time = new Array();
let list_note = document.getElementById("list_note");
let listItemsJSON = localStorage.getItem('${lesson.id}');
let list_note_JSON = listItemsJSON ? JSON.parse(listItemsJSON) : [];
for (let i = 0; i < list_note_JSON.length; i++) {
    let li = document.createElement('li');
    li.innerHTML = list_note_JSON[i];
    list_note.appendChild(li);
}
function myFunction() {
    var result = confirm("Bạn có muốn lưu ghi chú?");
    if (result) {
        var tm = document.getElementById("time").innerHTML = player.playerInfo.currentTime;
        var tm1 = tm.toFixed(0);
        function secondsToHms(tmm) {
            tmm = Number(tmm);
            var h = Math.floor(tmm / 3600);
            var m = Math.floor(tmm % 3600 / 60);
            var s = Math.floor(tmm % 3600 % 60);
            var hDisplay = h > 0 ? h + (h == 1 ? " h " : " h ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? " m " : " m ") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? " s" : " s") : "";
            return hDisplay + mDisplay + sDisplay;
        }

        var tm2 = secondsToHms(tm1);
        var note = document.getElementById("note");
        var text_note = note.value;
        list_time.push(tm2);
        let li = document.createElement('li');
        li.innerHTML = "Thời Gian: " + tm2 + " : " + text_note;
        list_note.appendChild(li);
        let text_JSON = "Thời Gian: " + tm2 + " : " + text_note;
        list_note_JSON.push(text_JSON);
        localStorage.setItem('${lesson.id}', JSON.stringify(list_note_JSON));
        text_JSON = "";
    }


}
function myFunction1() {
    document.getElementById("test").innerHTML = list_time;
}
function removeFunction() {
    var result = confirm("Bạn có muốn lưu ghi chú?");
    if (result) {
        localStorage.removeItem("${lesson.id}");
    } else {
        alert("abc");
    }
}

