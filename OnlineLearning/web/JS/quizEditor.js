/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
const qIndexAtt = document.createAttribute("qIndex");
const aIndexAtt = document.createAttribute("aIndex");
var currentQ = -1;
var qList = [];

function addNewQ(quizID) {
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'quizID=' + quizID + '&addQuestion=true',
        success: function (result) {
            if (result.success === "true") {
                var ans = [];
                var correct = 0;
                console.log(result.content);
                result.content.answerList.forEach((item, index) => {
                    ans.push({id: item.id, content: item.answer});
                    if (item.id === result.content.choosenAnswer)
                        correct = index;
                });
                var question = {
                    id: result.content.id,
                    question: result.content.question,
                    explanation: result.content.explaination,
                    answer: ans,
                    correct: correct
                };
                var index = qList.push(question) - 1;
                document.getElementById("questionListPanel").appendChild(questionNode(question.id, question.question, index));
            } else {
                console.log(result.content);
            }
        }
    });
}
//    var question = {
//        id: 0,
//        question: "New Question",
//        //num: -1,
//        explanation: "",
//        answer: [{id:0, content:"A"},
//                {id:1, content:"B"},
//                {id:2, content:"C"}],
//        correct: 0
//    };
function questionNode(id, title, index) {
    var btn = document.createElement("button");
    btn.setAttribute("class", "selectQuestion w-full text-left my-1 p-2 rounded border border-2 border-teal-600 bg-white hover:bg-teal-lightest truncate");
    btn.id = "Q_" + id;
    btn.setAttribute("qIndex", index);
    btn.title = title;
    btn.innerHTML = title;
    btn.setAttribute("onclick", "updateEditor(" + index + ")");
    return btn;
}
function saveQuestion(quizID) {
    if (currentQ < 0 || currentQ >= qList.length) {
        console.log("Cannot save because not select any");
        return;
    }
    var q = qList[currentQ];
    if (q.correct < 0 || q.correct >= q.answer) {
        alert("Please choose one correct answer!");
        return;
    }
    var ans = [];
    q.answer.forEach((a) => {
        ans.push({id: a.id, answer: a.content});
    });
    var quizQuestion = {
        id: q.id,
        questionIndex: 0,
        question: q.question,
        explaination: q.explanation,
        marked: false,
        choosenAnswer: q.answer[q.correct].id,
        answerList: ans
    };
    setLoading(true);
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'saveQuestion=true&data=' + JSON.stringify(quizQuestion),
        success: function (result) {
            if (result.success === "true") {
                alert("Question Saved!");
                //update color
                setQuestionEditted(false, q.id);
            } else {
                alert("Question Save Failed!");
                console.log(result.content);
            }
        },
        complete: (j, stat) => {setLoading(false);}
    });
}
function editInternally(element) {
    if (currentQ < 0 || currentQ >= qList.length) {
        console.log(currentQ + " is out of range");
        return;
    }
    var q = qList[currentQ];
    setQuestionEditted(true, q.id);
    switch (element.id) {
        case "qContent":
            q.question = element.value;
            break;
        case "explanationText":
            q.explanation = element.value;
            break;
        default:
            if (element.hasAttribute("aIndex")) {
                q.answer.forEach(item => {
                    if (item.id === parseInt(element.getAttribute("aIndex"))) {
                        item.content = element.value;
                    }
                });
            }
            break;
    }
}
function setQuestionEditted(bool, id) {
    if (!bool) {
        $("#Q_" + id).removeClass("edittedQuestion");
    } else {
        $("#Q_" + id).addClass("edittedQuestion");
    }
}
function answerNode(id, content, isCorrect) {
    var n = document.createElement("div");
    n.setAttribute("class", "selectAnswer w-full flex flex-row");
    var radio = document.createElement("input");
    radio.type = "radio";
    radio.name = "qAnswer";
    radio.id = "Answer_" + id;
    radio.value = id;
    radio.setAttribute("onchange", "changeCorrectAnswer(" + id + ")");
    if (isCorrect) {
        radio.setAttribute("checked", "checked");
        //radio.checked = true;
    }
    radio.setAttribute("data-modal", "centeredFormModal");
    var delBtn = document.createElement("button");
    delBtn.setAttribute("class", "rounded border border-1 border-red-500 px-2 py-1 my-1 mr-1");
    delBtn.innerHTML = '&#10006;';
    delBtn.setAttribute("onclick", "deleteAnswer(" + id + ")");
    var text = document.createElement("textarea");
    text.innerHTML = content;
    text.setAttribute("aIndex", id);
    text.setAttribute("value", radio.id);
    text.setAttribute("onblur", "textAreaAdjust(this)");
    text.setAttribute("onchange", "editInternally(this)");
    text.setAttribute("class", "m-1 flex flex-grow border border-1 border-grey-dark px-1");
    setTimeout(function () {
        textAreaAdjust(text);
    }, 100);
    n.appendChild(delBtn);
    n.appendChild(radio);
    n.appendChild(text);
    return n;
}
function changeCorrectAnswer(id) {
    qList[currentQ].answer.forEach((item, index) => {
        if (item.id === id) {
            qList[currentQ].correct = index;
            //alert(index);
            return;
        }
    });
}
var i = 10;
function addAnswer() {
    if (currentQ < 0 || currentQ >= qList.length) {
        console.log(currentQ + " is out of range");
        return;
    }
    //send to server to add
    var q = qList[currentQ];
    console.log(q);
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'questionID=' + q.id + '&addAnswer=true',
        success: function (result) {
            if (result.success === "true") {
                var a = {
                    id: result.content.id,
                    content: result.content.answer
                };
                q.answer.push(a);
                qList[currentQ] = q;
                $("#answerPanel").append(answerNode(a.id, a.content, false));
            } else {
                console.log(result.content);
            }
        }
    });
}
function updateEditor(qIndex) {
    if (qIndex >= qList.length || qIndex < 0) {
        $("#questionEditorPanel").hide();
        console.log("Invalid qIndex! Cannot load question");
        return;
    }
    $("#questionEditorPanel").show();
    currentQ = qIndex;
    var q = qList[qIndex];
    if (q.answer.length <= 0) {
        return;
    }
    $("#qContent").val(q.question);
    $("#explanationText").val(q.explanation);
    $("#answerPanel").empty();
    q.answer.forEach((now, i) => {
        $("#answerPanel").append(answerNode(now.id, now.content, i === q.correct).outerHTML);
    });
}
function deleteAnswer(id) {
    if (currentQ < 0 || currentQ >= qList.length) {
        return;
    }
    var q = qList[currentQ];
    if (qList.indexOf(q) === q.correct) {
        q.correct = -1;
    }
    q.answer = q.answer.filter(a => a.id !== id);
    qList[currentQ] = q;
    $("#Answer_" + id).parent().remove();
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'answerID=' + id + '&deleteAnswer=true'
    });
}
function textAreaAdjust(element) {
    element.style.height = "0px";
    element.style.height = (element.scrollHeight) + "px";
}
function loadQuizFromJson(info) {
    if (info !== null) {
        console.log(info);
        $("#quizTitle").val(info.tittle);
        $("#quizDesc").val(info.Description);
        $("#quizDuration").val(toDuration(info.length));
        $("#quizMaxPoint").val(info.maxPoint);
    }
}
function loadQuestionListFromJson(list) {
    if (list !== null) {
        var panel = document.getElementById("questionListPanel");
        panel.innerHTML = "";
        qList = [];
        list.forEach((q) => {
            var ans = [];
            q.answerList.forEach((answer) => {
                ans.push({id: answer.id, content: answer.answer});
            });
            var correct = 0;
            ans.forEach((ans, j) => {
                if (ans.id === q.choosenAnswer)
                    correct = j;
            });
            var question = {
                id: q.id,
                question: q.question,
                explanation: q.explaination,
                answer: ans,
                correct: correct
            };
            var index = qList.push(question);
            panel.appendChild(questionNode(q.id, q.question, index - 1));
        });
    }
}
function toDuration(num) {
    var hours = Math.floor(num / 3600);
    var minutes = Math.floor((num - (hours * 3600)) / 60);
    var seconds = num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
}
;

//-----------------------------
function updateInfo() {
    setLoading(true);
    var formData = $("#quizInfo").serialize();
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: formData + '&saveQuizInfo=true',
        success: function (result) {
            if (result.success === "true") {
                console.log(result.success);
                setQuizInfo(true, "Saved!");
            } else {
                console.log(result.success);
                setQuizInfo(false, "Something wrong internally");
            }
        },
        error: (obj, status, err) => {
            setQuizInfo(false, "Save failed! " + err);
        },
        complete: (obj, status) => {
            setLoading(false);
        }
    });
}
function setQuizInfo(bool, msg) {
    if (bool === null) {
        $("#infoSaveStatus").text("");
        return;
    }
    if (bool) {
        $("#infoSaveStatus").css("color", "#51d88a");
        $("#infoSaveStatus").text(msg);
    } else {
        $("#infoSaveStatus").css("color", "#ef5753");
        $("#infoSaveStatus").text(msg);
    }
}
function setLoading(bool) {
    if (bool) {
        $("#loadingScreen").show();
    } else {
        $("#loadingScreen").fadeOut(500);
    }
}
function showReconfirm(bool) {
    if (bool) {
        $("#reconfirm").show();
    } else {
        $("#reconfirm").hide();
    }
}
function askDeleteQuiz(id) {
    showReconfirm(true);
    $("#reconfirm-msg").text("Are you sure you want to Delete this Quiz ?");
    $("#reconfirm-yes").attr("onclick", "deleteQuiz(" + id + ")");
}
function deleteQuiz(id) {
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'quizID=' + id + '&deleteQuiz=true'
    });
}
function askDeleteQuestion() {
    if (currentQ < 0 || currentQ >= qList.length) {
        return;
    }
    var q = qList[currentQ];
    showReconfirm(true);
    $("#reconfirm-msg").text("Are you sure you want to Delete this question ?");
    $("#reconfirm-yes").attr("onclick", "deleteQuestion(" + q.id + ")");
}
function deleteQuestion(id) {
    $.ajax({
        type: "POST",
        url: "quizEditorCtrl",
        cache: false,
        data: 'questionID=' + id + '&deleteQuestion=true',
        complete: (j, stat) => {
            showReconfirm(false);
            $("#Q_" + id).remove();
            updateEditor(-1);
        }
    });
}