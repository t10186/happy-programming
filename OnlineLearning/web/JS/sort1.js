
document.querySelectorAll('#table th.sort-column').forEach(headerCell => {
    headerCell.addEventListener('click', () => {
        const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell);
        const type = !headerCell.classList.contains("sort-number");
        var asc =  headerCell.classList.contains('sort-column-desc');
        console.log(asc);
        sort(headerIndex, asc, type);
    });
});
function sort(column, asc, type) {
    var table, rows, switching, i, x, y, shouldSwitch, sign;
    sign = asc ? 1 : -1;
    table = document.getElementById("table");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[column];
            y = rows[i + 1].getElementsByTagName("TD")[column];
            // type ==true is text else number
            if ((x.innerHTML.trim() !== "" && y.innerHTML.trim() === "")) {
                continue;
            }
            if ((x.innerHTML.trim() === y.innerHTML.trim())) {
                continue;
            }
            if (asc && (x.innerHTML.trim() === "" && y.innerHTML.trim() !== "")) {
                shouldSwitch = true;
                break;
            }
            if (type) {
                if (x.innerHTML.toLowerCase().trim() > y.innerHTML.toLowerCase().trim() === asc) {
                    shouldSwitch = true;
                    break;
                }
            } else {
                if (x.innerHTML.toLowerCase() * sign > y.innerHTML.toLowerCase() * sign) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
    document.querySelectorAll('#table th').forEach(element => {
        element.classList.remove('sort-column-asc');
        element.classList.remove('sort-column-desc');
    });
    var className = 'sort-column-';
    className += asc ? 'asc' : 'desc';
    table.rows[0].getElementsByTagName("th")[column].classList.add(className);
}
; 