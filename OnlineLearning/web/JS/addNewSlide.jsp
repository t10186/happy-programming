%-- 
    Document   : addNewCourse
    Created on : Jun 5, 2022, 4:42:52 AM
    Author     : baobao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
              crossorigin="anonymous">
        <base href="${path}">
        <style>
            .login{
                background: url('Image/login-new.jpeg')
            }
            .new{
                margin-top: 10px;
                margin-left: 23px;
                padding: 7px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .save{
                margin-top: 10px;
                padding: 5px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .chooseType{
                background-color: #dedede;
                border-radius: 2px;
                border: 1px solid; 
            }
        </style>
        <link rel="stylesheet" href="CSS/styles.css">
    </head>
    <body class="h-screen font-sans login bg-cover">
        <div id="toast"></div>
        <div class="container mx-auto h-full flex flex-1 justify-center items-center"
             style="height: 961px;"
             >
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <form action="add-new-slide" method="post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl"
                          style="width: 700px">
                        <p style="margin-top: 35px;" class="text-gray-800 font-medium">Thêm Slide mới</p>    
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">Hình ảnh Slide</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="slide" type="file" required="">
                        </div> 
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">Ngày tạo</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="timeCreate" type="date" required=""">
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">Tên Slide</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="name" type="text" required="">
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">Mô tả</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="description" type="text" required="">
                        </div> 
                        <button type="submit" class="save">lưu</button>
                        <h3 style="color: red" class="text-danger">${Successful}</h3>
                    </form>
                     
                </div>
            </div>
        </div>     
    </body> 
</html>
