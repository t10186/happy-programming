/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours + ':' + minutes + ':' + seconds;
};

String.prototype.toHHMMSS2 = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = Math.floor(sec_num - (hours * 3600) - (minutes * 60));
    if (hours > 0) {
        if (hours   < 10) {
            hours   = "0"+hours;
        }
        hours = hours + "h ";
    } else {
        hours = "";
    }
    if (minutes > 0) {
        if (minutes < 10) {minutes = "0"+minutes;}
        minutes = minutes + "m ";
    } else {
        minutes = "";
    }
    if (seconds > 0) {
        if (seconds < 10) {seconds = "0"+seconds;}
        seconds = seconds + "s ";
    } else {
        seconds = "";
    }
    return hours + minutes + seconds;
};
String.prototype.toHHMMSS3 = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    if (sec_num <= 0) sec_num = 0;
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = Math.floor(sec_num - (hours * 3600) - (minutes * 60));
    if (hours > 0) {
        if (hours   < 10) {
            hours   = "0"+hours;
        }
        hours = hours + "h ";
    } else {
        hours = "";
    }
    if (minutes > 0) {
        if (minutes < 10) {minutes = "0"+minutes;}
        minutes = minutes + "m ";
    } else {
        minutes = "";
    }
    if (seconds < 10) {seconds = "0"+seconds;}
    seconds = seconds + "s ";
    return hours + minutes + seconds;
};
