/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


var isReview = false;
var question = [];
var qIndex = 0;
var startTime = 0;
var length = 0;
var marked = false;
var modified = false;
var username = "";
var Quiz_Id = 0;
var submitted = false;

function setEnvironment(username_, QuizId_, startTime_, length_, isReview_) {
    username = username_;
    Quiz_Id = QuizId_;
    if (isReview_ !== null) isReview = isReview_;
    if (!isReview) {
        startTime = startTime_;
        length = length_ * 1000;
    }
}
function setData(list, index) {
    question = list;
    qIndex = index;
    setUp();
}

function setUp() {
    if (isReview) {
        $("#timerMain").hide();
        //$("#prevBtn").hide();
        $("#reviewBtn").text("Review Results");
        $("#reviewTitle").text("Review Results");
        $("#reviewDesc").text("Review specific question below.");
        $("#unanswerBtn").hide();
        $("#submitBtn").hide();
        $("#notReviewPart").hide();
    } else {
        $("#exitBtn").remove();
        $("#exitForm").remove();
        $("#explainBtn2").hide();
        $("#incorrectBtn").hide();
        $("#peakAnswerTitle").text("Peak At Answer");
        setInterval(updateTimer, 300);
    }
    updateQuestion();
    updateReviewAll();
}
function updateQuestion() {
    if (qIndex < question.length && qIndex >= 0) {
        var curQuestion = question[qIndex];
        marked = curQuestion.marked;
        var unanswerBadge = "";
        if (isReview & curQuestion.choosenAnswer <= 0) {
            unanswerBadge = " <span class=\"badge badge-danger\">UNANSWERED</span>";
        }
        $("#questionProgress").text((qIndex + 1) + " / " + question.length);
        $("#prevBtn").show();
        $("#nextBtn").show();
        $("#questionHeader").html("QUESTION " + (qIndex + 1) + unanswerBadge);
        if (qIndex === 0) $("#prevBtn").hide();
        if (qIndex === question.length - 1) $("#nextBtn").hide();
        var correct = "";
        //iterate through answer list
        $("#answerContent").empty();
        var choosenAns = curQuestion.choosenAnswer;
        curQuestion.answerList.forEach((answer) => {
            $("#answerContent").append(getAnswerHTML(answer, answer.id == choosenAns));
            if(answer.correct) {
                if (correct.length > 0) {
                    correct = correct + ", or " + answer.answer;
                } else {
                    correct = answer.answer;
                }
            }
        });
        $('input[name="answerOption"]').change(updateChoosenAnswer);
        $("#correctAnswer").text("The Correct Answer is " + correct);
        $("#questionContent").text(curQuestion.question);
        if (isReview && curQuestion.explaination.length <= 0) {
            $("#explainBtn2").hide();
        } else {
            $("#explanationPanel").text(curQuestion.explaination);
        }
        updateMark();
        updateReviewBtn();
    }
}
function updateReviewAll() {
    $("#questionTable").empty();
    question.forEach((question_, i) => {
        var mark = "";
        if (question_.marked) {
            mark = "<i class=\"bi bi-bookmark-fill mark\"></i>";
        }
        if (question_.choosenAnswer <= 0) {
            //unanswered
            $("#questionTable").append("<button id='Btn" + i + "' onclick='gotoQuestion(" + i + ")'>" + (i + 1) + mark + "</button>");
        } else {
            //answered
            $("#questionTable").append("<button id='Btn" + i + "' class=\"answered\" onclick='gotoQuestion(" + i + ")'>" + (i + 1) + mark + "</button>");
        }
    });
    updateReviewBtn();
}
function gotoQuestion(index) {
    saveAnswer(function() {
        qIndex = index;
        $('#reviewModal').modal('hide');
        updateQuestion();
    });
}
function updateChoosenAnswer() {
    if (isReview) return;
    modified = true;
    var checked = $('input[name="answerOption"]:checked');
    if (checked.length > 0) {
        question[qIndex].choosenAnswer = checked.val();
    } else {
        question[qIndex].choosenAnswer = -1;
    }
    updateReviewBtn();
}
function updateReviewBtn() {
//    modified = true;
//    var checked = $('input[name="answerOption"]:checked');
//    if (checked.length > 0) {
//        //answered
//        $("#Btn" + qIndex).addClass("answered");
//        question[qIndex].choosenAnswer = checked.val();
//    } else {
//        $("#Btn" + qIndex).removeClass("answered");
//        question[qIndex].choosenAnswer = -1;
//    }
    if (question[qIndex].choosenAnswer > 0) {
        $("#Btn" + qIndex).addClass("answered");
    } else {
        $("#Btn" + qIndex).removeClass("answered");
    }
}
function saveAnswer(successFunc) {
    if (isReview) {
        successFunc();
        return;
    }
    if (!modified) {
        successFunc();
        return;
    }
    $.ajax({
        type: "POST",
        url: "quiz-handle",
        cache: false,
        data: 'submitHandle=true&index=' + qIndex + '&taker=' + username + '&question=' + JSON.stringify(question[qIndex]),
        success: function (result) {
            if (result.success === "true") {
                successFunc();
                modified = false;
            } else {
                console.log("Cannot save answer, please try again!\n" + result.error);
            }
        }
    });
}
function getAnswerHTML(answer, isChoosen) {
    if (!isReview) {
        var checked = "";
        if (isChoosen) {
            checked = " checked";
        }
        return "<label class=\"row custom-radio\">" +
                    "<input type=\"radio\" name=\"answerOption\" value=\"" + answer.id + "\"" + checked + "/>" +
                    "<span class=\"checkmark\"></span>" +
                    "<text>" + answer.answer + "</text>" +
                "</label>";
    }
    var extra = "";
    if (isChoosen) extra = " <span class=\"badge badge-secondary\">Your choice</span>";
    if (answer.correct) {
        return "<label class=\"row custom-radio\">" +
                    "<input type=\"radio\" name=\"answer" + answer.id + "\" disabled checked/>" +
                    "<span class=\"checkmark correct\"></span>" +
                    "<text>" + answer.answer + extra + "</text>" +
                "</label>";
    } else if (isChoosen) {
        return "<label class=\"row custom-radio\">" +
                    "<input type=\"radio\" name=\"answer" + answer.id + "\" disabled checked/>" +
                    "<span class=\"checkmark wrong\"></span>" +
                    "<text>" + answer.answer + extra + "</text>" +
                "</label>";
    }
    return "<label class=\"row custom-radio\">" +
                "<input type=\"radio\" name=\"answer" + answer.id + "\" disabled/>" +
                "<span class=\"checkmark\"></span>" +
                "<text>" + answer.answer + "</text>" +
            "</label>";
}
function nextQ() {
    saveAnswer(function() {
        qIndex++;
        updateQuestion();
    });
}
function prevQ() {
    saveAnswer(function() {
        qIndex--;
        updateQuestion();
    });
}
function clearAnsw() {
    $('input[name="answerOption"]').prop('checked', false);
    updateChoosenAnswer();
}
function mark() {
    modified = true;
    if (marked) {
        marked = false;
    } else {
        marked = true;
    }
    if (qIndex < question.length && qIndex >= 0) {
        question[qIndex].marked = marked;
    }
    updateMark();
}
function updateMark() {
    if (marked) {
        $("#questionHeader").addClass("mark");
        $("#markText").text("Unmark this question");
        $("#Btn" + qIndex).empty();
        $("#Btn" + qIndex).append((qIndex + 1) + "<i class=\"bi bi-bookmark-fill mark\"></i>");
    } else {
        $("#questionHeader").removeClass("mark");
        $("#markText").text("Mark for review");
        $("#Btn" + qIndex).empty();
        $("#Btn" + qIndex).append(qIndex + 1);
    }
}
function updateTimer() {
    var timeLeft = startTime - Date.now() + length;
    if (timeLeft <= 0) {
        //submit
        finalizeQuiz();
    } else {
        $("#timerValue").text(getTimeStr(timeLeft));
    }
}
function getTimeStr(time) {
    var sec_num = Math.floor(time / 1000); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours + ':' + minutes + ':' + seconds;
};
function finalizeQuiz() {
    var extraData = "";
    if (modified) {
        extraData = '&question=' + JSON.stringify(question[qIndex]);
    }
    $.ajax({
        url: 'quiz-handle',
        type: 'POST',
        cache: false,
        data: 'submitQuiz=true&taker=' + username + '&quizId=' + Quiz_Id + '&startTime=' + startTime + extraData,
        success: function (result) {
            if (result.success === "true") {
                submitted = true;
                modified = false;
                window.location = 'quiz-lesson';
            } else {
                console.log("Cannot save submission to database, please try again!\n" + result.error);
            }
        }
    });
}
function onSubmitting() {
    $("#reviewModal").modal('hide');
    var answered = 0;
    question.forEach((question_) => {
        if (question_.choosenAnswer > 0) {
            answered++;
        }
    });
    if (answered <= 0) {
        $("#submitTitle").text("Exit Exam ?");
        $("#submitDesc").text("You have not answered any question. By clicking on the [Exit Exam] button below, you will complete your current exam and return to result screen.");
        $("#scoreExamBtn").text("Exit Exam");
    } else {
        var progress = "";
        if (answered < question.length) {
            progress = "<p class=\"text-danger\">" + answered + " of " + question.length + " Question Answered</p>";
        }
        $("#submitDesc").html(progress + "You have not answered any question. By clicking on the [Exit Exam] button below, you will complete your current exam and receive your score. You will not be able to change any answers at this point.");
    }
    $("#submitConfirmModal").modal('show');
}
function poking() {
    $.ajax({
        url: 'quiz-handle',
        type: 'GET',
        cache: false
    });
}