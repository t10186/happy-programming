<%-- 
    Document   : listBlogs
    Created on : Jun 4, 2022, 10:46:40 PM
    Author     : Admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Blogs</title> 
    </head>
    <body>
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3 overflow-hidden">

                        <div class="container-fluid p-5 bg-white-300 text-gray text-center">
                            <h1>List Blogs</h1>
                            <p>Share all things about website</p> 
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-10 col-lg-8">
                                <form action="search-list-blogs" class="card card-sm">
                                    <div class="row no-gutters align-items-center">
                                        <!--                                             
                                        <!--end of col-->

                                        <div class="col">
                                            <input class="form-control form-control-lg form-control-borderless" type="search" name="searchKeyword"
                                                   placeholder="Tìm kiếm ...">
                                        </div>
                                        <!--end of col-->
                                        <div class="col-auto">
                                            <button class="btn btn-lg btn-dark" type="submit">Search</button>
                                        </div>

                                        <div class="container-fluid p-5 bg-white-300 text-gray text-center">
                                            <a href="JSP/addnewblog.jsp" class="btn btn-lg btn-dark">Add new blog</a>
                                            <a href="my-blog-list?id=${acc.id}" class="btn btn-lg btn-dark">My blogs</a>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                </form>

                            </div>
                            <!--end of col-->
                        </div>
                        <br>
                        <div class="container shadow-lg p-3 mb-5 bg-body rounded">
                            <div class="row align-items-start rounded-3 " >
                                <c:forEach items="${listBlogs}" var="item">          
                                    <div class="col-sm-4 " >
                                        <div class="card border border-5 shadow-sm p-3 mb-5 bg-body rounded" style="width: 18rem;">
                                            <img src="https://i.imgur.com/w1Bdydo.jpg" class="card-img-top" alt="...">
                                            <div class="card-body">
                                                <h5 class="card-title fw-bold fs-5 d-inline-block text-truncate">${item.getTitle()}</h5>
                                                <span class="d-inline-block text-truncate" style="max-width: 150px;">
                                                    ${item.getDetail()}
                                                </span>

                                                <br>
                                                <span class="badge bg-light text-dark">${item.getCategory()}</span>

                                            </div>
                                            <a href="detailblogs?blogsid=${item.id}" class="btn btn-primary">More Detail</a>
                                            <div class="card-footer">
                                                <small class="text-muted">Last updated: ${item.getTimeCreate()}</small>
                                            </div>
                                        </div>

                                        <br>
                                    </div>
                                </c:forEach> 
                            </div>
                        </div>
                        <br>
                        <c:choose>
                            <c:when test="${listBlogs==null || listBlogs.size()==0}">
                                Not founds
                            </c:when>
                            <c:otherwise>
                                <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                                    <h3 class="page-item"><a class="page-link" href="show-list-blogs?page=${1}">first</a></h3>
                                    <c:forEach begin="1" end="${totalPage}" var="i">
                                        <h3 class="page-item ${i == page?"active":""}"><a class="page-link" href="show-list-blogs?page=${i}">${i}</a></h3>
                                        </c:forEach>
                                    <h3 class="page-item"><a class="page-link" href="show-list-blogs?page=${totalPage}">Last</a></h3>

                                </nav>
                            </c:otherwise>
                        </c:choose>
                    </main>>
                    </body>
                </div>
                <!--Footer-->
                <footer class="bg-grey-darkest text-white p-2">
                    <div class="flex flex-1 mx-auto"></div>
                    <div class="flex flex-1 mx-auto"></div>
                </footer>
                <!--/footer-->
                <%@include file="footer.jsp" %>
            </div>
</html>
