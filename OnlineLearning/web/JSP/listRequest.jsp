<%-- 
    Document   : listRequest
    Created on : Jun 5, 2022, 1:56:20 AM
    Author     : baobao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>HOME</title> 
    </head>

    <body>
        <!-- card -->
        <!--Header Section Starts Here-->
        <!--/Header-->
        <div class="flex flex-1">
            <!--Sidebar-->
            <c:set var="linkColor" value="listRequest"/>
            <%@include file="sidebar.jsp" %>
            <!--/Sidebar-->
            <main class="bg-white-300 flex-1 p-3 overflow-hidden">
                <div class="rounded overflow-hidden shadow bg-white mx-2 w-full">
                    <div class="px-6 py-2 border-b border-light-grey">
                        <div class="font-bold text-xl">List Request</div>
                    </div>
                    <div class="table-responsive" style="height: 400px">
                        <table class="table text-grey-darkest">
                            <thead class="bg-grey-dark text-white text-normal">
                                <tr>
                                    <th scope="col">người thiết kế</th> 
                                    <th scope="col">email</th> 
                                    <th scope="col">tiêu đề</th>
                                    <th scope="col">tiêu đề phụ</th>
                                    <th scope="col">giá</th>
                                    <th scope="col">thể loại</th>
                                    <th scope="col">ảnh</th>
                                    <th scope="col">mô tả</th>
                                    <th scope="col">chi tiết</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listRequest}" var="request"> 
                                    <tr>
                                        <td>${request.getInfor().fullName}</td>
                                        <td>${request.getAcc().email}</td>
                                        <td>${request.getCour().title}</td>
                                        <td>${request.getCour().subTitle}</td>
                                        <td>${request.getCour().price}</td>
                                        <td>1</td>
                                        <td>${request.getCour().imageCourse}</td>
                                        <td>${request.getCour().description}</td>
                                        <td>${request.getCour().requirement}</td>
                                        <td>
                                            <div>
                                                <form action="acceptrq?nm=accept" method="post">
                                                    <input name="courseID" value="${request.getCour().id}" style="display: none"> 
                                                    <button
                                                        class="bg-blue-500 hover:bg-blue-800 text-white font-light py-1 px-2 rounded-full" id="nm" name="accept">
                                                        chấp nhận
                                                    </button>
                                                </form>
                                                <form action="acceptrq?mn=reject" method="post">
                                                    <input name="courseID" value="${request.getCour().id}" style="display: none"> 
                                                    <button
                                                        class="bg-red-500 hover:bg-red-800 text-white font-light py-1 px-2 rounded-full" id="nm" name ="declare" >
                                                        từ chối 
                                                    </button>
                                                </form> 
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /card -->
            </main>
        </div>

        <!--Footer-->
        <footer class="bg-grey-darkest text-white p-2">
            <div class="flex flex-1 mx-auto"></div>
            <div class="flex flex-1 mx-auto"></div>
        </footer>
        <!--/footer-->

        <%@include file="footer.jsp" %>
    </body>
</html>
