<%-- 
    Document   : home
    Created on : Jume 17, 2022, 16:32:16 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>HOME</title> 
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }

        </style>
    </head>

    <body>
        <br>
        <br>
        <br>
        <br>
        <!--Container -->
        <div class="d-flex align-items-center">
            <div class="flex-shrink-0">
                <img src="https://th.bing.com/th/id/OIP.AUwik5YXM3N7xpm8Nei3KwHaDt?w=182&h=91&c=7&r=0&o=5&dpr=1.25&pid=1.7" alt="...">
            </div>
            <div class="flex-grow-1 ms-3">
                This is some content from a media component. You can replace this with any content and adjust it as needed.
            </div>
        </div>
        <!--Screen-->

        <!--Header Section Starts Here-->
        <!--/Header-->

        <!--Sidebar-->
        <c:set var="linkColor" value="home"/>
        <%@include file="sidebar.jsp" %>
        <!--/Sidebar-->
        <!--Main--> 
        <main class="bg-white-300 flex-1 p-3 overflow-hidden">
            <table class="table caption-top">
                <caption>List of Post</caption>
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Author</th>
                        <th scope="col">Time create</th>
                        <th scope="col">Status</th>
                        <th scope="col">Flag</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listPost}" var="item">
                         <form action="edit-status" method="post">
                        <tr>
                            
                                <td class="col-md-1"> 
                                    <input value="${item.post_Id}" name="postid">   
                                </td>
                            <td>${item.author_id}</td>
                            <td>${item.timeCreate}</td>

                            <td >${item.status == 1?"Active":"Disable"}</td>
                            
                            <td>
                                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                    
                                    <button type="submit" class="btn btn-outline-secondary" name="status" value="${item.status}">${item.status == 0?"Active":"Disable"}</button>
                                </div>
                                 
                            </td>
                            <td>
                                <a href="detail-post?postid=${item.post_Id}" class="btn btn-secondary">More Detail</a>
                            </td>

                        </tr>
                        </form>
                    </c:forEach> 
                </tbody>
            </table>
            <c:choose>
                <c:when test="${listPost==null || listPost.size()==0}">
                    Not founds
                </c:when>
                <c:otherwise>
                    <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                        <h3 class="page-item"><a class="page-link" href="list-post?page=${1}">first</a></h3>
                        <c:forEach begin="1" end="${totalPage}" var="i">
                            <h3 class="page-item ${i == page?"active":""}"><a class="page-link" href="list-post?page=${i}">${i}</a></h3>
                            </c:forEach>
                        <h3 class="page-item"><a class="page-link" href="list-post?page=${totalPage}">Last</a></h3>
                    </nav>
                </c:otherwise>
            </c:choose>
        </main>

    </body>
    <!--Footer-->
    <footer class="bg-grey-darkest text-white p-2">
        <div class="flex flex-1 mx-auto"></div>
        <div class="flex flex-1 mx-auto"></div>
    </footer>
    <!--/footer-->
    <%@include file="footer.jsp" %>
    <script src="JS/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
