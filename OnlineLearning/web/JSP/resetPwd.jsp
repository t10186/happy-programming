<%-- 
    Document   : resetPwd
    Created on : May 22, 2022, 1:07:14 AM
    Author     : Phong Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reset password</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="CSS/styles.css">
        <style>
            .bg {
                background: url('Image/login-new.jpeg')
            }
        </style> 
    </head>
    <body class="h-screen font-sans bg bg-cover">
        <div class="container mx-auto h-full flex flex-1 justify-center items-center">
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <c:choose>
                        <c:when test="${param.usn != null}">
                            <form action="changePassword" method="POST" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                                <p class="text-gray-800 font-medium text-center text-xl font-bold">Thay đổi mật khẩu</p>
                                <div class="">
                                    <label class="block text-sm text-gray-00" for="pwd">Nhập mật khẩu mới:</label>
                                    <input name="username" type="text" hidden value="${param.usn}">
                                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="pwd" name="pwd" type="password" required="" placeholder="New password">
                                </div>
                                <div class="mt-4 items-center justify-between">
                                    <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Xác nhận
                                    </button>
                                </div>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <form action="resetPassword" method="POST" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                                <p class="text-gray-800 font-medium text-center text-xl font-bold">Thay đổi mật khẩu</p>
                                <c:if test="${reseterror != null}">
                                    <div role="alert" class="mb-2">
                                        <div class="border border-t-0 border-red-300 rounded-b bg-red-300 px-4 py-3 text-red-800">
                                            <p>${reseterror}</p>
                                        </div>
                                    </div>
                                </c:if>
                                <div class="">
                                    <label class="block text-sm text-gray-00" for="email">Nhập email của tài khoản mà bạn muốn thay đổi:</label>
                                    <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="email" name="email" type="email" required="" placeholder="Email" aria-label="email">
                                </div>
                                <div class="mt-4 items-center justify-between">
                                    <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit">Xác nhận
                                    </button>
                                </div>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </body>
</html>
