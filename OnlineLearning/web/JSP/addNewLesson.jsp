<%-- 
    Document   : addNewCourse
    Created on : Jun 5, 2022, 4:42:52 AM
    Author     : baobao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
              crossorigin="anonymous">
        <base href="${path}">
        <style>
            .login{
                background: url('Image/login-new.jpeg')
            }
            .new{
                margin-top: 10px;
                margin-left: 23px;
                padding: 7px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .save{
                margin-top: 10px;
                padding: 5px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .chooseType{
                background-color: #dedede;
                border-radius: 2px;
                border: 1px solid;
            }
        </style>
        <link rel="stylesheet" href="CSS/styles.css">
    </head>


    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div style="background-color: #333333 !important;" class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="forms"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->

                    <div class="container mx-auto h-full flex flex-1 justify-center items-center"
                         style="height: 761px;">
                        <div class="w-full max-w-lg">
                            <div class="leading-loose">
                                <form action="addnewlesson" method="post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl"
                                      style="width: 700px">
                                    <p style="margin-top: 35px;" class="text-gray-80 font-medium">Thêm bài học</p>    
                                    <input name="course_id" value="${courseID}" style="display: none" >
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Đơn vị bài học :</label>
                                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Thêm đơn vị bài học" id="cus_name" name="lesson_unit" type="number" required="">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Tiêu đề :</label>
                                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Tiêu đề bài học" id="cus_name" name="lesson_title" type="text" required=""">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Mô tả bài học :</label>
                                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Mô tả bài học" id="cus_name" name="lesson_description" type="text" required=""">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Nội dung bài học :</label>
                                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Nội dung bài học" id="cus_name" name="lesson_content" type="text" required=""">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Link video bài giảng :</label>
                                        <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Link video" id="cus_name" name="lesson_video" type="text" required=""">
                                    </div>
                                    <div class="mt-2">
                                        <label class="block text-sm text-gray-00" for="cus_name">Thời lượng bài giảng :</label>
                                        <div class="row">
                                            <input class="col-4 px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Giờ" id="cus_name" name="lesson_time_hours" type="number" required="">
                                            <input class="col-4 px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Phút" id="cus_name" name="lesson_time_minute" type="number" required="">
                                            <input class="col-4 px-5 py-1 text-gray-700 bg-gray-200 rounded" placeholder="Giây" id="cus_name" name="lesson_time_second" type="number" required="">
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <input class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" type="submit" value="Thêm bài học">
                                        <!--                                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" href="">
                                                                                    Thêm bài giảng
                                                                                </a>-->
                                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" href="list-lesson?courseID=${courseID}">
                                            Huỷ bỏ
                                        </a>
                                    </div>
                                    <h3>${error}</h3>
                                </form>
                            </div>
                        </div>
                    </div>     
                    <!--/Main-->
                </div>
                <!--Footer-->
                <!--/footer-->
                <%@include file="footer.jsp" %>
            </div>

        </div>
        <script src="JS/main.js"></script>
    </body> 

</html>

