<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Luu Duc Thang
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/sort.css">
        <link rel="stylesheet" href="CSS/search.css">
        <title>Forms | Tailwind Admin</title> 
        <style>
            .btn-edit{
                background-color: #927adcf5 !IMPORTANT;
                width: 99px;
                height: 36px;
                border: none;
            }
            .pagination{
                margin-top: 20px;
                display: flex;
                justify-content: center;
            }
            .page-item.active .page-link{
                background-color: gray !important;
                border-color: gray !important;
            }
            .container{
                display: flex;
                justify-content: space-between;
            }
            .wrap{
                margin-left: 800px !important;
            }
            .addCourse{
                border: 1px solid;
                display: flex;
                align-items: center;
                background-color: #808080b0;
                width: 137px;
                justify-content: center;
                border-radius: 5px;
                cursor: pointer;
            }
            .addCourse a{
                color: white;
            }
        </style>
    </head>
    <body>
        <sql:setDataSource var = "sqlConection" driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
                           url = "jdbc:sqlserver://localhost:1433;databaseName=OnlineLearning"
                           user = "sa"  password = "123456"/>
        
        
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <div class="content">
                            <div class="container">
                                <div class="addCourse">
                                    <a>thêm khóa học</a>
                                </div> 
                                <div class="wrap" >
                                    <form name="search"class="search" action="show-list-course-devoloper">
                                        <input name="keySearch" type="text" class="searchTerm" placeholder="tìm kiếm khóa học ..." value="${keySearch}">
                                        <button type="submit" class="searchButton">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div> 
                            </div> <!-- End div center   -->
                            <div class="table-responsive">
                                <table class="table custom-table" id="table">
                                    <thead>
                                        <tr>
                                            <th class="sort-column" scope="col">Tên khoá học</th>
                                            <th class="sort-column sort-number date" scope="col" class="date">ngày khởi tạo</th>
                                            <th class="sort-column sort-number" scope="col">Giá</th>
                                            <th class="sort-column" scope="col">Mô tả</th>  
                                            <th  scope="col">trạng thái</th>
                                            <th  scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listCourse}" var="item">
                                            <tr scope="row">
                                        <input type="hidden" value="${item.id}"/>
                                        <td>
                                            ${item.title}
                                        </td>
                                        <td>
                                            ${item.dateCreateCourse}
                                        </td>
                                        <td>${item.price}</td>
                                        <td>${item.description}</td>
                                        <td>
                                            <div class="form-check form-switch course-status">
                                                <input name="lesson_status" class="form-check-input" type="checkbox" ${item.status?'checked=""':''}>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="show-list-lesson-course?courseid=${item.id}">
                                                <button class="btn btn-xs btn-edit" type="submit">chi tiết</button>
                                            </a>
                                            <div class="lesson"></div>
                                        </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <c:if test="${endList>1}">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <c:if test="${index >1}">
                                            <li class="page-item">
                                                <a class="page-link" href="show-list-course-devoloper?index=${index-1}" aria-label="Previous" style="color: black">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        </c:if>
                                        <c:forEach begin="1" end="${endList}" var="i">
                                            <li class="page-item ${index == i?"active":""}"><a class="page-link " href="show-list-course-devoloper?index=${i}" style="color: black">${i}</a></li>
                                            </c:forEach>
                                            <c:if test="${index<endList}">
                                            <li class="page-item">
                                                <a class="page-link" href="show-list-course-devoloper?index=${index +1}" aria-label="Next" style="color: black">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </c:if>
                        </div>
                </div>
                </main>
                <!--/Main-->
            </div>
            <!--Footer-->
            <footer class="bg-grey-darkest text-white p-2">
                <div class="flex flex-1 mx-auto"> </div>
            </footer>
            <!--/footer-->

        </div>

    </div>
    <script src="JS/main.js"></script>  
    <script src="JS/sort1.js"></script>
    <script>
        document.querySelectorAll(".course-status").forEach(oject => {
            oject.addEventListener('click', () => {
                var courseID = oject.parentNode.parentNode.querySelector('input').value;
                var courseStatus = oject.parentNode.parentNode.querySelectorAll('input')[1].checked;
                var nameID = "[course_id]";
                var nameTable = "[dbo].[Course]";
                var nameStatus = "[course_status]";
                var https = new XMLHttpRequest();
                https.open("POST", "http://localhost:9999/OnlineLearning/JSP/updateStatus.jsp", true);
                https.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                https.send("id=" + courseID + "&status=" + courseStatus + "&table=" + nameTable + "&nameID=" + nameID + "&nameStatus=" + nameStatus);
            });
        });
    </script>
</body> 
</html>
