<%-- 
    Document   : accountdetail
    Created on : Jun 18, 2022, 1:06:28 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
        <style>
            .detail{
                width: 469px;
                height: 139px; 
            }
        </style>
    </head>
    <body>
        <div class="mx-auto bg-grey-lightest">
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">



                    <!--Sidebar-->
                    <c:set var="linkColor" value="forms"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->


                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <form action="update-information-user" method="post">
                            <div class="flex flex-col"> 
                                <div style="margin-top: 5%;" class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                    <div class="mb-2 border-solid border-grey-light rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-300 px-2 py-3 border-solid border-gray-400 border-b">
                                            Thông tin bài học  
                                        </div>
                                        <div class="p-3">
                                            <form action="update-details-lesson" method="post" class="w-full">
                                                <input type="text" style="display:none;" name="id"
                                                       value="${lesson.id}">
                                                <input type="text" style="display:none;" name="courseID"
                                                       value="${lesson.courseID}">
                                                <input type="text" style="display:none;" name="dateCreate"
                                                       value="${lesson.dateCreate}">
                                                <input type="text" style="display:none;" name="dateCreate"
                                                       value="${lesson.status}">
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-gray-500 font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-full-name">
                                                            Tiêu đề bài học :
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-full-name" type="text" value="${lesson.title}">
                                                    </div>
                                                </div>
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Đơn vị bài học :
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-username" type="number" value="${lesson.unit}">
                                                    </div>
                                                </div>
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Mô tả bài học :
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <textarea class="detail"
                                                                  class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                                  id="inline-username" type="text">${lesson.detail}</textarea>
                                                    </div>
                                                </div>              
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Link video bài giảng (Youtube) :
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-username" type="text" value="${lesson.linkVideo}">
                                                    </div>
                                                </div>
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Thời lượng bài giảng :
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <div class="row">
                                                            <input class="col-4 px-5 py-1 text-black-700 bg-200 rounded border-1 border-grey-200"
                                                                   placeholder="Giờ" value="${hours}" id="cus_name"
                                                                   name="lesson_time_hours" type="number" required="">
                                                            <input class="col-4 px-5 py-1 text-gray-700 bg-200 rounded border-1 border-grey-200"
                                                                   placeholder="Phút" value="${minute}" id="cus_name"
                                                                   name="lesson_time_minute" type="number" required="">
                                                            <input class="col-4 px-5 py-1 text-gray-700 bg-200 rounded border-1 border-grey-200"
                                                                   placeholder="Giây" value="${second}" id="cus_name"
                                                                   name="lesson_time_second" type="number" required="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div
                                        class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                                            Nội dung bài học
                                        </div>
                                        <div class="p-3">
                                            <div class="w-full">
                                                <div class="flex items-center border-b border-b-1 border-teal-500 py-2">
                                                    <img style="width: 300px;" src="${course.imageCourse}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-3">
                                            <div class="w-full">
                                                <div class="flex items-center border-b border-b-1 border-teal-500 py-2">
                                                    <textarea  style="height: 200px;"
                                                               class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                               id="inline-username" type="text" value="" placeholder="Đường dẫn hình ảnh">${lesson.content}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full"> 
                                        <div class="p-3">
                                            <div class="w-full">
                                                <div class="flex flex-wrap -mx-3 mb-2">
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-state">
                                                            Ngày đăng ký :
                                                        </label>
                                                        <div class="">
                                                            <input
                                                                class="appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                                id="" type="date"name="DOB"
                                                                value="${lesson.dateCreate}" readonly="">
                                                        </div>
                                                    </div>
                                                    <div sty class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-state">
                                                            <br>
                                                        </label>
                                                        <div class="">
                                                            <a style="float:right;" class="btn btn-success" class="nav-link" href="#">Cập nhật chỉnh sửa/ Yêu cầu xuất bản</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                                        <ul class="navbar-nav mr-auto">
                                                            <li style="margin-right: 10px;" class="nav-item">
                                                                <a class="btn btn-secondary" class="nav-link" href="#">Tổng quan khoá học</a>
                                                            </li>
                                                            <li style="margin-right: 10px;" class="nav-item active">
                                                                <a class="btn btn-secondary" class="nav-link" href="#">Dimension</a>
                                                            </li>
                                                            <li style="margin-right: 10px;" class="nav-item">
                                                                <a class="btn btn-secondary" class="nav-link" href="#">Giá khoá học</a>
                                                            </li>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    </form>   
                                    </main>
                                </div>
                                <!--Footer-->
                                <footer class="bg-grey-darkest text-white p-2">
                                    <div class="flex flex-1 mx-auto"> </div>
                                </footer>
                                <!--/footer-->
                                <%@include file="footer.jsp" %>
                            </div>
                            </div>
                            <script src="JS/main.js"></script>
                            </body> 
                            </html> 


