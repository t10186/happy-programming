<%-- 
    Document   : blogsDetail
    Created on : Jun 5, 2022, 2:22:08 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Blogs Detail</title> 
    </head>
    <style>
        .row {
            margin-left: 5%;
            margin-bottom: 2%;
            height: 150px;
            border-radius: 10px;
            background-size: 100%;

        }

        .row:hover {
            color: #bebebc;
            opacity: 0.7;


        }

        .detail {
            margin-left: 20px;
            font-weight: 500;
            word-spacing: 1px;
        }
        .mucluc{
            font-weight: 600;
            font-size: 18px;
            width: 100%;
            background-color: rgba(255, 187, 139, 0.584);
            display: flex;
            text-align: start;
            margin-top: 10px;
            height: 45px;
            border-radius: 10px;
            box-shadow: 3px 3px 3px rgba(255, 151, 76, 0.685);
            padding-left: 15px;

        }
    </style>
    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3 overflow-hidden">

                        <div class="flex flex-col">
                            <!-- Stats Row Starts Here -->

                            <!-- /Stats Row Ends Here -->

                            <!-- Card Sextion Starts Here -->
                            <div class="flex flex-1 flex-col md:flex-row lg:flex-row mx-2">

                                <!-- card -->
                                <div tabindex="-1" style="width: 60%; display: inline-block;">
                                    <div class="Slideshow_item__zjVUA"
                                         style="--cta-hover-color:#7612ff; background: linear-gradient(to right, rgb(118, 18, 255), rgb(5, 178, 255));">
                                        <div class="Slideshow_left__jdrc-">

                                        </div>
                                        <div class="Slideshow_right__1lu8d"><img class="Slideshow_img__K-c9+" 
                                                                                 src="${blog.thumbnail}"
                                                                                 alt="Thành quả của học viên" title="Thành Quả của Học Viên">
                                        </div>
                                    </div>
                                </div>
                                <!-- /card -->
                            </div>
                            <div style="display: flex; font-family: Courier; margin-left: 10px;" class="col-md-9">
                                <div style="width:80% ; ">

                                    <div>
                                        <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">
                                            Title Blogs
                                        </div>
                                        <p>${blog.title}</p>

                                    </div>
                                    <div>
                                        <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">Nội dung
                                            Blogs</div>
                                        <p>${blog.detail}</p>
                                    </div>

                                </div>
                                <div class="lg bg-vibrant  hover:bg-vibrant mb-2 p-2 md:w-1/4 mx-2 " style=" margin-top: 50px;">
                                    <div class="row"
                                         style=" background-image: url('${course.imageCourse}'); background-size: 100%; margin-left: 30px;">
                                    </div>
                                </div>
                                <div style="display: flex;  margin-left: 10px;" class="col-md-5">                        
                                    <div class="card mb-3" style="max-width: 540px;">
                                        <div class="row g-0">
                                            <div class="col-md-4">
                                                <img src="${account.avatar}" class="img-fluid rounded-start" alt="...">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card-body">
                                                    <h5 class="card-title">Post by : ${account.getDisplayName()!=null?"${account.getDisplayName()}":"Unkown"}</h5>
                                                    <p class="card-text"><small class="text-muted">Last updated: ${blog.timeCreate}</small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>                           
                            <c:if test="${acc.id== Integer.valueOf(blog.getAccount_id())}">
                                <div> 
                                    <form action="update-detail-blog" method="post">
                                        <input name="id" value="${blog.id}" type="hidden">
                                        <div class="col-md-7 border border-3 rounded-start" style="border: 1px">

                                            <h3 class="text-center">Update detail of blogs</h3>
                                            <br>
                                            <div class="form-floating mb-3">
                                                <input name="newtitle" class="form-control" id="floatingInput" placeholder="name@example.com" required>
                                                <label for="floatingInput">New Title</label>
                                            </div>
                                            <div class="form-floating">
                                                <input name="newdetail" class="form-control" id="floatingPassword" placeholder="Password" required>
                                                <label for="floatingPassword">New Detail</label>
                                            </div>
                                            <br>
                                            <select class="form-select" name="Category" required aria-label="select example">

                                                <option  value="Notify">Notify</option>
                                                <option  value="Ask">Ask</option>

                                            </select>
                                            <br>
                                            <div class="col-md-4">
                                                <label for="validationDefault01" class="form-label" required>Blogs thumbnail</label>
                                                <input type="text" class="form-control" name="thumbnail" id="validationDefault01" required>
                                            </div>
                                            <br>

                                            <button type="submit" class="btn btn-outline-secondary">Save</button>
                                        </div>
                                    </form>

                                </div>
                                <br>
                                <div> 
                                    <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">
                                        Status Blogs: ${blog.status==1?"Active":"Disable"}
                                    </div>
                                    <form action="update-blog-status" method="post">
                                        <input name="id" value="${blog.id}" type="hidden">
                                        <input name="status" value="${blog.status}" type="hidden">
                                        <button type="submit" class="btn btn-outline-secondary">${blog.status==0?"Active":"Disable"}</button>
                                    </form>
                                </div>
                            </c:if>                       
                            <!-- /Cards Section Ends Here -->
                        </div>
                    </main>
                    <!--/Main-->
                </div>

            </div>
        </div>
    </body>
    <footer class="bg-grey-darkest text-white p-2">
        <div class="flex flex-1 mx-auto"></div>
        <div class="flex flex-1 mx-auto"></div>
    </footer>
    <!--/footer-->
    <%@include file="footer.jsp" %>

</html>



