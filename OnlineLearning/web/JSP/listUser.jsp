<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Luu Duc Thang
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/sort.css">
        <link rel="stylesheet" href="CSS/search.css">
        <title>Forms | Tailwind Admin</title>
        <style >
        body{
                position: relative;
            }
         #user-profile{
            position: fixed;
            top: 80px;
            background-color: #5d6dc7c7;
            border-radius: 10px;
            display: none;
         }
        .img-account-profile {
            height: 10rem;
        }
        .rounded-circle {
            border-radius: 50% !important;
        }
        .card {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(33 40 50 / 15%);
        }
        .card .card-header {
            font-weight: 500;
        }
        .card-header:first-child {
            border-radius: 0.35rem 0.35rem 0 0;
        }
        .card-header {
            padding: 1rem 1.35rem;
            margin-bottom: 0;
            background-color: rgba(33, 40, 50, 0.03);
            border-bottom: 1px solid rgba(33, 40, 50, 0.125);
        }
        .form-control, .dataTable-input {
            display: block;
            width: 100%;
            padding: 0.875rem 1.125rem;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1;
            color: #69707a;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #c5ccd6;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.35rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .nav-borders .nav-link.active {
            color: #0061f2;
            border-bottom-color: #0061f2;
        }
        .nav-borders .nav-link {
            color: #69707a;
            border-bottom-width: 0.125rem;
            border-bottom-style: solid;
            border-bottom-color: transparent;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            padding-left: 0;
            padding-right: 0;
            margin-left: 1rem;
            margin-right: 1rem;
        } 
        .role-detail{
            display: flex ;
            align-content: stretch;
            align-items: center ;
        }
        .role-detail select{
            border: 1px solid ;
            margin-left: 30px;
        }
    </style>
        
        
    </head>

    <body>

        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">

                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="listUser"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <div class="content">
                            <div class="container">
                                <div class="wrap">
                                    <form name="search"class="search" action="showlistUserController">
                                        <input name="keySearch" type="text" class="searchTerm" placeholder="tìm kiếm khóa học ...">
                                        <button type="submit" class="searchButton">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>                
                            </div> <!-- End div center   -->
                            <br>
                            <div class="table-responsive">

                                <table class="table custom-table" id="table">
                                    <thead>
                                        <tr id="column-sort">
                                            <th scope="col" class="sort-column sort-number">ID</th>
                                            <th scope="col" class="sort-column">Tài khoản</th>
                                            <th scope="col" class="sort-column">Họ và tên</th>
                                            <th scope="col" class="sort-column">ngày đăng ký</th>
                                            <th scope="col" class="sort-column sort-number">Số điện thoại</th>
                                            <th scope="col" class="sort-column">Email</th>
                                            <th scope="col" class="sort-column">Quyền vụ</th>
                                            <th scope="col" >Chi tiêt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listAccInfor}" var="item">
                                        <tr scope="row">
                                            <input type="hidden" value="${item.getInformation().DOB}"/>
                                            <input type="hidden" value="${item.getInformation().gender}"/>
                                            <input type="hidden" value="${item.getCountry().name}"/>
                                            <input type="hidden" value="${item.getInformation().avatar}"/>
                                            <td name = "accountid">
                                                ${item.getAccount().id}
                                            </td>
                                            <td>
                                                ${item.getAccount().username}
                                            </td>
                                            <td>
                                                ${item.getInformation().fullName}
                                            </td>
                                            <td>${item.getAccount().dateRegister}</td>
                                            <td>${item.getInformation().phone}</td>
                                            <td>${item.getAccount().email}</td>
                                            <td>${item.getRole().name}</td>                                               
                                            <td> <button class="btn btn-xs btn-dark" onclick="getDetail(this)" >Thông tin</button></td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <c:if test="${endList>1}">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <c:if test="${index >1}">
                                            <li class="page-item">
                                                <a class="page-link" href="showlistUserController?index=${index-1}" aria-label="Previous" style="color: black">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        </c:if>
                                        <c:forEach begin="1" end="${endList}" var="i">
                                            <li class="page-item ${index == i?"active":""}"><a class="page-link " href="showlistUserController?index=${i}" style="color: black">${i}</a></li>
                                        </c:forEach>
                                        <c:if test="${index<endList}">
                                            <li class="page-item">
                                                <a class="page-link" href="showlistUserController?index=${index +1}" aria-label="Next" style="color: black">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </c:if>
                        </div>
                    </main>
                </div>
            </div>
        </div>
        <div class="container-xl px-4 mt-4" id="user-profile" >
            <hr class="mt-0 mb-4">
            <div class="row">
                <div class="col-xl-4">
                    <!-- Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">ảnh đại diện</div>
                        <div class="card-body text-center">
                            <!-- Profile picture image-->
                            <img id="avatar-detail" class="img-account-profile rounded-circle mb-2" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <!-- Account details card-->
                    <div class="card mb-4">
                        <div class="card-header">chi tiết tài khoản </div>
                        <div class="card-body">
                            <form action="update-role" method="post">
                                <input name="idUpdate" id="id-detail" value="">
                                <!-- Form Group (username)-->
                                <div class="row gx-3 mb-3">
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="username-detail">tài khoản</label>
                                        <input class="form-control" id="username-detail" type="text" readonly="">
                                    </div>
                                    <div class="col-md-3 role-detail">
                                        <label class="small mb-1" for="role-detail">chức vụ</label>
                                        <select name="roleDetail" id="role-detail">
                                            <option></option>
                                            <option></option>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row gx-3 mb-3">
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="phone-detail">số điện thoại</label>
                                        <input class="form-control" id="phone-detail" type="text" readonly="">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="email-detail">email</label>
                                        <input class="form-control" id="email-detail" type="text" readonly="">
                                    </div>
                                </div>
                                <!-- Form Row-->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (first name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="name-detail">họ và tên</label>
                                        <input class="form-control" id="name-detail" type="text" readonly="">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="DOB-detail">ngày sinh</label>
                                        <input class="form-control" id="DOB-detail" type="text" readonly="">
                                    </div>
                                </div>
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (organization name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="dateRegis-detail">ngày đăng ký</label>
                                        <input class="form-control" id="dateRegis-detail" type="text" readonly="">
                                    </div>
                                    <!-- Form Group (location)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="gender-detail">giới tính</label>
                                        <input class="form-control" id="gender-detail" type="text" readonly="">
                                    </div>
                                </div>
                                <!-- Form Row        -->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (organization name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="address-detail">địa chỉ</label>
                                        <input class="form-control" id="address-detail" type="text" readonly="">
                                    </div>
                                    <!-- Form Group (location)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="country-detail">quốc tịch</label>
                                        <input class="form-control" id="country-detail" type="text" readonly="">
                                    </div>
                                </div>
                                <!-- Save changes button-->
                                <button class="btn btn-primary">Save</button>
                                <div class="btn btn-primary" id="exist">hủy bỏ</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <script src="JS/main.js"></script>
        <script src="JS/sort1.js"></script>
        <script>
            var displayDetail = document.getElementById("user-profile");
                function getDetail(element){
                    // get id from this account
                    var faElement = element.parentNode.parentNode;
                    var arrTD = faElement.querySelectorAll('td');
                    var arrInput = faElement.querySelectorAll('input');
                    var id =  arrTD[0].innerText;  
                    console.log(id);
                    var username = arrTD[1].innerText;
                    var fullName = arrTD[2].innerText;
                    var dateRegister = arrTD[3].innerText;
                    var phone = arrTD[4].innerText;
                    var email = arrTD[5].innerText;
                    const listRole = [];
                    const listRoleID = [];
                    var role = arrTD[6].innerText;
                    <c:forEach items="${listRole}" var="item">
                        listRole.push('${item.name}');
                        listRoleID.push('${item.id}');
                    </c:forEach>
                    console.log(listRole);
                    var dob = arrInput[0].value;
                    var gender = arrInput[1].value;
                    var country = arrInput[2].value;
                    var avatar = arrInput[3].value;                    
                    if(avatar === ''){
                        avatar = 'null-image.jpg';
                    }
                    document.getElementById("id-detail").value = id;
                    document.getElementById("username-detail").value = username;
                    document.getElementById("name-detail").value = fullName;
                    document.getElementById("dateRegis-detail").value = dateRegister;
                    document.getElementById("phone-detail").value = phone;
                    document.getElementById("email-detail").value = email;
                    let j = 1;
                    for(let i = 0 ; i < listRole.length ; i++){
                        if(role === listRole[i]){
                            document.getElementById("role-detail").getElementsByTagName("option")[0].innerHTML = listRole[i];
                            document.getElementById("role-detail").getElementsByTagName("option")[0].value = listRoleID[i];
                        }else{
                            document.getElementById("role-detail").getElementsByTagName("option")[j].innerHTML = listRole[i];
                            document.getElementById("role-detail").getElementsByTagName("option")[j].value = listRoleID[i];
                            j++;
                        }
                    }
                    document.getElementById("DOB-detail").value = dob;
                    document.getElementById("gender-detail").value = gender;
                    document.getElementById("country-detail").value = country;
                    document.getElementById("avatar-detail").src = 'Image/' + avatar;
                    displayDetail.style.display = 'block';
                };
                document.getElementById("exist").addEventListener('click', () =>{
                    displayDetail.style.display = 'none';  
                });
        </script>
    </body> 
</html>
