<%-- 
    Document   : courses
    Created on : Jun 2, 2022, 6:06:22 AM
    Author     : Quynh_Nhu
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>DetailCourses</title>
    </head>

    <style>
        .row {
            margin-left: 5%;
            margin-bottom: 2%;
            height: 150px;
            border-radius: 10px;
            background-size: 100%;

        }

        .row:hover {
            color: #bebebc;
            opacity: 0.7;


        }

        .detail {
            margin-left: 20px;
            font-weight: 500;
            word-spacing: 1px;
        }
        .mucluc{
            font-weight: 600;
            font-size: 18px;
            width: 100%;
            background-color: rgba(255, 187, 139, 0.584);
            display: flex;
            text-align: start;
            margin-top: 10px;
            height: 45px;
            border-radius: 10px;
            box-shadow: 3px 3px 3px rgba(255, 151, 76, 0.685);
            padding-left: 15px;

        }
        .requiment{
            margin-bottom: 100px;
        }
        .requiment-content p{
            margin-left: 50px;
            color: black;
            font-weight: 600; 
            margin-top: 9px;
        }
    </style>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3 overflow-hidden">

                        <div class="flex flex-col">
                            <!-- Stats Row Starts Here -->

                            <!-- /Stats Row Ends Here -->

                            <!-- Card Sextion Starts Here -->
                            <div class="flex flex-1 flex-col md:flex-row lg:flex-row mx-2">

                                <!-- card -->


                                <div tabindex="-1" style="width: 100%; display: inline-block;">
                                    <div class="Slideshow_item__zjVUA"
                                         style="--cta-hover-color:#7612ff; background: linear-gradient(to right, rgb(118, 18, 255), rgb(5, 178, 255));">
                                        <div class="Slideshow_left__jdrc-">

                                        </div>
                                        <div class="Slideshow_right__1lu8d"><a rel="noreferrer noopener noreferrer"
                                                                               target="_blank"><img class="Slideshow_img__K-c9+"
                                                                 src="https://files.fullstack.edu.vn/f8-prod/banners/Banner_01_2.png"
                                                                 alt="Thành quả của học viên" title="Thành Quả của Học Viên"></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- /card -->

                            </div>
                            <div style="display: flex; font-family: Courier; margin-left: 10px;">
                                <div style="width:60% ; ">
                                    <div class="border-b border-light-grey">
                                        <div class="font-bold text-xl" style="margin-top:2%;  font-size: 28px; ">
                                            ${course.title}
                                        </div>
                                    </div>
                                    <div>
                                        <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">
                                            chi tiết bài học
                                        </div>
                                        <p>${course.description}</p>

                                    </div>
                                    <div>
                                        <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">Nội dung
                                            khóa học</div>
                                        <div style="    max-height: 600px;overflow: scroll;">
                                            <c:forEach items="${listLesson}" var="lesson">
 
                                                <div class="mucluc">${lesson.title}</div>

                                            </c:forEach>
                                        </div> 

                                    </div>
                                        <div class="requiment">
                                        <div class="font-bold text-xl" style="margin-top:5%;  font-size: 22px; ">Yêu Cầu
                                        </div >
                                        <c:forEach items="${course.requirement}" var="items">
                                            <div class="requiment-content">
                                                <p>- ${items.content}</p>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>

                                <div class="lg bg-vibrant  hover:bg-vibrant mb-2 p-2 md:w-1/4 mx-2 " style=" margin-top: 50px;">
                                    <div class="row"
                                         style=" background-image: url('${course.imageCourse}'); background-size: 100%; margin-left: 30px;">

                                    </div>
                                    <a href="payment-course?courseID=${course.id}">
                                        <button  style="margin-left: 110px;margin-top:30px; background-color: rgb(255, 151, 76); height: 40px; width:120px;
                                                 border-radius: 10px;
                                                 box-shadow: 2px 2px 2px rgb(244, 144, 72); font-size: 18px; color: black;">
                                            Đăng kí
                                        </button> 
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /Cards Section Ends Here -->
                </div>
                </main>
                <!--/Main-->
            </div>
            <!--Footer-->
            <footer class="bg-grey-darkest text-white p-2">
                <div class="flex flex-1 mx-auto"></div>
                <div class="flex flex-1 mx-auto"></div>
            </footer>
            <!--/footer-->
            <%@include file="footer.jsp" %>

        </div>
    </div>
    <script src="JS/main.js"></script>
</body>

</html>
