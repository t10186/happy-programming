<%-- 
    Document   : register
    Created on : May 20, 2022, 11:35:09 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.19/dist/sweetalert2.min.css">
        <base href="${path}">
        <style>
            .login{
                background: url('Image/login-new.jpeg')
            }
            .error{
                color: red;
                margin-left: 10px;
                font-size: 12px;
            }
        </style>
        <link rel="stylesheet" href="CSS/styles.css">
    </head>
    <body class="h-screen font-sans login bg-cover">
        <div id="toast"></div>
        <div class="container mx-auto h-full flex flex-1 justify-center items-center">
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <form action="register" method="post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                        <p class="text-gray-800 font-medium">Đăng ký</p>

                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cusname">Tên đăng nhập</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cusname" name="username" type="text" placeholder="Tên đăng nhập">
                            <h5 class="error"></h5>
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cusname">Số Điện Thoại</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cusname" name="phone" type="number"  placeholder="">
                            <h5 class="error"></h5>
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cusname">Giới Tính</label>
                            <select name="gender" id="gender">
                                <option value="male">Nam</option>
                                <option value="female">Nữ</option>
                                <option value="other">khác</option>
                            </select>
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cusname">Mật khẩu</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cusname" name="password" type="password" placeholder="Mật khẩu phải nhiều hơn 8 và ít hơn 30 ký tự">
                            <h5 class="error"></h5>
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cusname">Nhập lại mật khẩu</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cusname" name="rePassword" type="password" placeholder="Nhập lại mật khẩu">
                            <h5 class="error"></h5>
                        </div> 
                        <div class="mt-2">
                            <label class="block text-sm text-gray-600" for="cusemail">Email</label>
                            <input class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded" id="cusname" name="email" type="email"  placeholder="Email của bạn">
                            <h5 class="error"></h5>
                        </div>
                        <div class="mt-4">
                            <button class="px-4 py-1 text-white font-light tracking-wider bg-gray-900 rounded" type="submit" >Đăng ký</button>
                        </div>
                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800" href="JSP/login.jsp">
                            Bạn đã có tài khoản ?
                        </a>
                    </form>
                </div>
            </div>
        </div> 
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.19/dist/sweetalert2.all.min.js"></script>

        <script type="text/javascript">
            document.querySelector('button').addEventListener('click', (event) => {
            <sql:setDataSource var = "sqlConection" driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
                               url = "jdbc:sqlserver://localhost:1433;databaseName=OnlineLearning"
                               user = "sa"  password = "123456"/>
            <sql:query dataSource = "${sqlConection}" sql = "SELECT 
                       [username]
                       ,[email]
                       FROM [dbo].[Account] " var = "result" /> 
                var arrInput = document.querySelectorAll('input');
                var arrError = document.querySelectorAll('.error');
                arrError.forEach(element => element.innerHTML = '');
                var userName = arrInput[0].value;
                var phone = arrInput[1].value;
                var password = arrInput[2].value;
                var rePassword = arrInput[3].value;
                var email = arrInput[4].value;
                var communeListe = [];
            <c:forEach var="row" items="${result.rows}">
                communeListe.push({
                    username: '${row.username}',
                    email: '${row.email}'
                });
            </c:forEach>;
                const checkUsername = communeListe.find(f => f.username === userName);
                const checkEmail = communeListe.find(f => f.email === email);
                console.log(checkUsername);
                if (checkUsername !== undefined) {
                    arrError[0].innerHTML = 'Tài khoản đã tồn tại!';
                    event.preventDefault();
                    return;
                }
                if (checkEmail !== undefined) {
                    arrError[4].innerHTML = 'Email đã tồn tại!';
                    event.preventDefault();
                    return;
                }

                if (userName === '') {
                    arrError[0].innerHTML = 'Tài khoản không được bỏ trống';
                    event.preventDefault();
                    return;
                }
                if (phone.length !== 10) {
                    arrError[1].innerHTML = 'Vui lòng nhập đầy đủ số điện thoại với 10 chữ số!';
                    event.preventDefault();
                    return;
                }
                if (password.length < 8 || password.length > 30) {
                    arrError[2].innerHTML = 'Mật khẩu phải có độ dài từ 8 đến 30 ký tự!';
                    event.preventDefault();
                    return;
                }
                if (password !== rePassword) {
                    arrError[3].innerHTML = 'Nhập lại mật khẩu không chính xác!';
                    event.preventDefault();
                    return;
                }
                if (!email.match("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")) {
                    arrError[4].innerHTML = 'Email sai định dạng';
                    event.preventDefault();
                    return;
                }


            });

        </script>
    </body> 

</html>

