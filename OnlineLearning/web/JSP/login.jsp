<%-- 
    Document   : login
    Created on : May 20, 2022, 11:34:00 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
              integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <base href="${path}">
        <style>
            .login {
                background: url('Image/login-new.jpeg')
            }
        </style> 
        <link rel="stylesheet" href="CSS/styles.css">
    </head>
    <body class="h-screen font-sans login bg-cover">
        <div class="container mx-auto h-full flex flex-1 justify-center items-center">
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <form action="login" method="Post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl">
                        <p class="text-gray-800 font-medium text-center text-lg font-bold">Đăng nhập</p>
                        <div class="">
                            <label class="block text-sm text-gray-00" for="username">Tên đăng nhập</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="username" name="username" type="text"
                                   required="" placeholder="Tên đăng nhập" aria-label="username">
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-600" for="password">Mật khẩu</label>
                            
                            <input class="w-full px-5  py-1 text-gray-700 bg-gray-200 rounded" id="password" name="password" type="password"
                                   required="" placeholder="Mật khẩu" aria-label="password">
                        </div>
                        <div class="mt-4 items-center justify-between">
                            <div class="inline-block right-0 align-baseline  font-bold text-sm text-500 " href="#">
                                <input type="checkbox" name="remember"> Nhớ mật khẩu
                            </div>
                            <a class="inline-block right-0 align-baseline  font-bold text-sm text-500 hover:text-blue-800" href="JSP/resetPwd.jsp">
                                Quên mật khẩu?
                            </a>
                        </div>
                        <div>
                        <h3 style="color: green" class="text-danger">${mess}</h3>
                        <h3 style="color: red" class="text-danger">${error}</h3>
                        <button class="px-4 py-1 text-white text-center font-light tracking-wider bg-gray-900 rounded" type="submit">Đăng
                                nhập</button>
                        </div>
                        <div>
                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800" href="JSP/register.jsp">
                            Đăng ký tài khoản
                        </a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </body> 
</html>
