<%-- 
    Document   : errorPage
    Created on : Jun 29, 2022, 11:10:43 AM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <base href="${path}">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/errorStyle.css">
    </head>
    <body>
        <div class="container">
            <img src="Image/error-prod.png" alt="">
            <div href="" class="btn-back">
                <a href="home">Go Back To Home</a>
            </div>
        </div>
    </body> 
</html>
