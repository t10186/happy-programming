<%-- 
    Document   : accountdetail
    Created on : Jun 18, 2022, 1:06:28 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
    </head>
    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <!--Header Section Starts Here-->
                <!--/Header-->

                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="forms"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <form action="update-information-user" method="post">
                            <div class="flex flex-col">
                                <!-- Card Sextion Starts Here -->
                                <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                    <!--Horizontal form-->
                                    <div
                                        class="mb-2 border-solid border-grey-light rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-300 px-2 py-3 border-solid border-gray-400 border-b">
                                            Thông tin cá nhân
                                        </div>
                                        <div class="p-3">
                                            <form action="" method="" class="w-full">
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-gray-500 font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-full-name">
                                                            Tài khoản
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-full-name" type="text" value="${account.getAccount().username}" readonly
                                                            >
                                                    </div>
                                                </div>
                                                <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Email
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-username" type="text" value="${account.getAccount().email}" readonly>
                                                    </div>
                                                </div>
                                                    <div class="md:flex md:items-center mb-6">
                                                    <div class="md:w-1/4">
                                                        <label
                                                            class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                            for="inline-username">
                                                            Display Name
                                                        </label>
                                                    </div>
                                                    <div class="md:w-3/4">
                                                        <input
                                                            class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                            id="inline-username" type="text" value="${account.getInformation().displayName}" readonly>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--/Horizontal form-->

                                    <!--Underline form-->
                                    <div
                                        class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                                            Ảnh đại diện
                                        </div>
                                        <div class="p-3">
                                            <div class="w-full">
                                                <div class="flex items-center border-b border-b-1 border-teal-500 py-2">
                                                    <img style="width: 300px;border-radius: 50%;" src="Image/${account.getInformation().avatar}" alt="">
                                                </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <!--/Underline form-->
                                </div>
                                <!-- /Cards Section Ends Here -->

                                <!--Grid Form-->

                                <!-- Đăng ký -->
                                <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                    <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full"> 
                                        <div class="p-3">
                                            <div class="w-full">
                                                <div class="flex flex-wrap -mx-3 mb-2">
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-city">
                                                            name 
                                                        </label>
                                                        <input
                                                            class="appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                            id="grid-city" type="text" placeholder="name" name="fullname" value="${account.getInformation().fullName}"readonly>
                                                        
                                                    </div>
                                                    <!-- Giới tính -->
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-state">
                                                            Giới tính
                                                        </label>
                                                        <div class="">
                                                            <div
                                                                class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                                id="grid-state" name="gender">
                                                                <p value="${account.getInformation().gender}">${account.getInformation().gender?'Nữ':'Nam'}</p>
                                                               

                                                            </div>
                                                            <div
                                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                                                                <svg class="fill-current h-4 w-4"
                                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                                <path
                                                                    d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                                </svg>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <!-- Ngày sinh -->
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-state">
                                                            Ngày sinh
                                                        </label>
                                                        <div class="">
                                                            <input
                                                                class="appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                                id="" type="date"name="ÐOB"
                                                                value="${account.getInformation().DOB}" readonly >
                                                        </div>
                                                    </div>

                                                    <!-- Số điện thoại -->
                                                </div>
                                                <div class="flex flex-wrap -mx-3 mb-2">

                                                    <!-- Địa chỉ -->
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-city">
                                                            Địa chỉ

                                                        </label>
                                                        <input
                                                            class="appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                            id="grid-city" type="text" name="address" value="${account.getInformation().address}" readonly>
                                                       
                                                    </div>

                                                    <!-- Quốc gia -->
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-state">
                                                            Quốc gia
                                                        </label>
                                                        <div class="">
                                                            <input class="block appearance-none w-full bg-grey-200 border border-grey-200 text-grey-darker py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                                id="grid-state" name="country" value="${account.getCountry().name}" readonly>
                                                                 
                                                           
                                                            <div
                                                                class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-grey-darker">
                                                                <svg class="fill-current h-4 w-4"
                                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                                <path
                                                                    d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                                        <label
                                                            class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                                            for="grid-zip">
                                                            Số điện thoại
                                                        </label>
                                                        <input
                                                            class="appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                            id="grid-zip" type="text" placeholder="Số điện thoại" name="phone" value="${account.getInformation().phone}" readonly>
                                                    </div> 
                                                </div>
                                                <div class="flex flex-wrap -mx-3 mb-2">
                                                   
                                                </div>
                                                <div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/Grid Form-->
                                    </div>
                                    </form> 
                                    </main>
                                    <!--/Main-->
                                </div>
                                <!--Footer-->
                                <footer class="bg-grey-darkest text-white p-2">
                                    <div class="flex flex-1 mx-auto"> </div>
                                </footer>
                                <!--/footer-->
                                <%@include file="footer.jsp" %>
                            </div>

                            </div>
                            <script src="JS/main.js"></script>
                            </body> 
                            </html> 
