<%-- 
    Document   : quiz-handle
    Created on : Jun 4, 2022, 1:34:49 PM
    Author     : Phong Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" type="text/css" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <title>Question ${questionIndex+1}</title>
        <style>
            html, body {
                height: 100vh;
            }
            .custom-radio {
                cursor: pointer;
                display: flex;
                align-items: center;
            }
            .custom-radio input {
                position: absolute;
                opacity: 0;
            }
            .checkmark {
                position: relative;
                top: 0;
                left: 0;
                height: 25px;
                width: 25px;
                background-color: #eee;
                border-radius: 50%;
                margin: 5px 15px 5px 5px;
            }
            .custom-radio:hover input ~ .checkmark {
                background-color: #ccc;
            }
            .custom-radio input:checked ~ .checkmark {
                background-color: #1770ff;
            }
            .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            .custom-radio input:checked ~ .checkmark:after {
                display: block;
            }

            .custom-radio .checkmark:after {
                top: 9px;
                left: 9px;
                width: 8px;
                height: 8px;
                border-radius: 50%;
                background: white;
            }
            .pickBtn {
                background-color: #696969 !important;
                color: white !important;
            }
            #questionTable button {
                width: 6vmin;
                height: 6vmin;
                font-size: 2.5vmin;
                background-color: white;
                border: 1px solid black;
                border-radius: 1vmin;
                margin: 0.5vmin;
                position: relative;
            }
            .box {
                float: left;
                height: 20px;
                width: 20px;
                align-self: center;
                margin-right: 2px;
                border: 1px solid black;
                clear: both;
            }
            #questionTable button.answered,
            .box.answered {
                background-color: #999999 !important;
            }
            button .mark {
                position: absolute;
                top: -2vmin;
                right: 0;
            }
            .bi-bookmark-fill {
                background-color: transparent !important;
                color: #f05000;
            }
            .correct {
                background-color: #26bf4c !important;
            }
            .wrong {
                background-color: red !important;
            }
        </style>
    </head>
    <body class="bg-light">
        <script src="JS/quizHandle.js"></script>
<!--        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <div class="container-fluid d-flex flex-column" style="height:100%;width:100%;">
            <header class="row my-1 p-1 bg-white border-top border-bottom justify-content-end">
                <div class="flex h3 mx-2 my-auto p-2">
                    <i class="bi bi-geo-alt"></i> <p class="d-inline" id="questionProgress">1 / 20</p>
                </div>
                <div class="flex h3 mx-2 my-auto p-2" id="timerMain" style="background-color: #00fff2;">
                    <i class="bi bi-hourglass-split"></i> <p class="d-inline" id="timerValue">00:30:00</p>
                </div>
            </header>
            <div class="m-auto row flex-grow-1 w-100">
                <div class="card m-1 flex-grow-1" style="max-width:100%;">
                    <div class="card-header text-white bg-dark h4" id="questionHeader">
                        QUESTION 1
                    </div>
                    <div class="card-body bg-white overflow-y-auto">
                        <div class="row m-2 h4" id="questionContent">
                            Question:
                        </div>
                        <div class="col p-2 pl-5" id="answerContent">
                            <label class="row custom-radio">
                                <input type="radio" name="answer" id="A1"/>
                                <span class="checkmark"></span>
                                <text>A. AAAAAAAAAAA</text>
                            </label>
                            <label class="row custom-radio d-flex">
                                <input type="radio" name="answer" id="A2" checked/>
                                <span class="checkmark correct"></span>B. BBBBBBBBB
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-2 justify-content-end" id="notReviewPart">
                <button class="btn btn-outline-dark mx-1" onclick="clearAnsw()">
                    Clear Answer
                </button>
                <button type="button" class="btn btn-outline-success mx-1" data-toggle="modal" data-target="#peakAnswerModal">
                    Peak at Answer
                </button>
                <button class="btn mx-1 rounded text-white p-2" style="background-color: #26bf4c;" onclick="mark()">
                    <i class="bi bi-bookmark-fill"></i> <text id="markText">Mark for review</text>
                </button>
            </div>
            <footer class="row p-2 mt-auto d-flex justify-content-between" style="background-color: #26bf4c;">
                <div class="d-flex p-2">
                    <button class="btn border-white rounded text-white" id="reviewBtn" data-toggle="modal" data-target="#reviewModal">Review Progress</button>
                    <button class="btn btn-light border-danger rounded text-danger mx-2" id="exitBtn" type="submit" form="exitForm">Exit Review</button>
                    <form id="exitForm" method="POST" action="quiz-lesson"><input type="number" hidden name="QuizId" value="${QuizId}"/></form>
                </div>
                <div class="d-inline-flex p-2">
                    <button class="btn border-white rounded text-white mx-1" data-toggle="modal" data-target="#peakAnswerModal" id="explainBtn2">Explanation</button>
                    <div class="mx-1" style="min-width: 10vw;"><button class="btn border-white rounded text-white w-100" id="prevBtn" onclick="prevQ()">Previous</button></div>
                    <div class="mx-1" style="min-width: 10vw;"><button class="btn border-white rounded text-white w-100" id="nextBtn" onclick="nextQ()">Next</button></div>
                </div>
            </footer>
        </div>
        <!--        MOdal-->
        <div class="modal fade border border-dark" id="peakAnswerModal" tabindex="-1" role="dialog" aria-labelledby="peakAnswerTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <text class="modal-title h4" id="peakAnswerTitle">Explanation</text>
                        <button type="button" class="close d-inline" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="bi bi-x-circle w-100"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="w-100" id="correctAnswer">The Correct Answer is YOUR MOM.</div><br>
                        <div id="explanationPanel"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content p-3" style="min-height: 80vh;">
                    <div class="d-block my-2">
                        <b class="h4" id="reviewTitle">Review Progress</b>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="bi bi-x-circle"></i></span>
                        </button>
                    </div>
                    <div class="font-weight-light my-2" id="reviewDesc">
                        Review progress before submit...
                    </div>
                    <div class="">
                        <div class="d-block">
                            <button class="rounded bg-white border border-dark p-1" id="unanswerBtn">
                                <div class="box"></div>UNANSWERED
                            </button>
                            <button class="rounded bg-white text-capitalize border border-dark p-1">
                                <i class="bi bi-bookmark-fill"></i>MARKED
                            </button>
                            <button class="rounded bg-white text-capitalize border border-dark p-1">
                                <div class="box answered"></div>ANSWERED
                            </button>
                            <button class="rounded bg-white text-capitalize border border-dark p-1" id="incorrectBtn">
                                <div class="box"></div>INCORRECT
                            </button>
                            <button class="rounded bg-white text-capitalize border border-dark p-1 pickBtn">
                                ALL QUESTIONS
                            </button>
                            <button class="bg-white text-capitalize border border-3 border-primary py-1 px-2 float-right font-weight-bold" id="submitBtn" onclick="onSubmitting()">
                                SUBMIT NOW
                            </button>
                        </div>
                    </div>
                    <div id="questionTable" class="d-block my-3 mx-3 overflow-y-auto">
                        <button>1</button>
                        <button class="answered">999<i class="bi bi-bookmark-fill mark"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="submitConfirmModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title h5" id="submitTitle">Submit Exam ?</h5>
                    </div>
                    <div class="modal-body" id="submitDesc">
                        Are you sure to submit ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal"><i class="bi bi-arrow-left"></i> Back</button>
                        <button type="button" class="btn btn-outline-success" style="min-width: 10vw" onclick="finalizeQuiz()" id="scoreExamBtn">Score Exam</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            setEnvironment('${username}', ${QuizId}, ${startTime}, ${length}, ${isReview});
            setData(${questions}, ${questionIndex});
        </script>
    </body>
</html>