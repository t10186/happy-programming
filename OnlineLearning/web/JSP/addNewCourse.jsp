<%-- 
    Document   : accountdetail
    Created on : Jun 18, 2022, 1:06:28 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
        <style>
            .requiment{
                width: 476px;
                height: 187px;
            }
            .list-user{
                position: absolute;
                background-color: #80808094;
                width: 500px;
            }
            .list-user li{
                border: 1px solid white;
                cursor: pointer;
            }
            .list-user li:hover{
                background-color: #e4e1e1;
            }
        </style>
    </head>
    <body>
        <div class="mx-auto bg-grey-lightest">
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="forms"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <form action="addcourse" method="post">
                            <div class="flex flex-col"> 
                                <div style="margin-top: 5%;" class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                    <!--Horizontal form-->
                                    <div class="mb-2 border-solid border-grey-light rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-300 px-2 py-3 border-solid border-gray-400 border-b">
                                            Thông tin khoá học  
                                        </div>
                                        <div class="p-3">
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-gray-500 font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="inline-full-name">
                                                        Tên khoá học :
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="inline-full-name" type="text" name="title">
                                                </div>
                                            </div>
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="inline-username">
                                                        Mô tả khoá học :
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="inline-username" type="text" name="description">
                                                </div>
                                            </div>
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="inline-username">
                                                        Yêu cầu khoá học :
                                                        (- để phân cách)
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <textarea class="requiment" name="requirement">

                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="inline-username">
                                                        giá khóa học (vnd) :
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light" 
                                                           id="inline-username" type="number" name="price">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="md:flex md:items-center mb-6">
                                            <div class="md:w-1/4">
                                                <label
                                                    class="block text-gray-500 font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                    for="devoloper">
                                                    người phát triển (email) :
                                                </label>
                                            </div>
                                            <div class="md:w-3/4">
                                                <input
                                                    class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                    id="devoloper" type="text" name="EmailDevolop">
                                                <ul class="list-user">
                                                </ul>
                                            </div>
                                        </div> 
                                    </div>
                                    <div
                                        class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                        <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                                            Hình ảnh khoá học
                                        </div>
                                        <div class="p-3">
                                            <div class="w-full">   
                                                <div class="flex items-center border-b border-b-1 border-teal-500 py-2">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="inline-username" type="file" name="courseImage" placeholder="Đường dẫn hình ảnh">
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div sty class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                <label
                                    class="block uppercase tracking-wide text-grey-darker text-xs font-light mb-1"
                                    for="grid-state">
                                    <br>
                                </label>
                                <div class="">
                                    <button style="float:right;" class="btn btn-success" class="nav-link" href="#">thêm khóa học</button>
                                </div>
                            </div>
                        </form>  
                    </main>
                    <!--/Main-->
                </div>
                <!--Footer-->
                <footer class="bg-grey-darkest text-white p-2">
                    <div class="flex flex-1 mx-auto"> </div>
                </footer>
                <!--/footer-->
                <%@include file="footer.jsp" %>
            </div>

        </div>
        <script src="JS/main.js"></script>
        <script>
            var arrData = [];
            <c:forEach items="${listEmailExpert}" var="item">
            arrData.push('${item}');
            </c:forEach>
            var inputDevoloper = document.getElementById('devoloper');
            inputDevoloper.addEventListener('input', () => {
                arrDateNew = [];
                for (let i = 0; i < arrData.length && i < 5; i++) {
                    if (arrData[i].includes(inputDevoloper.value)) {
                        arrDateNew.push(arrData[i]);
                    }
                }
                loadingData(arrDateNew);
            });
            function loadingData(arrDataSrc) {
                var listView = document.getElementsByClassName('list-user')[0];
                listView.innerHTML = '';
                for (let i = 0; i < arrDataSrc.length; i++) {
                    var li = document.createElement('li');
                    li.appendChild(document.createTextNode(arrDataSrc[i]));
                    listView.appendChild(li);
                }
                document.querySelectorAll('.list-user li').forEach((element) => {
                    element.addEventListener('click', () => {
                        document.getElementById('devoloper').value = element.innerHTML;
                    });
                });
            }
            $(document).click((event) => {
                if (!$(event.target).closest(inputDevoloper).length) {
                    loadingData([]);
                }
            });
        </script>
    </body> 
</html> 
