<%-- 
    Document   : home
    Created on : May 20, 2022, 11:36:16 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/home.css">
        <link rel="stylesheet" href="CSS/backend.css">
        <link rel="stylesheet" href="CSS/backend-plugin.min.css">



        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>HOME</title> 
        <style>

            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
            .box{
                height:100%;
                width:100%;
                padding:15px;
                overflow-x:scroll;
            }
            .sub-box{
                height:150px;
                width:450px;
            }
            #myVideo {
                position: fixed;
                right: 0;
                bottom: 0;
                min-width: 100%;
                min-height: 100%;
            }

            .content {
                position: fixed;
                top: 20%;
                background: rgba(0, 0, 0, 0.5);
                color: #f1f1f1;
                width: 100%;
                padding: 20px;
            }

            #myBtn {
                width: 200px;
                font-size: 18px;
                padding: 10px;
                border: none;
                background: #000;
                color: #fff;
                cursor: pointer;
            }

            #myBtn:hover {
                background: #ddd;
                color: black;
            }


        </style>
    </head>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="home"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3">
                        <div class="flex flex-col">

                            <video autoplay muted loop id="myVideo">
                                <source src="To Paint - 4043.mp4">
                                Your browser does not support HTML5 video.
                            </video>

                            <div class="content">
                                <h1>Heading</h1>
                                <p>Lorem ipsum dolor sit amet, an his etiam torquatos. Tollit soleat phaedrum te duo, eum cu recteque expetendis neglegentur. Cu mentitum maiestatis persequeris pro, pri ponderum tractatos ei. Id qui nemore latine molestiae, ad mutat oblique delicatissimi pro.</p>
                                <button id="myBtn" onclick="myFunction()">Pause</button>
                            </div>






                        </div>
                </div>
                </main>
            </div>
            <!--Footer-->
            <%@include file="footer.jsp" %>
            <!--/footer-->
        </div>
    </div>
    <script src="JS/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
                                    var exampleModal = document.getElementById('exampleModal')
                                    exampleModal.addEventListener('show.bs.modal', function (event) {
                                        // Button that triggered the modal
                                        var button = event.relatedTarget
                                        // Extract info from data-bs-* attributes
                                        var recipient = button.getAttribute('data-bs-whatever')
                                        // If necessary, you could initiate an AJAX request here
                                        // and then do the updating in a callback.
                                        //
                                        // Update the modal's content.
                                        var modalTitle = exampleModal.querySelector('.modal-title')
                                        var modalBodyInput = exampleModal.querySelector('.modal-body input')

                                        modalTitle.textContent = 'Message of Post '
                                        modalBodyInput.value = recipient
                                    })

                                    var video = document.getElementById("myVideo");
                                    var btn = document.getElementById("myBtn");

                                    function myFunction() {
                                        if (video.paused) {
                                            video.play();
                                            btn.innerHTML = "Pause";
                                        } else {
                                            video.pause();
                                            btn.innerHTML = "Play";
                                        }
                                    }



    </script>

</body>
</html>


