<%-- 
    Document   : listLesson
    Created on : Jun 15, 2022, 11:30:13 AM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
              crossorigin="anonymous">
        <base href="${path}">
        <style>
            .login{
                background: url('Image/login-new.jpeg')
            }
            .new{
                margin-top: 10px;
                margin-left: 23px;
                padding: 7px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .save{
                margin-top: 10px;
                padding: 5px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .font-bold{
                padding: 7px 26px;
            }
            .lesson{
                background-color: #f97521;
                border-radius: 4px;
                margin-bottom: 5px;
                cursor: pointer;
                height: 45px;
                display: flex;
                align-items: center;
            }
            .lesson-unit{
                padding: 10px 0px 10px 18px;
                color: white;
            }
            .lesson:hover{
                background-color: #f5975b;
            }
            .function{
                margin-top: 37px;
                display: flex;
                flex-wrap: wrap;
            }
            .control{
                display: none;
            }
            .active{
                background-color: #a35100;
            }
            .unit{
                width: 45px;
            }
        </style>
        <link rel="stylesheet" href="CSS/styles.css">
    </head>
    <body class="h-screen font-sans login bg-cover">
        <div id="toast"></div>
        <div class="container mx-auto h-full flex flex-1 justify-center items-center">
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <div class="max-w-xl m-4 p-10 bg-white rounded shadow-xl"
                         style="width: 700px">
                        <c:forEach items="${listLesson}" var="item">
                            <div class="lesson">
                                <p  style="display: none" name="lessonid">${item.id}</p>
                                <div class="unit">
                                    <p class="lesson-unit" >${item.unit}</p>
                                </div>
                                <p>${item.title}</p>
                            </div>
                        </c:forEach>
                        <div class="function">
                            <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" href="show-list-course-devoloper">
                                quay lại 
                            </a>
                            <a href="addnewlesson?courseID=${courseID}" type="submit" class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" >
                                thêm 
                            </a>                            
                            <div class="control" id="control">
                                <a  id="edit"class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new"
                                    href="">
                                    chỉnh sửa 
                                </a>
                                <a id="delete" class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" href="">
                                    xóa
                                </a>  
                            </div>
                            <div class="control" id="Quiz">
                                <a  id="editQuiz" class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new"
                                    href="">
                                    câu hỏi
                                </a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <script>
            document.addEventListener("DOMContentLoaded", function (event) {
                const linkColor = document.querySelectorAll('.lesson');
                function colorLink() {
                    if (linkColor) {
                        linkColor.forEach(l => l.classList.remove('active'))
                        this.classList.add('active');
                        var lessonID = this.childNodes[1].innerHTML
                        document.getElementById('edit').href = 'editlesson?lessonID=' + lessonID;
                        document.getElementById('delete').href = 'deletelesson?lessonid=' + lessonID;
                        document.getElementById('editQuiz').href = 'JSP/quizEditor.jsp?lessonID=' + lessonID;
                    }
                    document.getElementById('control').style.display = 'block';
                    document.getElementById('Quiz').style.display = 'block';
                }
                linkColor.forEach(l => l.addEventListener('click', colorLink));
            })
        </script>
    </body> 
</html>
