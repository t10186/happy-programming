<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Hai Dang
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
    </head>

    <body>
        <div class="mx-auto bg-grey-lightest">
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">

                    <!--Sidebar-->
                    <c:set var="linkColor" value="listUser"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->


                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <div class="content">
                            <div class="container">

                                <!--navbar-->
                                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav mr-auto">
                                            <li style="margin-right: 10px;" class="nav-item">
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        Lựa chọn khoá học
                                                    </button>
                                                    <ul style="width: 300px;" class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                        <c:forEach items="${listNameCourse}" var="c">
                                                            <li><a class="dropdown-item" href="show-list-lesson-course?courseid=${c.id}">${c.title}</a></li>                                                            
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li style="margin-right: 10px;" class="nav-item">
                                                <a class="btn btn-success" class="nav-link" href="update-course?course_id_infor=${course_id_info}">Thông tin chi tiết khoá học</a>
                                            </li>
                                            <form action="show-list-lesson-course" method="post">
                                                <li style="margin-right: 10px;" class="nav-item">

                                                    <button class="btn btn-success" class="nav-link" type="submit">Yêu cầu xuất bản (Mới)</a> </button>
                                                </li>
                                            </form>
                                        </ul>
                                        <form class="form-inline my-2 my-lg-0">
                                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                        </form>
                                    </div>
                                </nav>
                                <a class="btn" class="nav-link">Khoá học: ${course_title}</a>
                                <!--/navbar-->
                            </div> 
                            <br>

                            <!--Danh sách bài học-->
                            <div class="table-responsive">
                                <table class="table custom-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">Tên bài học</th>
                                            <th scope="col">Thời lượng</th>
                                            <th scope="col">Ngày đăng</th>
                                            <th scope="col">Trạng thái</th>
                                            <th scope="col">Chỉnh sửa chi tiết</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listLessonByCourse}" var="item">
                                            <tr scope="row">

                                                <td name = "accountid">
                                                    ${item.id}
                                                </td>
                                                <td>
                                                    ${item.title}
                                                </td>
                                                <td>
                                                    ${item.lessonTime}
                                                </td>
                                                <td>${item.dateCreate}</td>  
                                                <td> <div class="form-check form-switch">
                                                        <input name="lesson_status" class="form-check-input" type="checkbox" ${item.status?'checked=""':''}>
                                                    </div>
                                                </td>
                                                <td>
                                                    <form action="update-lesson" method="post">
                                                        <input type="hidden" name="lesson_id" value="${item.id}">
                                                        <button class="btn btn-sm btn-dark" type="submit">Thông tin chi tiết</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <!--/Danh sách bài học-->
                        </div>
                </div>
            </div>
            <script src="JS/main.js"></script> 
    </body> 
</html>