<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
        <style>
            .error{
                color: red;
                margin-left: 10px;
                font-size: 12px;
            }
        </style>
    </head>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <!--Header Section Starts Here-->

                <!--/Header-->

                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="ChangePassword"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">

                        <div class="flex flex-col">
                            <!-- Card Sextion Starts Here -->
                            <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2">
                                <!--Horizontal form-->
                                <div
                                    class="mb-2 border-solid border-grey-light rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                    <div class="bg-gray-300 px-2 py-3 border-solid border-gray-400 border-b">
                                        Thay đổi mật khẩu
                                    </div>
                                    <div class="p-3">
                                        <form action="ChangePasswordAccountController" method="POST" class="w-full">
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-gray-500 font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="inline-full-name">
                                                        Tài khoản
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="inline-full-name" type="text" value="${acc.username}"
                                                        >
                                                </div>
                                            </div>
                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="olderPassword">
                                                        Mật khẩu cũ
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="olderPassword" name="oldpassword" type="password">
                                                    <h5 class="error"></h5>
                                                </div>
                                            </div>

                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="newPass">
                                                        Mật khẩu mới
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="newPass" name="newpassword" type="password">
                                                    <h5 class="error"></h5>
                                                </div>
                                            </div>

                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-1/4">
                                                    <label
                                                        class="block text-grey font-regular md:text-right mb-1 md:mb-0 pr-4"
                                                        for="re-newPass">
                                                        Nhập lại mật khẩu mới
                                                    </label>
                                                </div>
                                                <div class="md:w-3/4">
                                                    <input
                                                        class="bg-grey-200 appearance-none border-1 border-grey-200 rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple-light"
                                                        id="re-newPass" name="renewpassword" type="password">
                                                    <h5 class="error"></h5>

                                                </div>
                                            </div>

                                            <div class="md:flex md:items-center mb-6">
                                                <div class="md:w-3/4">
                                                    <button
                                                        class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded appearance-none block w-full bg-grey-200 text-grey-darker border border-grey-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
                                                        id="grid-city" type="submit" name="submit" >Lưu thay đổi</button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <!--/Horizontal form-->

                                <!--Underline form-->
                                <div
                                    class="mb-2 md:mx-2 lg:mx-2 border-solid border-gray-200 rounded border shadow-sm w-full md:w-1/2 lg:w-1/2">
                                    <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                                        Slide quảng cáo
                                    </div>
                                    <div class="p-3">
                                        <form class="w-full">
                                            <div class="flex items-center border-b border-b-1 border-teal-500 py-2">
                                                <img style="width: 300px;" src="../Image/48-Học-backend.png" alt="">
                                            </div>
                                            <div class="flex items-center border-b border-b-1 border-red-500 py-2">

                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--/Underline form-->
                            </div>
                            <!-- /Cards Section Ends Here -->

                            <!--Grid Form-->
                            <!--/Grid Form-->
                        </div>
                    </main>
                    <!--/Main-->
                </div>
                <!--Footer-->
                <footer class="bg-grey-darkest text-white p-2">
                    <div class="flex flex-1 mx-auto"> </div>
                </footer>
                <!--/footer-->

            </div>

        </div>

        <script src="JS/main.js"></script>
        <script>

            document.querySelector('button').addEventListener('click', (event) => {
                const oldPass = document.forms[0].elements["olderPassword"].value;
                const newPass = document.forms[0].elements["newPass"].value;
                const reNewPass = document.forms[0].elements["re-newPass"].value;
                const arrError = document.querySelectorAll('.error');
                arrError.forEach(element => {element.innerHTML = ''})
               
                var password = '${acc.password}';
                 console.log(password);
                 console.log(oldPass);
                if (oldPass !== password) {
                    event.preventDefault();
                    arrError[0].innerHTML = '* mât khẩu cũ không chính xác ! vui lòng nhập lại';
                } else if (newPass.length < 8 ||  newPass.length > 30) {
                    event.preventDefault();
                    arrError[1].innerHTML = '* mật khẩu phải ít nhất 8 ký tự và nhiều nhất là 30 ký tự';
                } else if (newPass !== reNewPass) {
                    event.preventDefault();
                    arrError[2].innerHTML = '*  nhập lại mật khẩu sai ! vui lòng nhập lại';
                }
            });
        </script>

    </body> 
</html>
