<%-- 
    Document   : dashBoard.jsp
    Created on : Jul 25, 2022, 7:38:33 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- GOOGLE FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500"
              rel="stylesheet" />
        <base href="${path}">
        <!-- SLEEK CSS -->
        <link id="sleek-css" rel="stylesheet" href="CSS/sleek.css" /> 
    </head>
    <body class="header-fixed sidebar-fixed sidebar-dark header-light" id="body" style="background-color: #414c68;">
        <c:set value="0" var="totalAccRegis"/>
        <c:set value="0" var="totalAccRegisThisYear"/>
            <c:forEach begin="0" end="11" var="items">
                <c:set value="${totalAccRegis + DataAccRegis.get(items)}" var="totalAccRegis"/>
                <c:set value="${totalAccRegisThisYear + DataAccRegisThisYear.get(items)}" var="totalAccRegisThisYear"/>
            </c:forEach> 
            <!--Container -->
            <div class="mx-auto bg-grey-400">
                <!--Screen-->
                <div class="min-h-screen flex flex-col">
                    <div class="flex flex-1">
                        <!--Sidebar-->
                        <c:set var="linkColor" value="dashBoard"/>
                        <%@include file="sidebar.jsp" %>
                        <!--/Sidebar-->
                        <!--Main-->
                        <main class="bg-white-300 flex-1 p-3 overflow-hidden">
                            <div class="">
                                <div class="content-wrapper">
                                    <div class="content">
                                        <!-- Top Statistics -->
                                        <div class="row">
                                            <div class="col-xl-3 col-sm-6">
                                                <div class="card card-mini mb-4">
                                                    <div class="card-body">
                                                        <h2 class="mb-1"><c:out value="${totalAccRegis}"/></h2>
                                                        <p>tông số người đăng ký</p>
                                                        <div class="chartjs-wrapper">
                                                            <canvas id="barChart"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-sm-6">
                                                <div class="card card-mini  mb-4">
                                                    <div class="card-body">
                                                        <h2 class="mb-1">${totalAccRegisThisYear}</h2>
                                                        <p>số người đăng ký trong năm</p>
                                                        <div class="chartjs-wrapper">
                                                            <canvas id="dual-line"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-sm-6">
                                                <div class="card card-mini mb-4">
                                                    <div class="card-body">
                                                        <h2 class="mb-1">23</h2>
                                                        <p>tổng số khóa học đã bán</p>
                                                        <div class="chartjs-wrapper">
                                                            <canvas id="area-chart"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-sm-6">
                                                <div class="card card-mini mb-4">
                                                    <div class="card-body">
                                                        <h2 class="mb-1">22</h2>
                                                        <p>số khóa học đã bán trong năm</p>
                                                        <div class="chartjs-wrapper">
                                                            <canvas id="line"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-12 col-md-12">
                                                <!-- Sales Graph -->
                                                <div class="card card-default">
                                                    <div class="card-header">
                                                        <h2>Doanh thu năm nay</h2>
                                                    </div>
                                                    <div class="card-body">
                                                        <canvas id="linechart" class="chartjs"></canvas>
                                                    </div>
                                                    <div class="card-footer d-flex flex-wrap bg-white p-0">
                                                        <div class="col-12 px-0">
                                                            <div class="text-center p-4 border-left">
                                                                <h4>24.600</h4>
                                                                <p class="mt-2">lợi nhuận</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12">
                                                <!-- Top Products -->
                                                <div class="card card-default">
                                                    <div class="card-header justify-content-between mb-4">
                                                        <h2>Top Products</h2>
                                                        <div>
                                                            <button class="text-black-50 mr-2 font-size-20"><i class="mdi mdi-cached"></i></button>
                                                            <div class="dropdown show d-inline-block widget-dropdown">
                                                                <a class="dropdown-toggle icon-burger-mini" href="#" role="button" id="dropdown-product"
                                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                                                                </a>
                                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-product">
                                                                    <li class="dropdown-item"><a href="#">Update Data</a></li>
                                                                    <li class="dropdown-item"><a href="#">Detailed Log</a></li>
                                                                    <li class="dropdown-item"><a href="#">Statistics</a></li>
                                                                    <li class="dropdown-item"><a href="#">Clear Data</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="card-body py-0">
                                                        <div class="media d-flex mb-5">
                                                            <div class="media-image align-self-center mr-3 rounded">
                                                                <a href="#"><img src="assets/img/products/p1.jpg" alt="customer image"></a>
                                                            </div>
                                                            <div class="media-body align-self-center">
                                                                <a href="#">
                                                                    <h6 class="mb-3 text-dark font-weight-medium"> Coach Swagger</h6>
                                                                </a>
                                                                <p class="float-md-right"><span class="text-dark mr-2">20</span>Sales</p>
                                                                <p class="d-none d-md-block">Statement belting with double-turnlock hardware adds “swagger” to a
                                                                    simple.</p>
                                                                <p class="mb-0">
                                                                    <del>$300</del>
                                                                    <span class="text-dark ml-3">$250</span>
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="media d-flex mb-5">
                                                            <div class="media-image align-self-center mr-3 rounded">
                                                                <a href="#"><img src="assets/img/products/p2.jpg" alt="customer image"></a>
                                                            </div>
                                                            <div class="media-body align-self-center">
                                                                <a href="#">
                                                                    <h6 class="mb-3 text-dark font-weight-medium"> Coach Swagger</h6>
                                                                </a>
                                                                <p class="float-md-right"><span class="text-dark mr-2">20</span>Sales</p>
                                                                <p class="d-none d-md-block">Statement belting with double-turnlock hardware adds “swagger” to a
                                                                    simple.</p>
                                                                <p class="mb-0">
                                                                    <del>$300</del>
                                                                    <span class="text-dark ml-3">$250</span>
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="media d-flex mb-5">
                                                            <div class="media-image align-self-center mr-3 rounded">
                                                                <a href="#"><img src="assets/img/products/p3.jpg" alt="customer image"></a>
                                                            </div>
                                                            <div class="media-body align-self-center">
                                                                <a href="#">
                                                                    <h6 class="mb-3 text-dark font-weight-medium"> Gucci Watch</h6>
                                                                </a>
                                                                <p class="float-md-right"><span class="text-dark mr-2">10</span>Sales</p>
                                                                <p class="d-none d-md-block">Statement belting with double-turnlock hardware adds “swagger” to a
                                                                    simple.</p>
                                                                <p class="mb-0">
                                                                    <del>$300</del>
                                                                    <span class="text-dark ml-3">$50</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-6 col-12">

                                                <!-- Top Sell Table -->
                                                <div class="card card-table-border-none">
                                                    <div class="card-header justify-content-between">
                                                        <h2>Sold by Units</h2>
                                                        <div>
                                                            <button class="text-black-50 mr-2 font-size-20"><i class="mdi mdi-cached"></i></button>
                                                            <div class="dropdown show d-inline-block widget-dropdown">
                                                                <a class="dropdown-toggle icon-burger-mini" href="#" role="button" id="dropdown-units"
                                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-units">
                                                                    <li class="dropdown-item"><a href="#">Action</a></li>
                                                                    <li class="dropdown-item"><a href="#">Another action</a></li>
                                                                    <li class="dropdown-item"><a href="#">Something else here</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body py-0 compact-units" data-simplebar style="height: 384px;">
                                                        <table class="table ">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="text-dark">Backpack</td>
                                                                    <td class="text-center">9</td>
                                                                    <td class="text-right">33% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">T-Shirt</td>
                                                                    <td class="text-center">6</td>
                                                                    <td class="text-right">150% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">Coat</td>
                                                                    <td class="text-center">3</td>
                                                                    <td class="text-right">50% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">Necklace</td>
                                                                    <td class="text-center">7</td>
                                                                    <td class="text-right">150% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">Jeans Pant</td>
                                                                    <td class="text-center">10</td>
                                                                    <td class="text-right">300% <i class="mdi mdi-arrow-down-bold text-danger pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">Shoes</td>
                                                                    <td class="text-center">5</td>
                                                                    <td class="text-right">100% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="text-dark">T-Shirt</td>
                                                                    <td class="text-center">6</td>
                                                                    <td class="text-right">150% <i class="mdi mdi-arrow-up-bold text-success pl-1 font-size-12"></i>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="card-footer bg-white py-4">
                                                        <a href="#" class="btn-link py-3 text-uppercase">View Report</a>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-xl-4 col-12">

                                                <!-- Notification Table -->
                                                <div class="card card-default">
                                                    <div class="card-header justify-content-between mb-1">
                                                        <h2>Latest Notifications</h2>
                                                        <div>
                                                            <button class="text-black-50 mr-2 font-size-20"><i class="mdi mdi-cached"></i></button>
                                                            <div class="dropdown show d-inline-block widget-dropdown">
                                                                <a class="dropdown-toggle icon-burger-mini" href="#" role="button" id="dropdown-notification"
                                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-notification">
                                                                    <li class="dropdown-item"><a href="#">Action</a></li>
                                                                    <li class="dropdown-item"><a href="#">Another action</a></li>
                                                                    <li class="dropdown-item"><a href="#">Something else here</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="card-body compact-notifications" data-simplebar style="height: 434px;">
                                                        <div class="media pb-3 align-items-center justify-content-between">
                                                            <div
                                                                class="d-flex rounded-circle align-items-center justify-content-center mr-3 media-icon iconbox-45 bg-primary text-white">
                                                                <i class="mdi mdi-cart-outline font-size-20"></i>
                                                            </div>
                                                            <div class="media-body pr-3 ">
                                                                <a class="mt-0 mb-1 font-size-15 text-dark" href="#">New Order</a>
                                                                <p>Selena has placed an new order</p>
                                                            </div>
                                                            <span class=" font-size-12 d-inline-block"><i class="mdi mdi-clock-outline"></i> 10 AM</span>
                                                        </div>

                                                        <div class="media py-3 align-items-center justify-content-between">
                                                            <div
                                                                class="d-flex rounded-circle align-items-center justify-content-center mr-3 media-icon iconbox-45 bg-success text-white">
                                                                <i class="mdi mdi-email-outline font-size-20"></i>
                                                            </div>
                                                            <div class="media-body pr-3">
                                                                <a class="mt-0 mb-1 font-size-15 text-dark" href="#">New Enquiry</a>
                                                                <p>Phileine has placed an new order</p>
                                                            </div>
                                                            <span class=" font-size-12 d-inline-block"><i class="mdi mdi-clock-outline"></i> 9 AM</span>
                                                        </div>


                                                        <div class="media py-3 align-items-center justify-content-between">
                                                            <div
                                                                class="d-flex rounded-circle align-items-center justify-content-center mr-3 media-icon iconbox-45 bg-warning text-white">
                                                                <i class="mdi mdi-stack-exchange font-size-20"></i>
                                                            </div>
                                                            <div class="media-body pr-3">
                                                                <a class="mt-0 mb-1 font-size-15 text-dark" href="#">Support Ticket</a>
                                                                <p>Emma has placed an new order</p>
                                                            </div>
                                                            <span class=" font-size-12 d-inline-block"><i class="mdi mdi-clock-outline"></i> 10 AM</span>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3"></div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    </main>
                                    <!--/Main-->
                                </div>
                                <!--Footer-->
                                <%@include file="footer.jsp" %>
                                <!--/footer-->
                            </div>
                    </div> 
                    <!-- End Content -->
                </div> <!-- End Content Wrapper -->
                <script src='JS/chart.min.js'></script>
                <script src='JS/main.js'></script>
                <script>
                    /*======== 20. BAR CHART ========*/
                    var phaseBarx = [];
                    <c:forEach items="${PhaseAccRegis}" var="items">
                    phaseBarx.push('${items}');
                    </c:forEach>
                    var dataBarx = [];
                    <c:forEach items="${DataAccRegis}" var="items">
                    dataBarx.push('${items}');
                    </c:forEach>
                    var barX = document.getElementById("barChart");
                    if (barX !== null) {
                        var areaChart = new Chart(barX, {
                            type: "line",
                            data: {
                                labels: phaseBarx,
                                datasets: [
                                    {
                                        label: "New",
                                        pointHitRadius: 10,
                                        pointRadius: 0,
                                        fill: true,
                                        backgroundColor: "rgba(76, 132, 255, 0.9)",
                                        borderColor: "rgba(76, 132, 255, 0.9)",
                                        data: dataBarx
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                layout: {
                                    padding: {
                                        right: 10
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false,
                                                display: false
                                            },
                                            ticks: {
                                                display: false, // hide main x-axis line
                                                beginAtZero: true
                                            },
                                            barPercentage: 1.8,
                                            categoryPercentage: 0.2
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false, // hide main y-axis line
                                                display: false
                                            },
                                            ticks: {
                                                display: false,
                                                beginAtZero: true
                                            }
                                        }
                                    ]
                                },
                                tooltips: {
                                    titleFontColor: "#888",
                                    bodyFontColor: "#555",
                                    titleFontSize: 12,
                                    bodyFontSize: 14,
                                    backgroundColor: "rgba(256,256,256,0.95)",
                                    displayColors: true,
                                    borderColor: "rgba(220, 220, 220, 0.9)",
                                    borderWidth: 2
                                }
                            }
                        });
                    }


                    /*======== 1. DUAL LINE CHART ========*/
                    var datadual = [];
                    <c:forEach items="${DataAccRegisThisYear}" var="items">
                    datadual.push('${items}');
                    </c:forEach>
                    var dual = document.getElementById("dual-line");
                    if (dual !== null) {
                        var areaChart = new Chart(dual, {
                            type: "line",
                            data: {
                                labels: phaseBarx,
                                datasets: [
                                    {
                                        label: "New",
                                        pointHitRadius: 10,
                                        pointRadius: 0,
                                        fill: true,
                                        backgroundColor: "rgba(76, 132, 255, 0.9)",
                                        borderColor: "rgba(76, 132, 255, 0.9)",
                                        data: datadual
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                layout: {
                                    padding: {
                                        right: 10
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false,
                                                display: false
                                            },
                                            ticks: {
                                                display: false, // hide main x-axis line
                                                beginAtZero: true
                                            },
                                            barPercentage: 1.8,
                                            categoryPercentage: 0.2
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false, // hide main y-axis line
                                                display: false
                                            },
                                            ticks: {
                                                display: false,
                                                beginAtZero: true
                                            }
                                        }
                                    ]
                                },
                                tooltips: {
                                    titleFontColor: "#888",
                                    bodyFontColor: "#555",
                                    titleFontSize: 12,
                                    bodyFontSize: 14,
                                    backgroundColor: "rgba(256,256,256,0.95)",
                                    displayColors: true,
                                    borderColor: "rgba(220, 220, 220, 0.9)",
                                    borderWidth: 2
                                }
                            }
                        });
                    }

                    /*======== 6. AREA CHART ========*/
                    var area = document.getElementById("area-chart");
                    if (area !== null) {
                        var areaChart = new Chart(area, {
                            type: "line",
                            data: {
                                labels: ["Fri", "Sat", "Sun", "Mon", "Tue", "Wed", "Thu"],
                                datasets: [
                                    {
                                        label: "Old",
                                        pointHitRadius: 10,
                                        pointRadius: 0,
                                        fill: true,
                                        backgroundColor: "rgba(253, 197, 6, 0.9)",
                                        borderColor: "rgba(253, 197, 6, 1)",
                                        data: [0, 2, 4.3, 3.8, 5.2, 1.8, 2.2]
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                layout: {
                                    padding: {
                                        right: 10
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false,
                                                display: false
                                            },
                                            ticks: {
                                                display: false, // hide main x-axis line
                                                beginAtZero: true
                                            },
                                            barPercentage: 1.8,
                                            categoryPercentage: 0.2
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false, // hide main y-axis line
                                                display: false
                                            },
                                            ticks: {
                                                display: false,
                                                beginAtZero: true
                                            }
                                        }
                                    ]
                                },
                                tooltips: {
                                    titleFontColor: "#888",
                                    bodyFontColor: "#555",
                                    titleFontSize: 12,
                                    bodyFontSize: 14,
                                    backgroundColor: "rgba(256,256,256,0.95)",
                                    displayColors: true,
                                    borderColor: "rgba(220, 220, 220, 0.9)",
                                    borderWidth: 2
                                }
                            }
                        });
                    }
                    var line = document.getElementById("line");
                    if (line !== null) {
                        var areaChart = new Chart(line, {
                            type: "line",
                            data: {
                                labels: ["Fri", "Sat", "Sun", "Mon", "Tue", "Wed", "Thu"],
                                datasets: [
                                    {
                                        label: "Old",
                                        pointHitRadius: 10,
                                        pointRadius: 0,
                                        fill: true,
                                        backgroundColor: "rgba(253, 197, 6, 0.9)",
                                        borderColor: "rgba(253, 197, 6, 1)",
                                        data: [0, 2, 4.3, 3.8, 5.2, 1.8, 2.2]
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                layout: {
                                    padding: {
                                        right: 10
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false,
                                                display: false
                                            },
                                            ticks: {
                                                display: false, // hide main x-axis line
                                                beginAtZero: true
                                            },
                                            barPercentage: 1.8,
                                            categoryPercentage: 0.2
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            gridLines: {
                                                drawBorder: false, // hide main y-axis line
                                                display: false
                                            },
                                            ticks: {
                                                display: false,
                                                beginAtZero: true
                                            }
                                        }
                                    ]
                                },
                                tooltips: {
                                    titleFontColor: "#888",
                                    bodyFontColor: "#555",
                                    titleFontSize: 12,
                                    bodyFontSize: 14,
                                    backgroundColor: "rgba(256,256,256,0.95)",
                                    displayColors: true,
                                    borderColor: "rgba(220, 220, 220, 0.9)",
                                    borderWidth: 2
                                }
                            }
                        });
                    }

                    /*======== 3. LINE CHART ========*/
                    var ctx = document.getElementById("linechart");
                    if (ctx !== null) {
                        var chart = new Chart(ctx, {
                            // The type of chart we want to create
                            type: "line",

                            // The data for our dataset
                            data: {
                                labels: [
                                    "Jan",
                                    "Feb",
                                    "Mar",
                                    "Apr",
                                    "May",
                                    "Jun",
                                    "Jul",
                                    "Aug",
                                    "Sep",
                                    "Oct",
                                    "Nov",
                                    "Dec"
                                ],
                                datasets: [
                                    {
                                        label: "",
                                        backgroundColor: "transparent",
                                        borderColor: "rgb(82, 136, 255)",
                                        data: [
                                            100,
                                            11000,
                                            10000,
                                            14000,
                                            11000,
                                            17000,
                                            14500,
                                            18000,
                                            5000,
                                            23000,
                                            14000,
                                            19000
                                        ],
                                        lineTension: 0.3,
                                        pointRadius: 5,
                                        pointBackgroundColor: "rgba(255,255,255,1)",
                                        pointHoverBackgroundColor: "rgba(255,255,255,1)",
                                        pointBorderWidth: 2,
                                        pointHoverRadius: 8,
                                        pointHoverBorderWidth: 1
                                    }
                                ]
                            },

                            // Configuration options go here
                            options: {
                                responsive: true,
                                maintainAspectRatio: false,
                                legend: {
                                    display: false
                                },
                                layout: {
                                    padding: {
                                        right: 10
                                    }
                                },
                                scales: {
                                    xAxes: [
                                        {
                                            gridLines: {
                                                display: false
                                            }
                                        }
                                    ],
                                    yAxes: [
                                        {
                                            gridLines: {
                                                display: true,
                                                color: "#eee",
                                                zeroLineColor: "#eee",
                                            },
                                            ticks: {
                                                callback: function (value) {
                                                    var ranges = [
                                                        {divider: 1e6, suffix: "M"},
                                                        {divider: 1e4, suffix: "k"}
                                                    ];
                                                    function formatNumber(n) {
                                                        for (var i = 0; i < ranges.length; i++) {
                                                            if (n >= ranges[i].divider) {
                                                                return (
                                                                        (n / ranges[i].divider).toString() + ranges[i].suffix
                                                                        );
                                                            }
                                                        }
                                                        return n;
                                                    }
                                                    return formatNumber(value);
                                                }
                                            }
                                        }
                                    ]
                                },
                                tooltips: {
                                    callbacks: {
                                        title: function (tooltipItem, data) {
                                            return data["labels"][tooltipItem[0]["index"]];
                                        },
                                        label: function (tooltipItem, data) {
                                            return "$" + data["datasets"][0]["data"][tooltipItem["index"]];
                                        }
                                    },
                                    responsive: true,
                                    intersect: false,
                                    enabled: true,
                                    titleFontColor: "#888",
                                    bodyFontColor: "#555",
                                    titleFontSize: 12,
                                    bodyFontSize: 18,
                                    backgroundColor: "rgba(256,256,256,0.95)",
                                    xPadding: 20,
                                    yPadding: 10,
                                    displayColors: false,
                                    borderColor: "rgba(220, 220, 220, 0.9)",
                                    borderWidth: 2,
                                    caretSize: 10,
                                    caretPadding: 15
                                }
                            }
                        });
                    }
                </script>
        </body>
    </html>
