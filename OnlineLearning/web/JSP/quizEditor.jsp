<%-- 
    Document   : quizCreate
    Created on : Jun 16, 2022, 8:59:24 PM
    Author     : Phong Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <link rel="stylesheet" type="text/css" href="CSS/all.css">
        <link rel="stylesheet" type="text/css" href="CSS/loading.css">
        <title>Quiz Editor</title>
    </head>
    <body>
        <style>
            details {
                overflow: hidden;
            }
            details summary {
                position: relative;
                z-index: 10;
            }
            @keyframes details-show {
                from {
                    margin-bottom: -80%;
                    opacity: 0;
                    transform: translateY(-100%);
                }
            }
            details > *:not(summary) {
                animation: details-show 500ms ease-in-out;
                position: relative;
                z-index: 1;
                transition: all 0.3s ease-in-out;
                color: transparent;
                overflow: hidden;
            }
            details[open] > *:not(summary) {
                color: inherit;
            }
            /* # Style 7 # */
            details.style7 summary {
                padding-left: 2rem;
            }
            details[open].style7 summary,
            details.style7:hover summary {
                background: #000;
                color: #CCC;
            }
            details[open].style7 summary strong,
            details.style7:hover summary strong {
                color: #FDCE4C;
            }
            details.style7:hover summary strong {
                color: #ffdf87;
            }
            details .moon-new {
                display: inline;
            }
            details .moon-full {
                display: none;
            }
            details[open] .moon-new {
                display: none;
            }
            details[open] .moon-full {
                display: inline;
            }
            details.style7 .content {
                background: #DDD;
            }

            /* # Just Some Pretty Styles # */
            details.style7 {
                max-width: 100%;
                box-sizing: border-box;
                margin-top: 5px;
                background: white;
            }
            details.style7 summary {
                border: 2px solid transparent;
                outline: none;
                padding: 1rem;
                display: block;
                background: #666;
                color: white;
                position: relative;
                cursor: pointer;
                font-size: 1.5rem;
            }
            details.style7[open] summary,
            details.style7 summary:hover {
                color: #FFCA28;
                background: #444;
            }
            details.style7 summary:hover strong,
            details.style7[open] summary strong,
            details.style7 summary:hover::before,
            details.style7[open] summary::before {
                color: #FFA128;
            }
            .content {
                padding: 10px;
                border: 2px solid #888;
                border-top: none;
            }
            .editorPanel {
                min-height: 80%;
                max-height: 100%;
            }
            .selectQuestion {
                min-height: 3rem;
            }
            .selectAnswer {
                min-height: 2rem;
            }
            .selectAnswerPanel {
                min-height: 30%;
                max-height: 60%;
                overflow-y: auto;
            }
            textarea {
                box-sizing: border-box;
                resize: none;
            }
            .explanation {
                position: relative;
                display: flex;
                flex-direction: column;
                flex: 1;
            }
            details.explanation[open] {
                height: 10rem;
            }
            .explainInput {
                height: calc(100% - 2px - 2rem);
            }
            .explainInput textarea {
                height: 90%;
            }
            edittedQuestion:hover {
                background-color: #f9acaa !important;
            }
            edittedQuestion, edittedQuestion:hover {
                border-color: #e53e3e !important;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/html-duration-picker@latest/dist/html-duration-picker.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="JS/quizEditor.js"></script>
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="h-screen flex flex-col">
                <div class="pl-4 m-3 flex-row">
                    <details class="style7">
                        <summary>
                            <span claDesc">Description: </label>
                                        <textarea class="m-1 p-1 w-full" name="quizDesc" id="quizDesc" placeholdess="moon-new">
                                Quiz ID <strong>${quizID}</strong> <i class="mx-4 text-gray-400">Click to edit quiz's properties...</i>
                                <button class="bg-white hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500hover:border-transparent rounded" onclick="askDeleteQuiz(${quizID})">Delete Quiz</button>
                            </span>
                            <span class="moon-full">Quiz ID <strong>${quizID} [EDITING]</strong></span>
                        </summary>
                        <div class="content text-xl p-2 w-full">
                            <form id='quizInfo' class="w-full">
                                <input type='number' name='quizID' value='${quizID}' hidden readonly>
                                <div class="flex flex-row justify-center">
                                    <div class="flex-col w-1/3 px-2">
                                        <label for="quizTitle">Title: </label>
                                        <textarea class="m-1 p-1 w-full" name="quizTitle" id="quizTitle" placeholder="Quiz's Awesome Title" onchange="setQuizInfo(null, null)"></textarea>
                                    </div>
                                    <div class="flex-col w-2/3 px-2">
                                        <label for="quizr="Quiz's Descriptive Description" onchange="setQuizInfo(null, null)"></textarea>
                                    </div>
                                </div>
                                <div class="flex flex-row justify-start">
                                    <div class="flex flex-col px-2">
                                        <label for="quizDuration">Duration: </label>
                                        <input type='text' class="my-2 p-1 w-full html-duration-picker" name="quizDuration" id="quizDuration" onchange="setQuizInfo(null, null)">
                                    </div>
                                    <div class="flex flex-col px-2">
                                        <label for="quizMaxPoint">Max Point: </label>
                                        <input class="m-1 p-1 w-full" type="number" name="quizMaxPoint" id="quizMaxPoint" value="10" min="1" oninput="validity.valid||(value='');" onchange="setQuizInfo(null, null)">
                                    </div>
                                    <div class="flex flex-col bottom-0 px-2 items-center">
                                        <button type="button" class='flex-auto m-3 bg-green-500 hover:bg-green-800 text-white font-bold py-2 px-4 rounded' onclick='updateInfo()'>Save Quiz Info</button>
                                    </div>
                                    <div class="flex flex-col bottom-0 px-2 items-center">
                                        <button type="button" class='flex-auto m-3 bg-red-500 hover:bg-red-800 text-white font-bold py-2 px-4 rounded' onclick="askDeleteQuiz(${quizID})">Delete Quiz</button>
                                    </div>
                                    <div id="infoSaveStatus" class="block my-auto text-left font-bold">
                                        Save status
                                    </div>
                                </div>
                            </form>
                        </div>
                    </details>
                </div>
                <div class="w-full flex bg-grey-lighter editorPanel">
                    <div class="flex flex-col w-1/4 p-2 overflow-y-auto border border-1 border-green-dark">
                        <button class="w-full bg-green-500 hover:bg-green-800 text-white font-bold py-2 px-4 rounded" onclick="addNewQ(${quizID})">
                            &#65291; New Question
                        </button>
                        <div id="questionListPanel">
                            
                        </div>
                    </div>
                    <div class="flex flex-row w-4/5 h-full" id="questionEditorPanel">
                        <div class="flex flex-col w-3/5 m-2">
                            Question's Content:
                            <textarea class="rounded h-full p-2 my-2" id="qContent" onchange="editInternally(this)">QUESTION CONTENT</textarea>
                            <div class='w-full'>
                                <details class='explanation'>
                                    <summary class='my-2'>
                                        <span class='moon-new'>Question's explanation: <b>[OFF]</b><i class='m-1 text-gray-500'>Click to turn on</i></span>
                                        <span class='moon-full'>Question's explanation: <b>[ON]</b><i class='m-1 text-gray-500'>Click to turn off</i></span>
                                    </summary>
                                    <div class='w-full explainInput'>
                                        <textarea class='w-full' id='explanationText' onchange="editInternally(this)"></textarea>
                                    </div>
                                </details>
                            </div>
                        </div>
                        <div class="flex flex-col w-2/5 m-2">
                            <div class="flex flex-col w-full" style='height:85%'>
                                <div clas='flex flex-row'>
                                    <label for='answerPanel'>Question's Answers:</label>
                                    <button class='bg-green-500 hover:bg-green-800 text-white font-bold py-1 px-2 border border-green-800 rounded' onclick='addAnswer()'>New</button>
                                </div>
                                <div id='answerPanel' class='w-full selectAnswerPanel bg-white p-2 my-2 overflow-y-auto' style='min-height: 50%; max-height: 100%;'>

                                </div>
                            </div>
                            <div class='flex flex-row fixed-bottom'>
                                <button class='w-1/2 border border-1 border-green-dark bg-transparent hover:bg-green-500 text-green-dark font-semibold hover:text-white py-2 px-4 border border-green rounded m-2' onclick="saveQuestion(${quizID})">Save this question</button>
                                <button class='w-1/2 bg-transparent hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 rounded m-2' onclick="askDeleteQuestion()">Delete this question</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="loadingScreen" class="w-screen h-screen bg-transparent absolute z-50 top-0 left-0">
            <div class="w-full h-full bg-grey-dark opacity-75 absolute top-0 left-0"></div>
            <div id="preloader">
                <div id="loader"></div>
                <div class="text-3xl text-white font-bold block relative" style="top: 30%; left: 50%;margin: -75px 0 0 -75px;">
                    Loading...
                </div>
            </div>
        </div>
        <div id="reconfirm" class="w-screen h-screen bg-transparent absolute z-40 top-0 left-0">
            <div class="w-full h-full bg-red-dark opacity-50 absolute top-0 left-0"></div>
            <div class="text-3xl text-white font-semibold block relative text-center" style="top: 30%; left: 0%;margin: auto;">
                <div class="p-10 m-5 w-1/3" style="margin: 0 auto;" id="reconfirm-msg">Are you sure you wawnt to</div>
                <button class="z-50 bg-red-500 hover:bg-red-200 text-white font-bold py-2 px-4 border border-red-800 rounded hover:text-black" id="reconfirm-yes">Yes</button>
                <button class="z-50 bg-gray-200 hover:bg-gray-500 text-gray-800 font-bold py-2 px-4 rounded" id="reconfirm-no" onclick="showReconfirm(false)">Cancel</button>
            </div>
        </div>
        <script type='text/javascript'>
            showReconfirm(false);
            setLoading(true);
            loadQuizFromJson(${quizInfo});
            loadQuestionListFromJson(${quizQuestion});
            setLoading(false);
            setQuizInfo(null, null);
            updateEditor(-1);
        </script>
    </body>
</html>
