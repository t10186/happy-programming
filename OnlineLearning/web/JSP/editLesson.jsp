<%-- 
    Document   : addNewCourse
    Created on : Jun 5, 2022, 4:42:52 AM
    Author     : baobao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
              crossorigin="anonymous">
        <base href="${path}">
        <style>
            .login{
                background: url('Image/login-new.jpeg')
            }
            .new{
                margin-top: 10px;
                margin-left: 23px;
                padding: 7px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
            .save{
                margin-top: 10px;
                padding: 5px 30px;
                background-color: #86aed6;
                color: white;
                border-radius: 5px;
            }
        </style>
        <link rel="stylesheet" href="CSS/styles.css">
    </head>
    <body class="h-screen font-sans login bg-cover">
        <div id="toast"></div>
        <div class="container mx-auto h-full flex flex-1 justify-center items-center">
            <div class="w-full max-w-lg">
                <div class="leading-loose">
                    <form action="editlesson" method="post" class="max-w-xl m-4 p-10 bg-white rounded shadow-xl"
                          style="width: 700px"
                          >
                        <p class="text-gray-800 font-medium">chỉnh sửa bài học</p>    
                        <input style="display: none" name="id" value="${lesson.id}">
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">bài</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="unit" type="number" required="" value="${lesson.unit}">
                        </div>

                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">tiêu đề</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="title" type="text" required="" value="${lesson.title}">
                        </div>
                        <div class="mt-2">
                            <label class="block text-sm text-gray-00" for="cus_name">chi tiết</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="detail" type="text" required="" value="${lesson.detail}">
                        </div>            
                        <div class="mt-2">
                            <label class="block text-sm text-gray-600" for="cus_email">nội dung</label>
                            <textarea  style="height: 200px;"class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded"  id="cus_name" name="content" type="text" >
                                ${lesson.content} 
                            </textarea> 
                        </div>  
                        <div class="mt-2">
                            <label class="block text-sm text-gray-600" for="cus_email">link Video</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded" id="cus_name" name="linkvideo" type="file"  value="${lesson.linkVideo}">
                        </div>
                        <button class="save">lưu</button>
                        <a class="inline-block right-0 align-baseline font-bold text-sm text-500 hover:text-blue-800 new" href="list-lesson?courseID=${courseID}">
                            hủy bỏ 
                        </a>
                    </form>
                </div>
            </div>
        </div>     
    </body> 
</html>
