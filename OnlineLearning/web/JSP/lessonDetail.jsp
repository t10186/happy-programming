<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Luu Duc Thang
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="java.io.*,java.util.*,java.sql.*"%>  
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/lessonDetail.css">
        <link rel="stylesheet" href="CSS/text-back-color.css">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>Forms | Tailwind Admin</title> 
        <style>
            .box ul {
                width: 200px;
                max-height: 500px;
                overflow: auto;
                scrollbar-width: none;
                -ms-overflow-style: none;
            }

            .box ul::-webkit-scrollbar {
                width: 0 !important;
                display: none;
            }
            .box1 ul {
                width: 200px;
                max-height: 100px;
                overflow: auto;
                scrollbar-width: none;
                -ms-overflow-style: none;
            }

            .box1 ul::-webkit-scrollbar {
                width: 0 !important;
                display: none;
            }
        </style>
    </head>
    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div style="background-color: #333333 !important;" class="flex flex-col">
                <!--Header Section Starts Here-->
                <!--/Header-->
                <div class="flex flex-1" style="margin-top: 62px;flex-direction: column;">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->

                    <main class="container">
                        <div class="row g-5">
                            <div class="col-md-8">
                                <div id="ytplayer"></div>
                                <h3 style="margin-top: 20px;" class="pb-4 mb-4 fst-italic border-bottom">
                                    Bài học: ${lessonShow.title}
                                    <button onclick="myFunction()" style="float:right;" class="btn btn-outline-primary" href="">Tạo ghi chú</button>
                                    <button onclick="removeFunction()" style="float:right;" class="btn btn-outline-primary" href="">Xoá ghi chú</button>
                                    <p>Nội dung ghi chú:</p>
                                    <p style="display:none;" id="time">

                                    </p>
                                    <div class="box1">
                                        <ul style="list-style-type: none ;" id="list_note">
                                            <li>

                                            </li>
                                        </ul>
                                    </div>

                                    <textarea id="note" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>


                                </h3>
                                <article class="blog-post">
                                    <h4>Mô tả:</h4> 
                                    <h3 class="blog-post-title">${lessonShow.detail}</h3> 
                                    <h4>Nội dung bài học:</h4>
                                    <p>${lessonShow.content}</p>
                                </article>
                            </div>
                            <div class="col-md-4">
                                <div class="position-sticky box" style="top: 5rem;">
                                    <ul class="list-group mt-5 text-white force-overflow">
                                        <h3 class="mb-0">Lấy tên khoá học</h3>

                                        <li style="border-color: #0d6efd !important;background-color: black;color: white;" class="list-group-item d-flex justify-content-between align-content-center">
                                            Nội dung khoá học
                                        </li>

                                        <c:forEach  items="${listLessonUnlock}" var="items">

                                            <c:choose>
                                                <c:when test = "${items.lesson_unlock}">
                                                    <a href="show-lesson-detail?idShow=${items.getLesson().id}">
                                                        <li style="border-color: #0d6efd !important;  background-color: pink;color: white;" class="list-group-item d-flex justify-content-between align-content-center">
                                                            <div class="d-flex flex-row">
                                                                <div class="ml-2">
                                                                    <h6 class="mb-0">${items.getLesson().title}</h6>
                                                                    <div class="about">

                                                                        <span>Ngày đăng: ${items.getLesson().dateCreate}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="check">
                                                                <input type="checkbox" name="a" value="">
                                                            </div>
                                                        </li>
                                                    </a>   
                                                </c:when>
                                                <c:otherwise>
                                                    <span>
                                                        <li style="border-color: #0d6efd !important;  background-color: black;color: white;" class="list-group-item d-flex justify-content-between align-content-center">
                                                            <div class="d-flex flex-row">
                                                                <div class="ml-2">
                                                                    <h6 class="mb-0">${items.getLesson().title}</h6>
                                                                    <div class="about">
                                                                        <span>Ngày đăng: ${items.getLesson().dateCreate}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="check">
                                                                <!--                                                                <input type="checkbox" name="a" value="">-->
                                                                <i class="bi bi-lock"></i>

                                                            </div>
                                                        </li>
                                                    </span> 
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach> 

                                    </ul>
                                    <h3 style="margin-top: 10px;">Tiến độ khoá học:</h3>
                                    <div style="margin-top: 10px;" class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="${process_persent}" aria-valuemin="0" aria-valuemax="100" style="width:${process_persent}%">
                                            <span class="sr-only">${process_persent} complete</span>
                                        </div>
                                    </div>
                                    <nav style="margin-top: 20px;" class="blog-pagination" aria-label="Pagination">
                                        <c:if test = "${lesson.unit>1}">
                                            <a class="btn btn-outline-primary" href="show-lesson-detail?idShow=${lesson.id-1}">Xem bài trước</a>
                                        </c:if>
                                        <c:if test = "${lesson.unit<lesson_unlock}">
                                            <a onclick="nextlesson()" class="btn btn-outline-primary" href="show-lesson-detail?idShow=${lesson.id+1}">Bài kế tiếp</a>
                                        </c:if>
                                        <a class="btn btn-outline-primary" href="quiz-lesson">Làm bài kiểm tra</a>

                                    </nav>
                                </div>
                            </div>
                        </div>


                    </main>
                    <!--/Main-->
                </div>
            </div>
        </div>
        <script src="JS/main.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script>

                                                // 1. ytplayer code: https://developers.google.com/youtube/player_parameters#IFrame_Player_API
                                                var tag = document.createElement('script');
                                                tag.src = "https://www.youtube.com/player_api";
                                                var firstScriptTag = document.getElementsByTagName('script')[0];
                                                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                                                var player;
                                                function onYouTubePlayerAPIReady() {
                                                    player = new YT.Player('ytplayer', {
                                                        height: '450',
                                                        width: '100%',
                                                        videoId: '${key_video_id}'
                                                    });
                                                }
                                                // 2. get player.playerInfo.currentTime
                                                var list_time = new Array();
                                                let list_note = document.getElementById("list_note");
                                                let listItemsJSON = localStorage.getItem('${lesson.id}');
                                                let list_note_JSON = listItemsJSON ? JSON.parse(listItemsJSON) : [];
                                                for (let i = 0; i < list_note_JSON.length; i++) {
                                                    let li = document.createElement('li');
                                                    li.innerHTML = list_note_JSON[i];
                                                    list_note.appendChild(li);
                                                }

                                                function secondsToHms1(tmm) {
                                                    tmm = Number(tmm);
                                                    var H = Math.floor(tmm / 3600);
                                                    var M = Math.floor(tmm % 3600 / 60);
                                                    var S = Math.floor(tmm % 3600 % 60);
                                                    var sum = H * 3600 + M * 60 + S;

                                                    return sum;
                                                }
                                                var unlocked = false;
                                                function checkTime() {
                                                    if (unlocked)
                                                        return; //unlock r thi khong chay lai
                                                    var lesson_time = secondsToHms1(player.playerInfo.currentTime);
                                                    if (lesson_time >= ${sumSecond})
                                                    {
//                                                        alert('Xin chao');
                                                        $.ajax({
                                                            url: 'update-unlock',
                                                            type: 'GET',
                                                            data: 'courseID=${course_id}',
                                                            success: function (response) {
//                                                                if (response.success) {
//                                                                    unlocked = true;
//                                                                    alert('thanh cong');
//                                                                } else {
//                                                                    alert('that bai');
//                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                                setInterval(checkTime, 1000);




                                                function myFunction() {
                                                    var result = confirm("Bạn có muốn lưu ghi chú?");
                                                    if (result) {
                                                        var tm = document.getElementById("time").innerHTML = player.playerInfo.currentTime;
                                                        var tm1 = tm.toFixed(0);
                                                        function secondsToHms(tmm) {
                                                            tmm = Number(tmm);
                                                            var h = Math.floor(tmm / 3600);
                                                            var m = Math.floor(tmm % 3600 / 60);
                                                            var s = Math.floor(tmm % 3600 % 60);
                                                            var hDisplay = h > 0 ? h + (h == 1 ? " h " : " h ") : "";
                                                            var mDisplay = m > 0 ? m + (m == 1 ? " m " : " m ") : "";
                                                            var sDisplay = s > 0 ? s + (s == 1 ? " s" : " s") : "";
                                                            return hDisplay + mDisplay + sDisplay;
                                                        }

                                                        var tm2 = secondsToHms(tm1);
                                                        var note = document.getElementById("note");
                                                        var text_note = note.value;
                                                        list_time.push(tm2);
                                                        let li = document.createElement('li');
                                                        li.innerHTML = "Thời Gian: " + tm2 + " : " + text_note;
                                                        list_note.appendChild(li);
                                                        let text_JSON = "Thời Gian: " + tm2 + " : " + text_note;
                                                        list_note_JSON.push(text_JSON);
                                                        localStorage.setItem('${lesson.id}', JSON.stringify(list_note_JSON));
                                                        text_JSON = "";
                                                    }


                                                }
                                                function myFunction1() {
                                                    document.getElementById("test").innerHTML = list_time;
                                                }
                                                function removeFunction() {
                                                    var result = confirm("Bạn có muốn lưu ghi chú?");
                                                    if (result) {
                                                        localStorage.removeItem("${lesson.id}");
                                                    } else {
                                                        alert("abc");
                                                    }
                                                }


        </script>
    </body> 
</html> 


