<%-- 
    Document   : sidebar
    Created on : May 20, 2022, 11:46:06 PM
    Author     : Luu Duc Thang
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <base href="${path}">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <link rel="stylesheet" href="CSS/sidebar.css">
    </head>
    <body>
    <body style="background-color: white;" id="body-pd">
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <div class="display flex">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Notifications <span class="badge bg-secondary">${listPost.size()}</span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">

                        <c:forEach items="${listPost}" var="post">
                            <li class="text-truncate">
                                <a class="dropdown-item text-truncate" href="#"> 
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="${post.detail}">Post #${post.post_Id}</button>
                                </a>
                            </li>
                        </c:forEach> 
                    </ul>
                </div>
                <c:if test="${infor != null}" >
                    <div class="header_img"> <img src="Image/${infor.avatar == null?'null-image.jpg':infor.avatar}" alt="">
                    </div>
                </c:if> 
            </div>
        </header>
        <div class="l-navbar" id="nav-bar" >
            <nav class="nav">
                <div id="sidebar-scroll"> <a href="#" class="nav_logo"> <i class='bx bx-layer nav_logo-icon'></i> <span class="nav_logo-name">OnlineLearning</span> </a>
                    <div class="nav_list ">
                        <a href="home"  id="home" class="nav_link"> <i class='bx bx-home-circle nav_icon'></i> <span class="nav_name">Trang chủ</span> </a>
                        <a href="ShowListCourseController" id="course" class="nav_link"> <i class='bx bx-book nav_icon'></i> <span class="nav_name">Khoá học</span> </a> 
                        <c:if test="${roleID!= null}">
                            <a href="forms" id="forms" class="nav_link"> <i class='bx bx-user nav_icon'></i> <span class="nav_name">Tài khoản của bạn</span> </a> 
                            <a href="ChangePasswordAccountController" id="ChangePassword" class="nav_link"> <i class='bx bx-bookmark nav_icon'></i> <span class="nav_name">Thay đổi mật khẩu</span> </a>
                            <c:if test="${roleID==2}">
                                <a href="dash-board"  id="dashBoard" class="nav_link"> <i class='bx bx-home-circle nav_icon'></i> <span class="nav_name">bảng điều khiển </span> </a>
                                <a href="show-list-user" id="listUser" class="nav_link"> <i class='bx bxs-user-detail nav_icon'></i> <span class="nav_name">Danh sách tài khoản</span> </a> 
                                <a href="slideshow" id="listSlide"class="nav_link"> <i class='bx bx-list-check nav_icon'></i> <span class="nav_name">Danh sách slide</span> </a>
                                <a href="add-new-slide" id="listSlide"class="nav_link"> <i class='bx bx-book-add nav_icon'></i> <span class="nav_name">Thêm slide mới</span> </a>
                                <a href="list-post" id="listRequest"class="nav_link"> <i class='bx-message-square-detail nav_icon'></i> <span class="nav_name">Danh sách Post</span> </a> 
                                <a href="add-new-post" id="listRequest"class="nav_link"> <i class='bx-message-square-detail nav_icon'></i> <span class="nav_name">Thêm post</span> </a>  
                            </c:if>
                            <c:if test="${roleID==1}">   
                                <a href="showList-course-user" id="listMyCourse" class="nav_link"> <i class='bx bx-book-open nav_icon'></i> <span class="nav_name">Khoá học của tôi</span> </a>
                            </c:if>
                            <c:if test="${roleID==2 || roleID==3}">
                                <a href="show-list-course-devoloper" class="nav_link"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">Danh sách khoá học </span> </a> 
                                <a href="show-manage-lesson" class="nav_link"> <i class='bx bx-folder nav_icon'></i> <span class="nav_name">Danh sách bài học </span> </a> 
                            </c:if>
                            <c:if test="${roleID==2}">
                                <a href="addcourse" class="nav_link"> <i class='bx bx-book-add nav_icon'></i> <span class="nav_name">Thêm khoá học</span> </a> 
                            </c:if>
                            <a href="show-list-blogs" class="nav_link"> <i class='bx bx-message-square-detail nav_icon'></i> <span class="nav_name">Blogs</span> </a> 
                        </c:if>
                        <!--                        <a href="#" class="nav_link"> <i class='bx bx-message-square-detail nav_icon'></i> <span class="nav_name">Messages</span> </a> -->
                        <!--                        <a href="#" class="nav_link"> <i class='bx bx-bar-chart-alt-2 nav_icon'></i> <span class="nav_name">Stats</span> </a> </div>-->
                    </div>
                    <c:choose>
                        <c:when test="${roleID== null}">
                            <a href="login" class="nav_link"> <i class='bx bx-log-in nav_icon'></i> <span class="nav_name">Đăng nhập</span> </a>
                        </c:when>
                        <c:otherwise>
                            <a href="JSP/login.jsp" class="nav_link"> <i class='bx bx-log-out nav_icon'></i> <span class="nav_name">Đăng xuất</span> </a> 
                        </c:otherwise>
                    </c:choose> 
                </div>
            </nav>
        </div>
        <!--Container Main start-->
        <!--        <div class="height-100">
                </div>-->
        <!--Container Main end-->
        <script>
            var scroll = document.getElementById("sidebar-scroll");
            if (scroll.offsetHeight > window.innerHeight) {
                scroll.classList.add("scroll-sidebar");
            }
            document.addEventListener("DOMContentLoaded", function (event) {

                const showNavbar = (toggleId, navId, bodyId, headerId) => {
                    const toggle = document.getElementById(toggleId),
                            nav = document.getElementById(navId),
                            bodypd = document.getElementById(bodyId),
                            headerpd = document.getElementById(headerId)

            // Validate that all variables exist
                    if (toggle && nav && bodypd && headerpd) {
                        toggle.addEventListener('click', () => {
            // show navbar
                            nav.classList.toggle('show')
            // change icon
                            toggle.classList.toggle('bx-x')
            // add padding to body
                            bodypd.classList.toggle('body-pd')
            // add padding to header
                            headerpd.classList.toggle('body-pd')
                        })
                    }
                }
                showNavbar('header-toggle', 'nav-bar', 'body-pd', 'header')

                /*===== LINK ACTIVE =====*/
                const linkColor = document.getElementById(${linkColor});
                linkColor.classList.add('active');
                // Your code to run since DOM is loaded and ready
            }); 
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </body>
</html>