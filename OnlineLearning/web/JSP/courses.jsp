<%-- 
    Document   : courses
    Created on : Jun 2, 2022, 6:06:22 AM
    Author     : Quynh_Nhu
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/search.css">
        <title>Courses</title>
    </head>

    <style>
        .row {
            margin-left: 5%;
            margin-bottom: 2%;
            height: 150px;
            border-radius: 10px;
            background-size: 100%;
            width: 99%;
        }

        .row:hover {
            color: #bebebc;
            opacity: 0.7;


        }

        .detail{
            font-weight:500 ;
            word-spacing: 2px;
            font-size: 18px;
        }
        .pagination{
            margin-top: 20px;
            display: flex;
            justify-content: center;    
        }
        .page-item.active .page-link{
            background-color: gray !important;
            border-color: gray !important;
        }
    </style>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="course"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3 overflow-hidden"
                          style="margin-top: 93px;"
                          >

                        <div class="flex flex-col">
                            <!-- Stats Row Starts Here -->

                            <!-- /Stats Row Ends Here -->

                            <!-- Card Sextion Starts Here -->
                            <div class="flex flex-1 flex-col md:flex-row lg:flex-row mx-2">

                                <!-- card -->


                                <div tabindex="-1" style="width: 100%; display: inline-block; box-sizing: border-box;">
                                    <!-- card -->
                                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">

                                        
                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                    <!-- /card -->

                                </div>
                            </div>
                            <div class="wrap">
                                <form name="search"class="search" action="ShowListCourseController">
                                    <input name="keySearch" type="text" class="searchTerm" placeholder="tìm kiếm khóa học ...">
                                    <button type="submit" class="searchButton">
                                         <i class="fa fa-search"></i>
                                    </button>
                                </form>
                             </div>
                            <div class="px-6 py-2 border-b border-light-grey">
                                <div class="font-bold text-xl" style="margin-top:2%;  font-size: 25px;">
                                    Danh Sách Khóa Học
                                </div>
                            </div>
                            <div style=" display: flex;flex-wrap: wrap;">
                                    <c:forEach items="${listCourses}" var="items">  
                                    <div class="col-3">
                                        <a href="detail-course?courseID=${items.id}" class="lg bg-vibrant  hover:bg-vibrant mb-2 p-2 md:w-1/4 mx-2 ">
                                            <div class="row"
                                                 style="background-image: url('${items.imageCourse}');">
                                            </div>
                                            <h2 class="detail" >${items.title}</h2> 
                                        </a>
                                    </div>
                                </c:forEach>
                                
                            </div>
                            <c:if test="${endList>1}">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <c:if test="${index >1}">
                                            <li class="page-item">
                                                <a class="page-link" href="ShowListCourseController?index=${index-1}" aria-label="Previous" style="color: black">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        </c:if>
                                        <c:forEach begin="1" end="${endList}" var="i">
                                            <li class="page-item ${index == i?"active":""}"><a class="page-link " href="ShowListCourseController?index=${i}" style="color: black">${i}</a></li>
                                        </c:forEach>
                                        <c:if test="${index<endList}">
                                            <li class="page-item">
                                                <a class="page-link" href="ShowListCourseController?index=${index +1}" aria-label="Next" style="color: black">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </c:if>
                            <!-- /Cards Section Ends Here -->

                        </div>
                    </main>
                    <!--/Main-->
                </div>
                <!--Footer-->
                <footer class="bg-grey-darkest text-white p-2">
                    <div class="flex flex-1 mx-auto"></div>
                    <div class="flex flex-1 mx-auto"></div>
                </footer>
                <!--/footer-->
                <%@include file="footer.jsp" %>

            </div>
        </div>
        <script src="JS/main.js"></script>
    </body>

</html>
