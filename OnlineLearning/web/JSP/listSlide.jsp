<%-- 
    Document   : listSlide.jsp
    Created on : Jun 18, 2022, 5:11:22 PM
    Author     : baobao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>HOME</title> 
    </head>

    <body>
        <!-- card -->

        <div class="flex flex-1">
            <!--Sidebar-->
            <c:set var="linkColor" value="listRequest"/>
            <%@include file="sidebar.jsp" %>
            <!--/Sidebar-->
            <main class="bg-white-300 flex-1 p-3 overflow-hidden">
                <div class="rounded overflow-hidden shadow bg-white mx-2 w-full">
                    <div class="px-6 py-2 border-b border-light-grey">
                        <div class="font-bold text-xl">List SlideShow</div>
                    </div>
                    <div class="table-responsive">
                        <table class="table text-grey-darkest">
                            <thead class="bg-grey-dark text-white text-normal">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col"></th>
                                    <th scope="col">Name</th>
                                    <th scope="col"></th>
                                    <th scope="col">Time Create</th>
                                    <th scope="col"></th>
                                    <th scope="col">Description</th><!-- comment -->
                                    <th scope="col"></th><!-- comment -->

                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                    <th scope="col">Detail</th>

                                </tr>
                            </thead>
                            <tbody>


                                <c:forEach items="${listSlide}" var="p">

                                    <tr>
                                        <th scope="row">${p.getId()}</th>
                                        <td>
                                        </td>
                                        <td>${p.getName()}</td>
                                        <td></td>
                                        <td>${p.getTimeCreate()}</td>
                                        <td></td>
                                        <td>${p.getDescription()}</td>
                                        <td></td>
                                        <td>${p.getStatus() == 1?"Active":"Disable"}</td>
                                        <td>
                                            <form action="update-status-slide?id=${p.getId()}" method="post">
                                                <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                                    <button type="submit" class="btn btn-outline-secondary" name="status" value="${p.getStatus()}">${p.getStatus() == 0?"Active":"Disable"}</button>
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <a class="btn btn-outline-secondary" href="detail-slide?id=${p.getId()}">More Detail</a>
                                        </td>
                                    </tr>                                   
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /card -->
            </main>
        </div>

        <!--Footer-->


    </body>
</html>

