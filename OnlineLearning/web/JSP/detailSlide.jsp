<%-- 
    Document   : courses
    Created on : July 2, 2022, 6:06:22 AM
    Author     : admin
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Courses</title>
    </head>

    <style>
        .row {
            margin-left: 5%;
            margin-bottom: 2%;
            height: 150px;
            border-radius: 10px;
            background-size: 100%;
            width: 99%;
        }

        .row:hover {
            color: #bebebc;
            opacity: 0.7;


        }

        .detail{
            font-weight:500 ;
            word-spacing: 2px;
            font-size: 18px;
        }
    </style>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <div class="flex flex-1">
                    <!--Sidebar-->
                    <c:set var="linkColor" value="course"/>
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-300 flex-1 p-3 overflow-hidden"
                          style="margin-top: 93px;"
                          >

                        <div class="flex flex-col">
                            <!-- Stats Row Starts Here -->

                            <!-- /Stats Row Ends Here -->
                            <figure class="text-center">
                                <blockquote class="blockquote">
                                    <p>DETAIL OF SLIDE</p>
                                </blockquote>
                                <figcaption class="blockquote-footer">

                                </figcaption>
                            </figure>
                            <!-- Card Sextion Starts Here -->
                            <div class="flex flex-1 flex-col md:flex-row lg:flex-row mx-2">
                                <!-- card -->

                                <div tabindex="-1" style="width: 100%; display: inline-block; box-sizing: border-box;">
                                    <!-- card -->
                                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">

                                        <div class="carousel-inner">
                                            <c:set var="tmp" value="0"/> 
                                            <c:forEach items="${listImg}" var="p">

                                                <div class="carousel-item
                                                     ${(tmp == 0)?'active':''}
                                                     ">

                                                    <img src=" ${p.getImg()}" class="d-block w-100" alt="...">
                                                </div>
                                                <c:set var="tmp" value="${tmp+1}"/> 
                                            </c:forEach>

                                        </div>
                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                    <!-- /card -->

                                </div>
                            </div>

                        </div>
                        <!-- /Cards Section Ends Here -->

                </div>
                </main>
                <!--/Main-->
                
            </div>
             
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                         <th scope="col">Number of img</th>
                        <th scope="col">Time Create</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">${slide.id}</th>
                        <td>${slide.name}</td>
                        <td>${slide.description}</td>
                        <td>${listImg.size()}</td>
                        <td>${slide.timeCreate}</td>
                        <td>${slide.status==1?"Active":"Disable"}</td>
                        <td><a href="update-slide?slideid=${slide.id}" class="btn btn-secondary">Update</a></td>
                    </tr>
                   
                </tbody>
            </table>
            <!--Footer-->
            <footer class="bg-grey-darkest text-white p-2">
                <div class="flex flex-1 mx-auto"></div>
                <div class="flex flex-1 mx-auto"></div>
            </footer>
            <!--/footer-->
            <%@include file="footer.jsp" %>

        </div>
    </div>
    <script src="JS/main.js"></script>
</body>

</html>
