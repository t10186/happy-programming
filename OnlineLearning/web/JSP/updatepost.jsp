<%-- 
    Document   : updatepost
    Created on : Jul 5, 2022, 1:11:55 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        /* @import url(https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900); */
        body {
            background-color: #333333;

            margin:0;
            padding:0;
            overflow-x:hidden;
            height:100%;
            font-family: 'Lato', Helvetica, arial, sans-serif;
            font-weight: 300;
            font-size: 20px;
            line-height: 1.45;
            color: #eee;
            color: rgba(255,255,255,.85);
        }
        #container {
            padding-top: 50px;
        }
        #content {
            max-width: 43em;
            padding:10px;
            margin: 0 auto;
        }
        h1 {
            font-size: 4.8em;
            font-weight: 100;
            text-transform: uppercase;
            margin: 0;
        }
        h3 {
            font-size: 2.4em;
            font-weight: 300;
            line-height: 1.5;
        }
        p, li {
            font-size: 1.7em;
        }
        a {
            font-weight: 700;
            text-decoration: none;
            color: #fff;
        }
        a:hover {
            text-decoration: underline;
        }
        p#pleft {
            max-width:20em;
            float:left;
        }
        p#pright {
            max-width:20em;
            float:left;
        }
        ul {
            clear:both;
        }


        html {
            font-size: 62.5%;
        }
        body {
            font-size: 1em;
        }
        @media (max-width: 300px) {
            html {
                font-size: 70%;
            }
            .stage {
                -webkit-transform:scale(0.05);
                transform:scale(0.05);
            }
        }
        @media (max-width: 440px) {
            h1 {
                line-height:55px;
            }
        }
        @media (max-width: 460px) {
            .stage {
                position:absolute;
                top:25px;
                left:50%;
                margin-left:-45px;
            }
            h1 {
                margin-top:50px;
                text-align:center;
            }
        }
        @media (max-width:600px) {
            .stage {
                -webkit-transform:scale(0.55);
                transform:scale(0.55);
                margin-right:60px;
            }
        }
        @media (min-width: 600px) {
            html {
                font-size: 80%;
            }
            .stage {
                -webkit-transform:scale(0.68);
                transform:scale(0.68);
                margin-right:80px;
            }
        }
        @media (min-width: 880px) {
            html {
                font-size: 100%;
            }
            p, li {
                font-size: 1em;
            }
            p#pright {
                margin-left:3em;
            }
            .stage {
                -webkit-transform:scale(0.85);
                transform:scale(0.85);
                margin-right:120px;
            }
        }


    </style>
    <body>
        <c:set var="linkColor" value="home"/>
        <%@include file="sidebar.jsp" %>
        <div id="container col-md-6">
            <div id="content">
                <div id="about">
                    
                    <h1>
                        <div style='float:left; margin-bottom:20px;'name="id" value="${post.post_Id}">
                            #${post.post_Id} ${post.title}
                        </div>
                        <br>
                    </h1>
                    <br>

                    <h3 style='clear:both' class="subhead">
                        Detail: 
                    </h3>

                    <ul>
                        <li>
                            ${post.detail}
                        </li>
                    </ul>
                    <br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Category</th>
                                <th scope="col">Author</th>
                                <th scope="col">Date create</th>
                                <th scope="col">Img post</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th> ${post.category}</th>
                                <td> ${post.author_id}</td>
                                <td>${post.timeCreate}</td>
                                <td>
                                    <img style="height: 100px; width: 100px" src="${post.thumbnail}" alt="alt"/> 


                                </td>
                                <td>
                                    ${item.status == 0?"Active":"Disable"}
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    <form action="update-post" method="post">
        <div class="col-md-2">
            ID
            <input name="id" id="validationTextarea" value="${post.post_Id}" readonly> 
                        </div>
        <div class="was-validated col-md-6">
            <div class="mb-3">
                <label for="validationTextarea" class="form-label">Title</label>
                <textarea class="form-control is-invalid" name="title" id="validationTextarea" placeholder="Required title " required>   </textarea>

            </div>
            <div class="mb-3">
                <label for="validationTextarea" class="form-label">Detail</label>
                <textarea class="form-control is-invalid" name="detail" id="validationTextarea" placeholder="Required detail " required>  </textarea>

            </div>
            <label for="validationTextarea" class="form-label">Status of post</label>
            <div class="mb-3">
                <select class="form-select" name="status" required aria-label="select example">
                   
                    <option value="0">Disable</option>
                    <option value="1">Active</option>
                   
                </select>
                <label for="validationTextarea" class="form-label">Category</label>
            <div class="mb-3">
                <select class="form-select" name="Category" required aria-label="select example">
                   
                    <option value="Notify">Notify</option>
                    <option value="News">News</option>
                   
                </select>
                <div class="invalid-feedback">Example invalid select feedback</div>
            </div>

            <div class="col-md-4">
                <label for="validationDefault01" class="form-label" required>Post thumbnail</label>
                <input type="text" class="form-control" name="thumbnail" id="validationDefault01" value="${post.thumbnail}" required>
            </div>
            <br>
            
            <div class="mb-3">
            <button type="submit" class="btn btn-outline-secondary">Save</button>                  
            </div>
            </form>
        </div>


    </body>
</html>
