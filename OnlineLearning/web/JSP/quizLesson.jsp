<%-- 
    Document   : quizLesson
    Created on : Jun 2, 2022, 11:07:33 PM
    Author     : Phong Linh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.QuizHistory"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" type="text/css" href="CSS/styles.css">
        <link rel="stylesheet" type="text/css" href="CSS/all.css">
        <title>Quiz Lesson</title>
        <!--<fmt:setLocale value="en"/>-->
        <style>
            .quizDesc {
                min-height: 60vh;
            }
            .quizDescText {
                min-height: 40vh;
            }
            .reviewPanel {
                max-height: 80vh;
                min-height: 200px;
            }
            .reviewRow-1 {
                min-height: 40vh;
            }
            .reviewRow-child {
                min-height: 30%;
            }
            .reviewRow-2 {
                min-height: 25vh;
            }
            .reviewRow-3 {
                min-height: 10vh;
            }
            .alignVertically {
                top: 50%;
                transform: translateY(-50%);
            }
            .hover\:border-dotted:hover {
                border-style: dotted;
            }
        </style> 
    </head>
    <body>
        <script src="JS/quiz.js"></script>
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <!--Header Section Starts Here-->
                <header class="bg-nav">
                    <div class="flex justify-between">
                        <div class="p-1 mx-3 inline-flex items-center">
                            <button class="text-white p-1 font-bold text-xl" onclick="window.location.href='home'">Home</button>
                        </div>
                        <div class="p-1 flex flex-row items-center">
                            <!--<img onclick="profileToggle()" class="inline-block h-8 w-8 rounded-full" src="https://avatars0.githubusercontent.com/u/4323180?s=460&v=4" alt="">-->
                            <div href="" class="text-white p-2 no-underline hidden md:block lg:block">${info.displayName}</div>
                        </div>
                    </div>
                </header>
                <!--/Header-->
                <c:choose>
                    <c:when test="${quizHistory != null}">
                        <c:set var="correctCount" scope="page" value="${quizHistory.getCorrectCount()}"/>
                        <c:set var="wrongCount" scope="page" value="${quizHistory.getWrongCount()}"/>
                        <c:set var="unanswerCount" scope="page" value="${quizHistory.getUnansweredCount()}"/>
                        <c:set var="totalCount" scope="page" value="${quizHistory.questionList.size()}"/>
                        <div class="m-auto py-3 w-2/3 reviewPanel">
                            <div class="flex flex-1 flex-row pb-2 reviewRow-1">
                                <div class="px-3 border-dashed w-1/2 shadow-md hover:shadow-xl rounded border-4 hover:border-dotted">
                                    <div class="w-full underline text-center text-2xl" style="height: 10%">   Point   </div>
                                    <div class="text-center m-3"><b style="font-size: 12vh;" id="curPoint">${quizHistory.point}</b><text style="font-size: 6vh;"> / ${quiz.maxPoint}</text></div>
                                    <script>
                                        if (${quizHistory.point >= quiz.passRate}) {
                                            document.getElementById("curPoint").style.color = "#51d88a";
                                        } else {
                                            document.getElementById("curPoint").style.color = "#ef5753";
                                        }
                                    </script>
                                    <div class="text-center text-lg">
                                        Correct <b class="text-green-light">${correctCount}</b> out of <b>${totalCount}</b>
                                    </div>
                                    <div class="text-center text-xl p-4 font-semibold">
                                        <c:choose>
                                            <c:when test="${quizHistory.point >= quiz.passRate}">
                                                PASSED THE QUIZ!
                                            </c:when>
                                            <c:otherwise>
                                                FAILED THE QUIZ!
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="pl-3 w-1/2 flex-col reviewRow-1 flex justify-between">
                                    <div class="flex-row bg-grey shadow-md hover:shadow-xl reviewRow-child relative rounded-lg">
                                        <div class="bg-green-400 h-full rounded-l-lg absolute" style="width: ${correctCount*100/totalCount}%"></div>
                                        <div class="px-3 text-4xl alignVertically text-white absolute">
                                            <fmt:formatNumber value="${correctCount*100/totalCount}"
                                            maxFractionDigits="2" />% Correct
                                        </div>
                                    </div>
                                    <div class="flex-row bg-grey shadow-md hover:shadow-xl reviewRow-child relative rounded-lg">
                                        <div class="bg-red-400 h-full rounded-l-lg absolute" style="width: ${wrongCount*100/totalCount}%"></div>
                                        <div class="px-3 text-4xl alignVertically text-white absolute">
                                            <fmt:formatNumber value="${wrongCount*100/totalCount}"
                                            maxFractionDigits="2" />% Incorrect
                                        </div>
                                    </div>
                                    <div class="flex-row bg-grey shadow-md hover:shadow-xl reviewRow-child relative rounded-lg">
                                        <div class="bg-grey-darker h-full rounded-l-lg absolute" style="width: ${unanswerCount*100/totalCount}%"></div>
                                        <div class="px-3 text-4xl alignVertically text-white absolute">
                                            <fmt:formatNumber value="${unanswerCount*100/totalCount}"
                                            maxFractionDigits="2" />% Unanswered
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex flex-1 py-3 flex-row flex-row reviewRow-2">
                                <c:set var="quizHistoryDuration" scope="page" value="${quizHistory.finishTime.getTime()/1000 - quizHistory.takeTime.getTime()/1000}"/>
                                <div class="flex-col border-l-8 border-orange-dark p-3 w-1/3 shadow-md hover:shadow-xl">
                                    <div class="flex-row text-3xl text-left font-bold underline" >Last take:</div>
                                    <div class="flex-row text-2xl text-left font-semibold"><fmt:formatDate type="both" pattern="dd/MMM/yyyy - HH:mm" value="${quizHistory.takeTime}"/></div>
                                </div>
                                <div class="flex-col border-l-8 border-orange-dark p-3 w-1/3 shadow-md hover:shadow-xl">
                                    <div class="flex-row text-3xl text-left font-bold underline">Take's total time:</div>
                                    <div class="flex-row text-2xl text-left font-semibold" id="quizHistoryDuration">#Duration#</div>
                                    <script type="text/javascript">
                                        document.getElementById("quizHistoryDuration").innerHTML = "${quizHistoryDuration}".toHHMMSS2();
                                    </script>
                                </div>
                                <div class="flex-col border-l-8 border-orange-dark p-3 w-1/3 shadow-md hover:shadow-xl">
                                    <div class="flex-row text-3xl text-left font-bold underline">Time per question:</div>
                                    <div class="flex-row text-2xl text-left font-semibold" id="quizHistoryTimerPerQ">#Very Fast#</div>
                                    <script type="text/javascript">
                                        document.getElementById("quizHistoryTimerPerQ").innerHTML = "${quizHistoryDuration/totalCount}".toHHMMSS2();
                                    </script>
                                </div>
                            </div>
                            <div class="flex flex-1 flex-row justify-between reviewRow-3">
                                <form action="quiz-handle" method="POST">
                                    <input type="number" name="quizId" value="${quiz.id}" hidden>
                                    <input type="text" name="username" value="${acc.username}" hidden>
                                    <input type="checkbox" name="reviewQuiz" value="true" checked hidden>
                                    <button type="submit" class="flex-col pr-3 bg-transparent hover:bg-green-500 text-green-dark font-semibold hover:text-white py-2 px-4 border border-4 border-green-dark hover:border-transparent rounded">
                                        Xem lại kết quả lần trước
                                    </button>
                                </form>
                                <form action="quiz-handle" method="POST">
                                    <input type="number" name="quizId" value="${quiz.id}" hidden>
                                    <input type="text" name="username" value="${acc.username}" hidden>
                                    <input type="checkbox" name="startQuiz" value="true" checked hidden>
                                    <button type="submit" class="flex-col pl-3 bg-transparent hover:bg-green-500 text-green-dark font-semibold hover:text-white py-2 px-4 border border-4 border-green-dark hover:border-transparent rounded">
                                        Làm lại Quiz
                                    </button>
                                </form>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="m-auto border-solid border-grey-light rounded border-info shadow-lg w-1/2">
                            <div class="font-semibold text-2xl border-b bg-blue-lighter hover:bg-blue-light border-blue-500 text-grey-darskest hover:text-blue-lightest px-4 py-3">
                                ${quiz.tittle}
                            </div>
                            <div class="p-3 quizDesc">
                                <div class="flex flex-1 justify-around py-3 mx-2">
                                    <div class="text-lg w-1/2 text-center" id="quizDuration">
                                        ${quiz.length} minutes
                                    </div> | 
                                    <div class="text-lg w-1/2 text-center">
                                        ${quiz.questionCount} questions
                                    </div>
                                    <script type="text/javascript">
                                        document.getElementById("quizDuration").innerHTML = "Duration: " + "${quiz.length}".toHHMMSS2();
                                    </script>
                                </div>
                                <div class="p-3 bg-grey-light hover:bg-grey quizDescText">
                                    ${quiz.description}
                                </div>
                                <form class="my-3 text-lg" action="quiz-handle" method="POST">
                                    <input type="number" name="quizId" value="${quiz.id}" hidden>
                                    <input type="text" name="username" value="${acc.username}" hidden> <!-- username -->
                                    <input type="checkbox" name="startQuiz" value="true" checked hidden>
                                    <input class="bg-blue-dark hover:bg-blue-darker text-white font-bold py-2 px-4 border-b-4 border-r-2 border-blue-darker hover:border-blue-darker rounded" type="submit" value="Start Test">
                                </form>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <!--<script src="../JS/main.js"></script>-->
    </body>
</html>