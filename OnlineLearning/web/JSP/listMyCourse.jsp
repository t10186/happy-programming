<%-- 
    Document   : listMyCourse
    Created on : Jun 2, 2022, 7:22:05 AM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}"> 
        <link rel="stylesheet" href="../CSS/styles.css">
        <link rel="stylesheet" href="../CSS/all.css">
        <link rel="stylesheet" href="CSS/search.css">
        <link rel="stylesheet" href="CSS/backend.css">
        <link rel="stylesheet" href="CSS/backend-plugin.min.css">
        <style>
            /* Apply css properties to h1 element */
            h1 {
                text-align: center;
            }

            /* Create a container using CSS properties */


            /* Apply CSS properties to ui-widgets class */
            .ui-widgets {
                position: relative;
                display: inline-block;
                width: 10rem;
                height: 10rem;
                border-radius: 9rem;
                margin: 1.5rem;
                border: 1.2rem solid palegreen;
                box-shadow: inset 0 0 7px grey;
                border-left-color: palegreen;
                border-top-color: chartreuse;
                border-right-color: darkgreen;
                border-bottom-color: white;
                text-align: center;
                box-sizing: border-box;
            }

            /*  Apply css properties to the second 
                child of ui-widgets class */
            .ui-widgets:nth-child(2) {
                border-top-color: chartreuse;
                border-right-color: white;
                border-left-color: palegreen;
                border-bottom-color: white;
            }

            /*  Apply css properties to ui-widgets class 
                and ui-values class*/
            .ui-widgets .ui-values {
                top: 40px;
                position: absolute;
                left: 10px;
                right: 0;
                font-weight: 700;
                font-size: 2.0rem;

            }

            /*  Apply css properties to ui-widgets 
                class and ui-labels class*/
            .ui-widgets .ui-labels {

                left: 0;
                bottom: -16px;
                text-shadow: 0 0 4px grey;
                color: black;
                position: absolute;
                width: 100%;
                font-size: 16px;
            }
            .row {
                margin-left: 5%;
                margin-bottom: 2%;
                border-radius: 10px;
                background-size: 100%;

            }

            .detail{
                font-weight:500 ;
                word-spacing: 2px;
                font-size: 18px;
            }
            .pagination{
                margin-top: 20px;
                display: flex;
                justify-content: center;
            }
            .page-item.active .page-link{
                background-color: gray !important;
                border-color: gray !important;
            }
        </style>
        <title>HOME</title> 
    </head>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-400">
            <!--Screen-->
            <div class="flex flex-col">
                <c:set var="linkColor" value="listMyCourse"/>
                <%@include file="sidebar.jsp"%>
                <!--Sidebar-->
                <div style="margin-top: 5%;" class="col-md-6 col-lg-3">
                    <div class="card card-block card-stretch card-height">
                        <div class="card-body">
                            <div class="top-block d-flex align-items-center justify-content-between">
                                <h5>${information.displayName}(${information.fullName})</h5>
                                <span class="badge badge-primary">Student</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <p class="mb-0">Tổng số khoá học của bạn ${total_myCourse_int}/${total_course} </p>
                                <span class="text-primary">${process_persent_course}%</span>
                            </div>
                            <div class="iq-progress-bar bg-primary-light mt-2">
                                <span class="bg-primary iq-progress progress-1" data-percent="65"></span>
                            </div>
                        </div>
                        <div style="width: 300%" class="wrap">
                            <form name="search"class="search" action="showList-course-user">
                                <input name="keySearch" type="text" class="searchTerm" placeholder="tìm kiếm khóa học ..." value="${keySearch}"> 
                                <button type="submit" class="searchButton">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">Tiến trình học</h4>
                    </div>
                </div>

                <div class="row">
                    <c:forEach items="${listCourse}" var="item">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div style="margin-bottom: 0;" class="row">
                                        <div class="col-sm-8">
                                            <div class="row align-items-center">
                                                <div class="col-md-3">
                                                    <div class="ui-widgets">
                                                        <div class="ui-values">${item.persent}%</div>
                                                        <div class="ui-labels"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <img src="${item.imageCourse}">

                                                </div>      
                                                <div class="col-md-6">
                                                    <div class="mt-3 mt-md-0">
                                                        <h5 class="mb-1">${item.title}</h5>
                                                        <p class="mb-0">${item.description}</p>
                                                    </div>                                                        
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 text-sm-right mt-3 mt-sm-0">
                                            <div class="iq-media-group">
                                                <a style="margin-top:100px;" href="lesson-detail?courseID=${item.id}" class="btn btn-outline-primary" href="quiz-lesson">Tiếp tục học</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <nav aria-label="Page navigation example" style="margin-bottom: 50px">
                    <ul class="pagination">
                        <c:if test="${index >1}">
                            <li class="page-item">
                                <a class="page-link" href="showList-course-user?index=${index-1}" aria-label="Previous" style="color: black">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${endList}" var="i">
                            <li class="page-item ${index == i?"active":""}"><a class="page-link " href="showList-course-user?index=${i}" style="color: black">${i}</a></li>
                            </c:forEach>
                            <c:if test="${index<endList}">
                            <li class="page-item">
                                <a class="page-link" href="showList-course-user?index=${index +1}" aria-label="Next" style="color: black">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>

            </div>

        </div>
        <%@include file="footer.jsp" %>
        <script src="JS/main.js"></script>
    </body>
</html>


