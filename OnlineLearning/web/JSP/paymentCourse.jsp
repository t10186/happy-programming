<%-- 
    Document   : paymentCourse.jsp
    Created on : Jun 4, 2022, 6:19:19 PM
    Author     : Luu Duc Thang
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>RegisterCourses</title>
        <!-- Bootstrap core CSS -->
        <link href="http://localhost:9999/OnlineLearning/assets/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="http://localhost:9999/OnlineLearning/assets/jumbotron-narrow.css" rel="stylesheet">      
        <script src="http://localhost:9999/OnlineLearning/assets/jquery-1.11.3.min.js"></script> 
    </head>
    <style>
        html {
            height: 100%;
        }
        body {
            margin:0;
            padding:0;
            font-family: sans-serif;
            background: linear-gradient(#141e30, #243b55);
        }
        a{
            text-decoration: none;
        }
        .login-box {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 400px;
            padding: 40px;
            transform: translate(-50%, -50%);
            background: rgba(0,0,0,.5);
            box-sizing: border-box;
            box-shadow: 0 15px 25px rgba(0,0,0,.6);
            border-radius: 10px;
        }

        .login-box h2 {
            margin: 0 0 30px;
            padding: 0;
            color: rgb(255, 201, 7);
            text-align: center;
        }

        .login-box .user-box {
            position: relative;
        }

        .login-box .user-box input {
            width: 100%;
            padding: 10px 0;
            font-size: 16px;
            color: #fff;
            margin-bottom: 30px;
            margin-top: 10px;
            /* border: none;
            border-bottom: 1px solid #fff; */
            outline: none;
            background: transparent;
        }
        label {
            font-size: 16px;
            color: #fff;
            pointer-events: none;
            transition: .5s;
        }
        .course-title{
            color: white;

        }

        .submit {
            padding: 10px 20px;
            color: rgb(255, 201, 7);
            font-size: 16px;
            text-decoration: none;
            text-transform: uppercase;
            overflow: hidden;
            transition: .5s;
            margin-top: 40px;
            letter-spacing: 4px;
            font-size: 12px;
        }

        .submit:hover {
            background: rgb(192, 151, 3);
            color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 5px rgb(255, 201, 7),
                0 0 25px rgb(255, 201, 7),
                0 0 50px rgb(255, 201, 7),
                0 0 100px rgb(255, 201, 7);
        }

        .login-box a span {
            position: absolute;
            display: block;
        }

        button{
            background-color: transparent;
            cursor: pointer;
        }
        a.submit{
            border-width: 2px;
            border-style: outset;
            border-color: buttonborder;

        }
    </style>
    <body>
        <div class="login-box">
            <h2>ĐĂNG KÍ KHÓA HỌC ${course.title}</h2>
            <form action="payment-course" id="frmCreateOrder">
                <input value="${course.id}" name="courseID" type="hidden">
                <input value="${course.title}" name="courseTitle" type="hidden">
                <label>Giá khóa học</label>
                <div class="user-box">
                    <input type="text" name="amount" readonly value="${course.price} vnd">
                </div>
                <button class="submit" style="submit" >
                    thanh toán
                </button>
                <a class="submit" href="home" style="margin-left:21px ;">
                    hủy bỏ
                </a>
            </form>
        </div>        
        <link href="https://pay.vnpay.vn/lib/vnpay/vnpay.css" rel="stylesheet" />
        <script src="https://pay.vnpay.vn/lib/vnpay/vnpay.min.js"></script> 
        <script type="text/javascript">
            $("#frmCreateOrder").submit(function () {
                var postData = $("#frmCreateOrder").serialize();
                var submitUrl = $("#frmCreateOrder").attr("action");
                $.ajax({
                    type: "POST",
                    url: submitUrl,
                    data: postData,
                    dataType: 'JSON',

                    success: function (x) {
                        console.log(x.data);
                        if (x.code === '00') {
                            location.href = x.data;
                            return false;
                        } else {
                            alert(x.Message);
                        }
                    }
                });
                return false;
            });
        </script>   
    </body>
</html> 

