<%-- 
    Document   : forms
    Created on : May 20, 2022, 11:42:21 PM
    Author     : Luu Duc Thang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Css -->

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
        <base href="${path}">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/style.css">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <title>Forms | Tailwind Admin</title> 
    </head>

    <body>
        <!--Container -->
        <div class="mx-auto bg-grey-lightest">
            <!--Screen-->
            <div class="min-h-screen flex flex-col">
                <!--Header Section Starts Here-->
                <%@include file="header.jsp" %>
                <!--/Header-->

                <div class="flex flex-1">
                    <!--Sidebar-->
                    <%@include file="sidebar.jsp" %>
                    <!--/Sidebar-->
                    <!--Main-->
                    <main class="bg-white-500 flex-1 p-3 overflow-hidden">
                        <div class="content">
                            <div class="container">
                                <div class="table-responsive">    
                                    <div class="select-dropdown">    
                                        <table>
                                            <tr>
                                                <td>
                                                    <select>
                                                        <option value="Option 1">First Option</option>
                                                        <option value="Option 2">2nd Option</option>
                                                        <option value="Option 3">Option Number 3</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select>
                                                        <option value="Option 1">First Option</option>
                                                        <option value="Option 2">2nd Option</option>
                                                        <option value="Option 3">Option Number 3</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <button class="btn btn-xs btn-dark" type="submit">Thêm tài khoản</button>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-md-10 col-lg-8">
                                                <form class="card card-sm">
                                                    <div class="row no-gutters align-items-center">
<!--                                             
                                                        <!--end of col-->
                                                        <div class="col">
                                                            <input class="form-control form-control-lg form-control-borderless" type="search"
                                                                   placeholder="Tìm kiếm ...">
                                                        </div>
                                                        <!--end of col-->
                                                        <div class="col-auto">
                                                            <button class="btn btn-lg btn-dark" type="submit">Search</button>
                                                        </div>
                                                        <!--end of col-->
                                                    </div>
                                                </form>
       
                                            </div>
                                            <!--end of col-->
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End div center   -->
                            <div class="table-responsive">

                                <table class="table custom-table">
                                    <thead>
                                        <tr>

                                            <th scope="col">ID</th>
                                            <th scope="col">Tài khoản</th>
                                            <th scope="col">Họ và tên</th>
                                            <th scope="col">Địa chỉ</th>
                                            <th scope="col">Số điện thoại</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Quyền vụ</th>
                                            <th scope="col">Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr scope="row">
                                            <td>
                                                1392
                                            </td>
                                            <td>James Yates</td>
                                            <td>
                                                Web Designer
                                                <small class="d-block">Far far away, behind the word mountains</small>
                                            </td>
                                            <td>+63 983 0962 971</td>
                                            <td>NY University</td>
                                            <td>+63 983 0962 971</td>
                                            <td>NY University</td>
                                            <td>
                                                <button class="btn btn-xs btn-dark" type="submit">Chỉnh sửa</button>
                                                <button class="btn btn-xs btn-dark" type="submit">xoá</button>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                </div>


                <script src="js/jquery-3.3.1.min.js"></script>
                            <script src="js                    /popper.min.js                    "></scrip >
                                <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
        <script>
$("#inpt_search").on('focus', function () {
                                        $(this).parent('label').addClass('active');
});
        
        $("#inpt_search").on('blur', function () {
                                        if ($(this).val().length == 0)
                                        $(this).parent('label').removeClass('active');
        });</script>
                        
                    </main>
                    <!--/Main-->
                </div>
                <!--Footer-->
                <footer class="bg-grey-darkest text-white p-2">
                    <div class="flex flex-1 mx-auto"> </div>
     </footer>
               <!--/footer-->

       </div>

   </div>

        <script src="../JS/main.js"></script>

    </body> 
</html>
