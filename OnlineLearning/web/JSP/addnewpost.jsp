<%-- 
    Document   : addnewpost
    Created on : Jun 19, 2022, 12:56:34 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Css -->
        <!--     <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">-->
        <base href="${path}">
        <link rel="stylesheet" href="CSS/styles.css">
        <link rel="stylesheet" href="CSS/all.css">
        <link rel="stylesheet" href="CSS/style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>
    <body>
        <%@include file="sidebar.jsp" %>
        <br>
        <br>
        <br>
       <div class="card mb-3 container mt-3 bg-light text-dark" style="max-width: 540px;">
  <div class="row g-0">
    <div class="col-md-4 text-justify">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdjffkleHVsvuhP8oRE2yTlo7XCrpjBRmWpQ&usqp=CAU" class="img-fluid rounded-start" alt="...">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">ADD NEW POST</h5>
        <p class="card-text">NEW POST WILL DISPLAY AT HOMEPAGE</p>
        
      </div>
    </div>
  </div>
</div>
       
        <div class="container mt-3 bg-light text-dark">
            <form action="add-new-post" method="post">

                <div class="was-validated col-md-6">
                    <div class="mb-3">
                        <label for="validationTextarea" class="form-label">Title</label>
                        <textarea class="form-control is-invalid" name="title" id="validationTextarea" placeholder="Required title " required></textarea>

                    </div>
                    <div class="mb-3">
                        <label for="validationTextarea" class="form-label">Detail</label>
                        <textarea class="form-control is-invalid" name="detail" id="validationTextarea" placeholder="Required detail " required></textarea>

                    </div>
                    <label for="validationTextarea" class="form-label">Status of post</label>
                    <div class="mb-3">
                        <select class="form-select" name="status" required aria-label="select example">

                            <option value="0">Disable</option>
                            <option value="1">Active</option>

                        </select>
                        <label for="validationTextarea" class="form-label">Category</label>
                        <div class="mb-3">
                            <select class="form-select" name="Category" required aria-label="select example">

                                <option  value="Notify">Notify</option>
                                <option  value="News">News</option>

                            </select>
                            <div class="invalid-feedback">Example invalid select feedback</div>
                        </div>

                        <div class="col-md-4">
                            <label for="validationDefault01" class="form-label" required>Post thumbnail</label>
                            <input type="text" class="form-control" name="thumbnail" id="validationDefault01" required>
                        </div>
                        <br>

                        <div class="mb-3">
                            <button type="submit" class="btn btn-outline-secondary">Save</button>                  
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </body>
    <footer class="bg-grey-darkest text-white p-2">
        <div class="flex flex-1 mx-auto"></div>
        <div class="flex flex-1 mx-auto"></div>
    </footer>
    <!--/footer-->
    <%@include file="footer.jsp" %>
</html>
