/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Phong Linh
 */
public class QuizHistory {
    private int id;
    private int quizId;
    private String taker;
    private Date takeTime;
    private Date finishTime;
    private double point;
    private List<QuizQuestion> questionList;

    public QuizHistory() {
    }
    
    public QuizHistory(int id, int quizId, String taker, Timestamp takeTime, Timestamp finishTime, double point, List<QuizQuestion> questionList) {
        this.id = id;
        this.quizId = quizId;
        this.taker = taker;
        this.takeTime = new Date(takeTime.getTime());
        this.finishTime = new Date(finishTime.getTime());
        this.point = point;
        this.questionList = questionList;
    }

    public QuizHistory(int id, int quizId, String taker, Date takeTime, Date finishTime, double point, List<QuizQuestion> questionList) {
        this.id = id;
        this.quizId = quizId;
        this.taker = taker;
        this.takeTime = takeTime;
        this.finishTime = finishTime;
        this.point = point;
        this.questionList = questionList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public String getTaker() {
        return taker;
    }

    public void setTaker(String taker) {
        this.taker = taker;
    }

    public Date getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(Date takeTime) {
        this.takeTime = takeTime;
    }
    
    public void setTakeTime(Timestamp takeTime) {
        this.takeTime = new Date(takeTime.getTime());
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
    
    public void setFinishTime(Timestamp finishTime) {
        this.finishTime = new Date(finishTime.getTime());
    }

    public double getPoint() {
        return point;
    }

    public void setPoint(double point) {
        this.point = point;
    }
    
    public List<QuizQuestion> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<QuizQuestion> questionList) {
        this.questionList = questionList;
    }
    public boolean isMarked(int questionId) {
        for (QuizQuestion q : questionList) {
            if (q != null && q.getId() == questionId) return q.isMarked();
        }
        return false;
    }

    public int getCorrectCount() {
        int correct = 0;
        for (QuizQuestion q : questionList) {
            for (QuizAnswer ans : q.getAnswerList()) {
                if (q.getChoosenAnswer() == ans.getId() && ans.isCorrect()) {
                    correct++;
                }
            }
        }
        return correct;
    }
    public int getWrongCount() {
        int wrong = 0;
        for (QuizQuestion q : questionList) {
            if (q.getChoosenAnswer() > 0) {
                for (QuizAnswer ans : q.getAnswerList()) {
                    if (q.getChoosenAnswer() == ans.getId() && !ans.isCorrect()) {
                        wrong++;
                    }
                }
            }
        }
        return wrong;
    }
    public int getUnansweredCount() {
        int un = 0;
        for (QuizQuestion q : questionList) {
            if (q.getChoosenAnswer() < 1) un++;
        }
        return un;
    }
    
    @Override
    public String toString() {
        return "QuizHistory{" + "id=" + id + ", quizId=" + quizId + ", taker=" + taker + ", takeTime=" + takeTime + ", finishTime=" + finishTime + ", point=" + point + ", questionList=" + questionList + '}';
    }
}
