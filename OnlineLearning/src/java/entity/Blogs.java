/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class Blogs {
   public String id;
   public String Account_id;
   public String title;
   public String detail;
   public String category;
   public String thumbnail;
   public Date timeCreate;
   public int status;

    public Blogs(String newtitle, String newdetail, String newcategory, String thumbnail) {

        this.title = newtitle;
        this.detail = newdetail;
        this.category = newcategory;
        this.thumbnail = thumbnail;  
   }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public Blogs() {
    }

    public Blogs(String id, String Account_id, String title, String detail, String category, String thumbnail, Date timeCreate) {
        this.id = id;
        this.Account_id = Account_id;
        this.title = title;
        this.detail = detail;
        this.category = category;
        this.thumbnail = thumbnail;
        this.timeCreate = timeCreate;
    }

    public Blogs(String title, String detail, String category, String thumbnail, String authorid) {
        this.Account_id = authorid;
        this.title = title;
        this.detail = detail;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_id() {
        return Account_id;
    }

    public void setAccount_id(String Account_id) {
        this.Account_id = Account_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Date getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(Date timeCreate) {
        this.timeCreate = timeCreate;
    }

    @Override
    public String toString() {
        return super.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

}
