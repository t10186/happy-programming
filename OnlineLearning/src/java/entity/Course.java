/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class Course {

    private int id;
    private String title;
    private int price;
    private String imageCourse;
    private String description;
    private ArrayList<Requiment> requirement;
    private boolean status;
    private Date dateCreateCourse;
    private double persent;

    public Date getDateCreateCourse() {
        return dateCreateCourse;
    }

    public double getPersent() {
        return persent;
    }

    public void setPersent(double persent) {
        this.persent = persent;
    }

    public Course(int id, String title, int price, String imageCourse, String description, ArrayList<Requiment> requirement, boolean status, Date dateCreateCourse, double persent) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.imageCourse = imageCourse;
        this.description = description;
        this.requirement = requirement;
        this.status = status;
        this.dateCreateCourse = dateCreateCourse;
        this.persent = persent;
    }

    public void setDateCreateCourse(Date dateCreateCourse) {
        this.dateCreateCourse = dateCreateCourse;
    }

    public Course() {
    }

    public Course(int id, int coursetypeID, int Money, String title, String subTitle, int price, String imageCourse, String description, ArrayList<Requiment> requirement) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.imageCourse = imageCourse;
        this.description = description;
        this.requirement = requirement;
    }

    public Course(String title, String subTitle, int price, String imageCourse, int coursetypeID, String description, ArrayList<Requiment> requirement) {
        this.title = title;
        this.price = price;
        this.imageCourse = imageCourse;
        this.description = description;
        this.requirement = requirement;

    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageCourse() {
        return imageCourse;
    }

    public void setImageCourse(String imageCourse) {
        this.imageCourse = imageCourse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Requiment> getRequirement() {
        return requirement;
    }

    public void setRequirement(ArrayList<Requiment> requirement) {
        this.requirement = requirement;
    }

}
