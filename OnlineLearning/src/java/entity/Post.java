/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author Admin
 */
public class Post {

    private String detail;
    private String title;
    private String author_id;
    private int post_Id;
    private Date timeCreate;
    private String category;
    private String thumbnail;
    private int status;
    public Post() {
    }

    public Post(String detail, String title, String author_id, int post_Id, String category, String thumbnail, int status) {
        this.detail = detail;
        this.title = title;
        this.author_id = author_id;
        this.post_Id = post_Id;
        this.category = category;
        this.thumbnail = thumbnail;
        this.status = status;
    }

    public Post(String detail, String title, String author_id, int post_Id, Date timeCreate, String category, String thumbnail, int status) {
        this.detail = detail;
        this.title = title;
        this.author_id = author_id;
        this.post_Id = post_Id;
        this.timeCreate = timeCreate;
        this.category = category;
        this.thumbnail = thumbnail;
        this.status = status;
    }

    public Post(String detail, String title, String authorid, String category, String thumbnail, int status) {
        this.detail = detail;
        this.title = title;
        this.author_id = authorid;
         this.category = category;
        this.thumbnail = thumbnail;
        this.status = status;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public int getPost_Id() {
        return post_Id;
    }

    public void setPost_Id(int post_Id) {
        this.post_Id = post_Id;
    }

    public Date getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(Date timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

   
}