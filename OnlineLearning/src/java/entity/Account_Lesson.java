/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Admin
 */
public class Account_Lesson {

    public Account account;
    public Lesson lesson;
    public boolean lesson_unlock;

    public Account_Lesson() {
    }

    public Account_Lesson(Account account, Lesson lesson, boolean lesson_unlock) {
        this.account = account;
        this.lesson = lesson;
        this.lesson_unlock = lesson_unlock;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public boolean isLesson_unlock() {
        return lesson_unlock;
    }

    public void setLesson_unlock(boolean lesson_unlock) {
        this.lesson_unlock = lesson_unlock;
    }

}
