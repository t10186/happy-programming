/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.List;

/**
 *
 * @author Phong Linh
 */
public class QuizQuestion {
    private int id;
    private int subject;
    private String question;
    private String explaination;
    private boolean marked;
    private int choosenAnswer;
    private List<QuizAnswer> answerList;

    public QuizQuestion() {
    }

    public QuizQuestion(int id, String question, String explaination, boolean marked, int choosenAnswer, List<QuizAnswer> answerList) {
        this.id = id;
        this.question = question;
        this.explaination = explaination;
        this.marked = marked;
        this.choosenAnswer = choosenAnswer;
        this.answerList = answerList;
        this.subject = -1;
    }

    public QuizQuestion(int id, int subject, String question, String explaination, boolean marked, int choosenAnswer, List<QuizAnswer> answerList) {
        this.id = id;
        this.subject = subject;
        this.question = question;
        this.explaination = explaination;
        this.marked = marked;
        this.choosenAnswer = choosenAnswer;
        this.answerList = answerList;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getExplaination() {
        return explaination;
    }

    public void setExplaination(String explaination) {
        this.explaination = explaination;
    }

    public boolean isMarked() {
        return marked;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public int getChoosenAnswer() {
        return choosenAnswer;
    }

    public void setChoosenAnswer(int choosenAnswer) {
        this.choosenAnswer = choosenAnswer;
    }

    public List<QuizAnswer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<QuizAnswer> answerList) {
        this.answerList = answerList;
    }
    public QuizAnswer getCurrentAnswer() {
        if (answerList == null || choosenAnswer < 0 || choosenAnswer >= answerList.size()) {
            return null;
        }
        return answerList.get(choosenAnswer);
    }
}
