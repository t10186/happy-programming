/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author baobao
 */
public class Slide {

    private int id;
    private String listimg[];
    private Date timeCreate;
    private String name;
    private String description;
    private int status;
    private String img;
    public Slide() {
    }


    public Slide(String image, java.sql.Date date, String name, String desciption) {
        this.timeCreate = date;
        this.name = name;
        this.description = desciption;
        this.img = image;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String[] getListimg() {
        return listimg;
    }

    public void setListimg(String[] listimg) {
        this.listimg = listimg;
    }

    public Date getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(Date timeCreate) {
        this.timeCreate = timeCreate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}

