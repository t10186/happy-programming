/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Quynh_Nhu
 */
public class Account_Course {

    int account_id;
    int course_id;
    double lesson_unlock;
    double total_lesson;
    double persent;

    public Account_Course() {
    }

    public double getPersent() {
        return persent;
    }

    public void setPersent(double persent) {
        this.persent = persent;
    }

    public Account_Course(int account_id, int course_id, double lesson_unlock, double total_lesson, double persent) {
        this.account_id = account_id;
        this.course_id = course_id;
        this.lesson_unlock = lesson_unlock;
        this.total_lesson = total_lesson;
        this.persent = persent;
    }

    public Account_Course(int account_id, int course_id, double lesson_unlock, double total_lesson) {
        this.account_id = account_id;
        this.course_id = course_id;
        this.lesson_unlock = lesson_unlock;
        this.total_lesson = total_lesson;
    }

    public Account_Course(int account_id, int course_id) {
        this.account_id = account_id;
        this.course_id = course_id;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public double getLesson_unlock() {
        return lesson_unlock;
    }

    public void setLesson_unlock(double lesson_unlock) {
        this.lesson_unlock = lesson_unlock;
    }

    public double getTotal_lesson() {
        return total_lesson;
    }

    public void setTotal_lesson(double total_lesson) {
        this.total_lesson = total_lesson;
    }

    @Override
    public String toString() {
        return "Account_Course{" + "account_id=" + account_id + ", course_id=" + course_id + '}';
    }

}
