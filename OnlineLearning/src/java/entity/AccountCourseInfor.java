/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Luu Duc Thang
 */
public class AccountCourseInfor {
    private Account acc;
    private Course cour;
    private Information infor;

    public AccountCourseInfor() {
    }

    public AccountCourseInfor(Account acc, Course cour, Information infor) {
        this.acc = acc;
        this.cour = cour;
        this.infor = infor;
    }

    public Account getAcc() {
        return acc;
    }

    public void setAcc(Account acc) {
        this.acc = acc;
    }

    public Course getCour() {
        return cour;
    }

    public void setCour(Course cour) {
        this.cour = cour;
    }

    public Information getInfor() {
        return infor;
    }

    public void setInfor(Information infor) {
        this.infor = infor;
    }

    @Override
    public String toString() {
        return "AccountCourseInfor{" + "acc=" + acc + ", cour=" + cour + ", infor=" + infor + '}';
    }

    
}
