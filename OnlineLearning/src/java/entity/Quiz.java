/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author Phong Linh
 */
public class Quiz {
    private int id;
    private int subject;
    private int type;
    private String tittle;
    private String Description;
    private int length;
    private double maxPoint;
    private double passRate;
    private int questionCount;

    public Quiz() {
    }

    public Quiz(int id, String tittle, String Description, int length, double maxPoint, int questionCount, double passRate, int subject, int type) {
        this.id = id;
        this.tittle = tittle;
        this.Description = Description;
        this.length = length;
        this.maxPoint = maxPoint;
        this.passRate = passRate;
        this.questionCount = questionCount;
        this.type = type;
        this.subject = subject;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getPassRate() {
        return passRate;
    }

    public void setPassRate(double passRate) {
        this.passRate = passRate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public double getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(double maxPoint) {
        this.maxPoint = maxPoint;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }
}
