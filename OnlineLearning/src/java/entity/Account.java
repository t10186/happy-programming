/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class Account {

    private int id;
    private int roleID;
    private String username;
    private String password;
    private String email;
    private Date dateRegister;
    private boolean status =true;

    public Account() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public Account(int id, int roleID, String username, String password, String email) {
        this.id = id;
        this.roleID = roleID;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public Account(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email; 
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", roleID=" + roleID + ", username=" + username + ", password=" + password + ", email=" + email + '}';
    }

}
