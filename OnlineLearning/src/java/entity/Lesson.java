/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class Lesson {
    private int id;
    private int courseID;
    private int unit;
    private String title;
    private String detail;
    private String content;
    private String linkVideo;
    private String lessonTime;
    private Date dateCreate;
    private boolean status;

    public String getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(String lessonTime) {
        this.lessonTime = lessonTime;
    }

    public Lesson(int id, int unit, String title, String detail, String content, String linkVideo, String lessonTime, Date dateCreate, boolean status) {
        this.id = id;
        this.unit = unit;
        this.title = title;
        this.detail = detail;
        this.content = content;
        this.linkVideo = linkVideo;
        this.lessonTime = lessonTime;
        this.dateCreate = dateCreate;
        this.status = status;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Lesson() {
    }

    public Lesson(int id, int courseID, int unit, String title, String detail, String content, String linkVideo) {
        this.id = id;
        this.courseID = courseID;
        this.unit = unit;
        this.title = title;
        this.detail = detail;
        this.content = content;
        this.linkVideo = linkVideo;
    }

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }


    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLinkVideo() {
        return linkVideo;
    }

    public void setLinkVideo(String linkVideo) {
        this.linkVideo = linkVideo;
    }

    @Override
    public String toString() {
        return "Lesson{" + "id=" + id + ", courseID=" + courseID + ", status_id=" + ", unit=" + unit + ", title=" + title + ", detail=" + detail + ", content=" + content + ", linkVideo=" + linkVideo + '}';
    }
    
}
