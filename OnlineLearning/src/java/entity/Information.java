/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.sql.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class Information {
    private int id;
    private int countryID;
    private int statusID;
    private String displayName;
    private String fullName;
    private String address;
    private int phone;
    private String gender;
    private Date DOB;
    private String avatar;

    public Information() {
    }

    public Information(int id, int countryID, String fullName, String address, int phone, String gender, String avatar) {
        this.id = id;
        this.countryID = countryID;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.gender = gender;
        this.avatar = avatar;
    }

    public Information(int id, int countryID, int statusID, String displayName, String fullName, String address, int phone, String gender, Date DOB, String avatar) {
        this.id = id;
        this.countryID = countryID;
        this.statusID = statusID;
        this.displayName = displayName;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.gender = gender;
        this.DOB = DOB;
        this.avatar = avatar;
    }

    public Information(int id ,int countryID, String fullName, String address, int phone, String gender, Date DOB, String avatar) {
        this.id = id;
        this.countryID = countryID;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.gender = gender;
        this.DOB = DOB;
        this.avatar = avatar;
    }

    public Information(String fullName, String address, int phone) {
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryID() {
        return countryID;
    }

    public void setCountryID(int countryID) {
        this.countryID = countryID;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }


    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "Information{" + "id=" + id + ", countryID=" + countryID + ", statusID=" + statusID + ", displayName=" + displayName + ", fullName=" + fullName + ", address=" + address + ", phone=" + phone + ", gender=" + gender + ", DOB=" + DOB + ", avatar=" + avatar + '}';
    }
    
    
    
}
