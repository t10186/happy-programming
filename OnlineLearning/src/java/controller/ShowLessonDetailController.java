/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CourseAccountDAO;
import dao.CourseDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Account_Lesson;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
@WebServlet(name = "ShowLessonDetailController", urlPatterns = {"/show-lesson-detail"})
public class ShowLessonDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowLessonDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowLessonDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idLesson = request.getParameter("idShow");
        LessonDAO lessonDAO = new LessonDAO();
        HttpSession session = request.getSession();
        Account acc = (Account) session.getAttribute("acc");
        ArrayList<Account_Lesson> listLessonUnlock = lessonDAO.getAllByIDUnlock(1, acc.getId());

        CourseAccountDAO courseAccountDAO = new CourseAccountDAO();
        int lesson_unlock = courseAccountDAO.lesson_unlock(1, acc.getId());

        int idShowLesson = Integer.parseInt(idLesson);
        Lesson lesson = lessonDAO.getLessonByID(idShowLesson);

        String lesson_time = lesson.getLessonTime();
        String[] hours = lesson_time.split("H");
        double hours1 = Double.parseDouble(hours[0]);

        String hoString = hours[1];
        String[] minute = hoString.split("M");
        double minute1 = Double.parseDouble(minute[0]);
        String mString = minute[1];
        String[] second = mString.split("S");
        double second1 = Double.parseDouble(second[0]);
        double sum_second = (hours1 * 3600 + minute1 * 60 + second1) * 70 / 100;

        int totalLesson = listLessonUnlock.size();

        CourseDAO courseDAO = new CourseDAO();
        double total_Lesson = courseDAO.total_Lesson(lesson.getCourseID());
        double total_Unlock = courseAccountDAO.lesson_unlock(lesson.getCourseID(), acc.getId());
        double persent = Math.round((total_Unlock / total_Lesson) * 100);
        int process_persent = (int) persent;
        request.setAttribute("process_persent", process_persent);

//        ---------------------------------------------------------------------
//        Phân tích video_link
        String lesson_video = lesson.getLinkVideo();
        String[] words = lesson_video.split("v=");
        String vd = words[1];
        String[] words1 = vd.split("&");
        for (String string : words1) {
            System.out.println(string);
        }
        String key_video_id = words1[0];

        double a = 211.537648;
        request.setAttribute("sumSecond", sum_second);
        request.setAttribute("key_video_id", key_video_id);
//        ---------------------------------------------------------------------

//        ---------------------------------------------------------------------
//Cho phép qua bài học
//        ---------------------------------------------------------------------
        request.setAttribute("course_id", lesson.getCourseID());
        request.setAttribute("lesson_unlock", lesson_unlock);
        request.setAttribute("lessonShow", lesson);
        request.setAttribute("lesson", lesson);
        request.setAttribute("listLessonUnlock", listLessonUnlock);
//        request.setAttribute("course", course);
        request.setAttribute("totalLesson", totalLesson);
        request.getRequestDispatcher("JSP/lessonDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
