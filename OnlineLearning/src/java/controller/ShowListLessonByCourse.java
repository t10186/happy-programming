/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CourseDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Course;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
@WebServlet(name = "ShowListLessonByCourse", urlPatterns = {"/show-list-lesson-course"})
public class ShowListLessonByCourse extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowListLessonByCourse</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowListLessonByCourse at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account acc = (Account) session.getAttribute("acc");
        CourseDAO courseDAO = new CourseDAO();
        ArrayList<Course> listCourses = new ArrayList<>();
        listCourses = courseDAO.getAllCourseOfDevoloper(acc.getId(), acc.getRoleID());
        request.setAttribute("listNameCourse", listCourses);
        int course_id = Integer.parseInt(request.getParameter("courseid"));
        LessonDAO lessonDAO = new LessonDAO();
        ArrayList<Lesson> listLessonByCourse = new ArrayList<>();
        listLessonByCourse = lessonDAO.getLessonAllByIDCourse(course_id);
        request.setAttribute("listLessonByCourse", listLessonByCourse);
        Course course = courseDAO.getCourseByID(course_id);
            request.setAttribute("course_title", course.getTitle());
            request.setAttribute("course_id_info", course_id);
        request.getRequestDispatcher("JSP/showManageLesson.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account) request.getSession().getAttribute("acc");
        //Lấy ra tên các khoá học
        CourseDAO courseDAO = new CourseDAO();
        ArrayList<Course> listCourses = new ArrayList<>();
        listCourses = courseDAO.getAllCourseOfDevoloper(acc.getId(), acc.getRoleID());
        request.setAttribute("listNameCourse", listCourses);
        //Lấy ra các bài học yêu cầu
        LessonDAO lessonDAO = new LessonDAO();
        ArrayList<Lesson> listLessonByCourse = new ArrayList<>();
        listLessonByCourse = lessonDAO.getAllLessonHidden();
        request.setAttribute("listLessonByCourse", listLessonByCourse);
        request.getRequestDispatcher("JSP/showManageLesson.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
