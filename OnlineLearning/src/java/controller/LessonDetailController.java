/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CourseAccountDAO;
import dao.CourseDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Account_Lesson;
import entity.Course;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class LessonDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int courseID = Integer.parseInt(request.getParameter("courseID"));
        HttpSession session = request.getSession();
        
        Account acc = (Account) session.getAttribute("acc");
        
        CourseDAO courseDAO = new CourseDAO();
        Course course = courseDAO.getCourseByID(courseID);
        session.setAttribute("course", course);
        LessonDAO lessonDAO = new LessonDAO();
        ArrayList<Account_Lesson> listLessonUnlock = lessonDAO.getAllByIDUnlock(courseID, acc.getId());
        
        CourseAccountDAO courseAccountDAO = new CourseAccountDAO();
        double total_Lesson = courseDAO.total_Lesson(courseID);
        double total_Unlock = courseAccountDAO.lesson_unlock(courseID, acc.getId());
        double persent = Math.round((total_Unlock / total_Lesson) * 100);
        int process_persent = (int) persent;
        request.setAttribute("process_persent", process_persent);
        
        
        request.setAttribute("key_video_id", "aplTmzoy3b8");
//        Lesson currentLesson = lessonDAO.getFirstLesson(courseID);
        request.setAttribute("listLessonUnlock", listLessonUnlock);
//        request.setAttribute("currentLesson", currentLesson);
        request.getRequestDispatcher("JSP/lessonDetail.jsp").forward(request, response); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
