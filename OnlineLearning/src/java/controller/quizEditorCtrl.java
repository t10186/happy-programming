/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dao.QuizDAO;
import entity.QuizAnswer;
import entity.QuizQuestion;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author Phong Linh
 */
@WebServlet(name = "quizEditorCtrl", urlPatterns = {"/quizEditorCtrl"})
public class quizEditorCtrl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet quizEditorCtrl</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet quizEditorCtrl at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (haveParameter(request.getParameter("getQuestions"))) {
            
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        QuizDAO dao = new QuizDAO();
        if (haveParameter(request.getParameter("saveQuestion"))) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                QuizQuestion q = new Gson().fromJson(request.getParameter("data"), QuizQuestion.class);
                if (q == null) throw new Exception("Add a null value");
                out.write("{\"success\": \"" + dao.updateQuestion(q.getId(), q.getQuestion(), q.getExplaination(), q.getAnswerList(), q.getChoosenAnswer())
                        + "\", \"content\": {\"id\": " + q.getId() + "}}");
                dao.Close();
                out.flush();
            } catch (Exception e) {
                dao.Close();
                out.write("{\"success\": \"false\", \"content\": \"" + e.toString() + "\"}");
                out.flush();
            }
            //dao.Close();
        } else if (haveParameter(request.getParameter("saveQuizInfo"))) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                int quizID = Integer.parseInt(request.getParameter("quizID"));
                String title = request.getParameter("quizTitle");
                String desc = request.getParameter("quizDesc");
                int length = string2Second(request.getParameter("quizDuration"));
                int maxPoint = Integer.parseInt(request.getParameter("quizMaxPoint"));
                out.write("{\"success\": \"" + dao.updateQuizInfo(quizID, title, desc, length, maxPoint) + "\"}");
                dao.Close();
                out.flush();
            } catch (NumberFormatException e) {
                dao.Close();
                out.write("{\"success\": \"" + e.getLocalizedMessage() + "\"}");
                out.flush();
            }
        } else if (haveParameter(request.getParameter("addQuestion"))) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                int quizID = Integer.parseInt(request.getParameter("quizID"));
                QuizQuestion q = dao.addQuestion(quizID);
                if (q == null) throw new Exception("Add a null value");
                out.write("{\"success\": \"true\", \"content\": " + new Gson().toJson(q, QuizQuestion.class) + "}");
                dao.Close();
                out.flush();
            } catch (Exception e) {
                dao.Close();
                out.write("{\"success\": \"false\", \"content\": \"" + e.getMessage() + "\"}");
                out.flush();
            }
        } else if (haveParameter(request.getParameter("addAnswer"))) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                int questionID = Integer.parseInt(request.getParameter("questionID"));
                QuizAnswer a = dao.addAnswer(questionID, false, "New Answer");
                if (a == null) throw new Exception("Add a null value");
                out.write("{\"success\": \"true\", \"content\": " + new Gson().toJson(a, QuizAnswer.class) + "}");
                dao.Close();
                out.flush();
            } catch (Exception e) {
                dao.Close();
                out.write("{\"success\": \"false\", \"content\": \"" + e.getMessage() + "\"}");
                out.flush();
            }
        } else if (haveParameter(request.getParameter("deleteAnswer"))) {
            try {
                dao.deleteAnswer(Integer.parseInt(request.getParameter("answerID")));
            } catch (NumberFormatException e) {
            }
        } else if (haveParameter(request.getParameter("deleteQuiz"))) {
            try {
                dao.deleteQuiz(Integer.parseInt(request.getParameter("quizID")));
                //redirect to home
                response.sendRedirect("../home");
            } catch (NumberFormatException e) {
            }
        } else if (haveParameter(request.getParameter("deleteQuestion"))) {
            try {
                dao.deleteQuestion(Integer.parseInt(request.getParameter("questionID")));
            } catch (NumberFormatException e) {
            }
        }
    }
    private int string2Second(String str) {
        String[] parts = str.split(":");
        int s = parseTime(parts[0]) * 3600;
        s += parseTime(parts[1]) * 60;
        s += parseTime(parts[2]);
        return s;
    }
    private int parseTime(String s) {
        if (s.matches("[0-9]{2}")) {
            return Integer.parseInt(s);
        }
        return 0;
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private boolean haveParameter(String s) {
        return !(s == null || s.isEmpty()) && s.equals("true");
    }
}
