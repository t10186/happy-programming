/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CourseDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Course;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author Quynh_Nhu
 */
public class AddNewLessonController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewLessonController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewLessonController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("JSP/addNewLesson.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
//            Account acc = (Account) request.getSession().getAttribute("acc");
//            int course_id = Integer.parseInt(request.getParameter("course_id"));
            int course_id = 1;
            int lesson_unit = Integer.parseInt(request.getParameter("lesson_unit"));
            String lesson_title = request.getParameter("lesson_title");
            String lesson_description = request.getParameter("lesson_description");
            String lesson_content = request.getParameter("lesson_content");
            String lesson_video = request.getParameter("lesson_video");
            String lesson_time_hours = request.getParameter("lesson_time_hours");
            String lesson_time_minute = request.getParameter("lesson_time_minute");
            String lesson_time_second = request.getParameter("lesson_time_second");
            String lesson_time = lesson_time_hours + "H" + lesson_time_minute + "P" + lesson_time_second + "S";
            
            String error = "";
            boolean check_unit = false;
            LessonDAO lessonDAO = new LessonDAO();
            ArrayList<Lesson> lessons = lessonDAO.getAllByID(1);
            for (Lesson lesson : lessons) {
                if (lesson.getUnit() == lesson_unit) {
                    check_unit = true;
                    error = "[Bài học đã tồn tại]";
                }
            }
            if (check_unit == false) {
                Lesson lesson = new Lesson();
                lesson.setCourseID(course_id);
                lesson.setUnit(lesson_unit);
                lesson.setTitle(lesson_title);
                lesson.setDetail(lesson_description);
                lesson.setContent(lesson_content);
                lesson.setLinkVideo(lesson_video);
                lesson.setLessonTime(lesson_time);
                lessonDAO.addLesson(lesson);
                error = "[Thêm bài học mới thành công !]";
            }

//            if (lessonDAO.getunit(lesson_unit, course_id) == 0) {
//                lessonDAO.addLesson(lesson);
//            } else {
//                System.out.println("Unit is not duplicate!!!");
//            }
//            response.sendRedirect("list-lesson?courseID=" + course_id);
            request.setAttribute("error", error);
            request.getRequestDispatcher("JSP/addNewLesson.jsp").forward(request, response);
        } catch (Exception e) {
            System.out.println("unit is not Duplicate!!!");
        }
    }
    
    public boolean CheckDuplicate() {
        return false;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
