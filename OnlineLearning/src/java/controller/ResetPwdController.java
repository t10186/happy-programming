/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import SendEmail.SendMail;
import dao.AccountDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

/**
 *
 * @author Phong Linh
 */
public class ResetPwdController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResetPwdController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResetPwdController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            //email-timefrom-timeto
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        ServletContext context = request.getServletContext();
        AccountDAO accMng = new AccountDAO();
        String username = accMng.getUsername(email);
        
        if (username == null) {
            request.setAttribute("reseterror", "Không có tài khoản nào đăng ký với \"" + email +"\"!");
            context.getRequestDispatcher("/JSP/resetPwd.jsp").forward(request, response);
            return;
        }
        //TODO: ENcrypt 
        
        //TEMPORARY
        String path = request.getScheme() + "://" +request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        String changePwdUrl = path + "/JSP/resetPwd.jsp?usn=" + Base64.getMimeEncoder().encodeToString(username.getBytes());
        SendMail.sendmail(email/*"minhnthe163336@fpt.edu.vn"*/, "Verify password changing", "Please ignore if you don't want to change password. Click the link below to change your password:\n" + changePwdUrl);
        //TODO: set start-expire time
        response.sendRedirect("JSP/login.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
