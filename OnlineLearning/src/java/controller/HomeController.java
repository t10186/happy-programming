/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.BlogsDAO;
import dao.CourseAccountDAO;
import dao.CourseDAO;
import dao.PostDAO;
import entity.Account;
import entity.AccountCourseInfor;
import entity.Blogs;
import entity.Course;
import entity.Information;
import entity.Post;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Luu Duc Thang
 */
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet homeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet homeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account acc = (Account) request.getSession().getAttribute("acc");
        String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
        request.getSession().setAttribute("path", path);
        //Tất cả các khoá học
        ArrayList<Course> listCourse = new ArrayList<>();
        CourseDAO courseDAO = new CourseDAO();
        listCourse = courseDAO.getTop4Course(4);
        request.setAttribute("listCourse", listCourse);
        //TOP Các khoá học hot
        HashMap<Course, Integer> listCoursePopular = new HashMap<Course, Integer>();
        CourseAccountDAO courseAccountDAO = new CourseAccountDAO();
        listCoursePopular = courseAccountDAO.getAllCourseByAccountRes(4);
        request.setAttribute("listCoursePopular", listCoursePopular);
        
        
        ArrayList<AccountCourseInfor> listExpert = new ArrayList<>();
        
        
        
        
        
        
        BlogsDAO blogsDAO = new BlogsDAO();
        ArrayList<Blogs> listBlogses = new ArrayList<>();
        listBlogses = blogsDAO.getTop4AllBlogs(4);
        request.setAttribute("listBlogses", listBlogses);

        //Các khoá học nhiều người đăng ký
        //Thông tin giảng viên được đăng ký nhiều
//        ArrayList<AccountCourseInfor> listExperts = new ArrayList<>();
//        listExperts = courseAccountDAO.getAllInforExpert();
//        for (int i = 0; i <= listExperts.size(); i++) {
//            int get_course_id = listExperts.get(i).getAccount_Course().getCourse_id();
//            
//            if(listExperts.get(i).getAccount_Course().getCourse_id())
//
//        }
        //Thông tin giảng viên được đăng ký nhiều nhất
        ArrayList<Information> listExperts = new ArrayList<>();
        CourseAccountDAO courseAccDAO = new CourseAccountDAO();
        //listExperts =  courseAccDAO.getAllExpertByAccountRes(4);

        if (acc == null) {
            request.getRequestDispatcher("JSP/home.jsp").forward(request, response);
            return;
        }
        PostDAO postdao = new PostDAO();
        ArrayList<Post> listPost = new ArrayList<>();
        listPost = postdao.getAllPostActive();
        request.getSession().setAttribute("listPost", listPost);
        request.setAttribute("listExperts", listExperts);
        request.getSession().setAttribute("roleID", acc.getRoleID());
        request.getRequestDispatcher("JSP/home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
