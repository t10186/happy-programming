/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CourseDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Course;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
@WebServlet(name = "UpdateDetailLessonController", urlPatterns = {"/update-detail-lesson"})
public class UpdateDetailLessonController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateDetailLessonController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateDetailLessonController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //        processRequest(request, response);
        int lesson_id = Integer.parseInt(request.getParameter("lesson_id"));
        int course_id = Integer.parseInt(request.getParameter("course_id"));
        int lesson_unit = Integer.parseInt(request.getParameter("lesson_unit"));
        String lesson_title = request.getParameter("lesson_title");
        String lesson_description = request.getParameter("lesson_description");
        String lesson_content = request.getParameter("lesson_content");
        String lesson_video = request.getParameter("lesson_video");
        String lesson_time_hours = request.getParameter("lesson_time_hours");
        String lesson_time_minute = request.getParameter("lesson_time_minute");
        String lesson_time_second = request.getParameter("lesson_time_second");
        String lesson_time = lesson_time_hours + "H" + lesson_time_minute + "P" + lesson_time_second + "S";
        Date date_create_lesson = (Date.valueOf(request.getParameter("date_create_lesson")));
        Boolean lesson_status = Boolean.parseBoolean(request.getParameter("lesson_status"));
        //update lesson
        LessonDAO lessonDAO = new LessonDAO();
        Lesson lesson = new Lesson(lesson_id, lesson_unit, lesson_title, lesson_description, lesson_content, lesson_video, lesson_time, date_create_lesson, true);
        lessonDAO.updateLesson(lesson);
        response.sendRedirect("show-manager-lesson");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
