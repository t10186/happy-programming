/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dao.QuizDAO;
import entity.QuizHistory;
import entity.QuizQuestion;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.*;

/**
 *
 * @author Phong Linh
 */
public class QuizHandleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizHandleController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizHandleController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private static final Type QUESTION_LIST_TYPE = new TypeToken<ArrayList<QuizQuestion>>() {}.getType();
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        int quizId = 0;
        String reviewQuiz = request.getParameter("reviewQuiz");
        String startQuiz = request.getParameter("startQuiz");
        String submitQuiz = request.getParameter("submitQuiz");
        String submitHandle = request.getParameter("submitHandle");
        QuizDAO dao = new QuizDAO();
        if (startQuiz != null && startQuiz.equals("true")) {
            try {
                quizId = Integer.parseInt(request.getParameter("quizId"));
            } catch (NumberFormatException e) {
                //error
                return;
            }
            //String user = request.getParameter("username");
            session.removeAttribute("quizHistory");
            List<QuizQuestion> questions = dao.getQuestions(quizId);
            session.setAttribute("QuizId", quizId);
            session.setAttribute("questions", new Gson().toJson(questions));
            session.setAttribute("questionIndex", 0);
            session.setAttribute("isReview", false);
            session.setAttribute("startTime", System.currentTimeMillis());
            session.setAttribute("length", dao.getQuizDuration(quizId));
            session.setAttribute("username", request.getParameter("username"));
            dao.Close();
            response.sendRedirect("JSP/quizHandle.jsp");
            return;
        }
        if (reviewQuiz != null && reviewQuiz.equals("true")) {
            try {
                quizId = Integer.parseInt(request.getParameter("quizId"));
            } catch (NumberFormatException e) {
                //error
                return;
            }
            QuizHistory history = dao.getHistory(quizId);
            List<QuizQuestion> questions = history.getQuestionList();
            session.setAttribute("QuizId", quizId);
            session.setAttribute("questions", new Gson().toJson(questions));
            session.setAttribute("questionIndex", 0);
            session.setAttribute("isReview", true);
            session.setAttribute("startTime", 0);
            session.setAttribute("length", 0);
            session.setAttribute("username", request.getParameter("username"));
            dao.Close();
            response.sendRedirect("JSP/quizHandle.jsp");
            return;
        }
        if (submitHandle != null && submitHandle.equals("true")) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                QuizQuestion question = new Gson().fromJson(request.getParameter("question"), QuizQuestion.class);
                int index = Integer.parseInt(request.getParameter("index"));
                if (request.getParameter("taker") == null || request.getParameter("taker").length() <= 0) {
                    throw new Exception("Invalid taker's username");
                }
                if (dao.submitQuestionHandle(request.getParameter("taker"), question.getId(), question.getChoosenAnswer(), question.isMarked())) {
                    dao.Close();
                    out.write("{\"success\": \"true\"}");
                    List<QuizQuestion> list = new Gson().fromJson(session.getAttribute("questions").toString(), QUESTION_LIST_TYPE);
                    list.set(index, question);
                    session.setAttribute("questions", new Gson().toJson(list));
                    session.setAttribute("questionIndex", index);
                    return;
                } else {
                    throw new Exception("Cannot save to database");
                }
            } catch (Exception e) {
                dao.Close();
                out.write("{\"success\": \"false\", \"error\": \"" + e.toString() + "\"}");
                //error
                return;
            }
        }
        if (submitQuiz != null && submitQuiz.equals("true")) {
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            try {
                String taker = request.getParameter("taker");
                if (taker == null || taker.length() <= 0) {
                    throw new Exception("Invalid taker's username");
                }
                quizId = Integer.parseInt(request.getParameter("quizId"));
                long startTime = Long.parseLong(request.getParameter("startTime"));
                if (request.getParameter("question") != null && !request.getParameter("question").isEmpty()) {
                    QuizQuestion question = new Gson().fromJson(request.getParameter("question"), QuizQuestion.class);
                    dao.submitQuestionHandle(taker, question.getId(), question.getChoosenAnswer(), question.isMarked());
                }
                if (dao.finalizeQuiz(taker, quizId, startTime, System.currentTimeMillis())) {
                    dao.RemovePreviousHandle(taker);
                    out.write("{\"success\": \"true\"}");
                } else {
                    out.write("{\"success\": \"false\"}");
                }
                //clean up
                session.removeAttribute("quiz");
                session.removeAttribute("questions");
                session.removeAttribute("questionIndex");
                session.removeAttribute("isReview");
                session.removeAttribute("startTime");
                session.removeAttribute("length");
                session.removeAttribute("username");
                session.removeAttribute("quizHistory");
                session.setAttribute("QuizId", quizId);
                dao.Close();
            } catch (Exception e) {
                dao.Close();
                //e.printStackTrace();
                out.write("{\"success\": \"false\", \"error\": \"" + e.toString() + "\"}");
                //error
            }
        }
//        int upQuestionId = 0, answerId = 0;
//        QuizSubmission submission = (QuizSubmission)session.getAttribute("submission");
//        QuizQuestion lastQuestion = (QuizQuestion)session.getAttribute("curQuestion");
//        quizId = submission.getQuizId();
//        if (request.getParameter("answer") == null) {
//            answerId = -1;
//        } else {
//            answerId = Integer.parseInt(request.getParameter("answer"));
//        }
//        upQuestionId = Integer.parseInt(request.getParameter("upQuestion"));
//        submission.AddAnswer(lastQuestion.getId(), answerId, isValid(request.getParameter("marker")));
//        session.setAttribute("submission", submission);
//        QuizQuestion upQuestion = dao.getQuestion(upQuestionId);
//        session.setAttribute("curQuestion", upQuestion);
//        session.setAttribute("nextQuestion", dao.getNextQuestionId(upQuestion.getId(), quizId));
//        session.setAttribute("prevQuestion", dao.getPrevQuestionId(upQuestion.getId(), quizId));
//        response.sendRedirect("JSP/quizHandle.jsp");
//        dao.Close();
    }
    private boolean isValid(String s) {
        return s != null && !s.isEmpty();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
