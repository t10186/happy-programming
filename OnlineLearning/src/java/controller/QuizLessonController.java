/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.QuizDAO;
import entity.Quiz;
import entity.QuizHistory;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Phong Linh
 */
public class QuizLessonController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get course
        //get lesson
        //get user's info
        int quizId;
        try {
            quizId = Integer.parseInt(request.getParameter("QuizId"));
        } catch (NumberFormatException e) {
            quizId = 1;
            request.getSession().setAttribute("acc", new AccountDAO().getAccountByUserPass("minh", "minh"));
        }
        QuizDAO dao = new QuizDAO();
        Quiz quiz = dao.getQuiz(quizId);
        if (quiz == null) {
            //show error
            //go back to course list
            return;
        }
        QuizHistory quizHistory = dao.getHistory(quizId);
        
        //for testing
//        quiz.setTittle("TEST_QUIZ_TITLE");
//        quiz.setLength(1800);
//        quiz.setMaxPoint(10);
//        quiz.setDescription("TEST_QUIZ DESCRIPTION");
//        List<QuizHistory.Question> tempList = new ArrayList<>();
//        quizHistory = new QuizHistory();
//        tempList.add(quizHistory.new Question(0, 1, false, 1, true));
//        tempList.add(quizHistory.new Question(1, 2, false, 2, true));
//        tempList.add(quizHistory.new Question(2, 3, false, 3, true));
//        tempList.add(quizHistory.new Question(3, 4, false, 4, false));
//        tempList.add(quizHistory.new Question(4, 5, false, -1, false));
//        quizHistory = new QuizHistory(0, 0, "minh", Date.from(Instant.now().minusSeconds(1234)), Date.from(Instant.now()), 6, tempList);
        //-----------
        request.getSession().setAttribute("quiz", quiz);
        request.getSession().setAttribute("quizHistory", quizHistory);
        //request.getSession().setAttribute("QuizId", quizId);
        dao.Close();
        request.getRequestDispatcher("/JSP/quizLesson.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
