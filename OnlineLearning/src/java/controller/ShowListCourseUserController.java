/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accessory.Search;
import dao.CourseAccountDAO;
import dao.CourseDAO;
import dao.InformationDAO;
import dao.LessonDAO;
import entity.Account;
import entity.Account_Course;
import entity.Course;
import entity.Information;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class ShowListCourseUserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowListCourseUserController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowListCourseUserController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //search
        String keySearch = request.getParameter("keySearch");
        String filter = request.getParameter("filter");
        
        String columnSear = "";
        if (keySearch != null) {
            String[] nameColumn = {"[course_title]"};
            columnSear = new Search().getSearch(nameColumn, keySearch);
        }
        Account acc = (Account) request.getSession().getAttribute("acc");
        CourseDAO courseDAO = new CourseDAO();
        String indexPage = request.getParameter("index");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int count = courseDAO.countCourseByID(acc.getId(), columnSear);
        // number courses display in page
        int numberRows = 12;

        ArrayList<Course> listCourse = courseDAO.getAllByID(acc.getId(),index,numberRows,columnSear,filter);
        int endList = count / numberRows;
        if (count % numberRows != 0) {
            endList++;
        }

        LessonDAO lessonDAO = new LessonDAO();
        CourseAccountDAO courseAccountDAO = new CourseAccountDAO();
        ArrayList<Course> listCourse = courseDAO.getAllByID(acc.getId(), index, numberRows, columnSear);

        for (Course course : listCourse) {
            double total_Lesson = courseDAO.total_Lesson(course.getId());
            double total_Unlock = courseAccountDAO.lesson_unlock(course.getId(), acc.getId());
            double persent = Math.round((total_Unlock / total_Lesson) * 100);
            int process_persent = (int) persent;
            course.setPersent(process_persent);
        }

        int total_course = courseDAO.countCourse();
        double total_course_double = (double) total_course;
        double total_myCourse = (double) listCourse.size() ;
        int total_myCourse_int = listCourse.size();
        double persent_course = Math.round((total_myCourse / total_course_double) * 100);
        int process_persent_course = (int) persent_course;

        InformationDAO informationDAO = new InformationDAO();
        Information information = informationDAO.getInformation(acc.getId());
        request.setAttribute("total_course", total_course);
        request.setAttribute("total_myCourse_int", total_myCourse_int);
        request.setAttribute("process_persent_course", process_persent_course);
        request.setAttribute("information", information);
        request.setAttribute("listCourse", listCourse);
        request.setAttribute("endList",endList);
        request.setAttribute("index", index);
        request.setAttribute("keySearch", keySearch);
        request.getRequestDispatcher("JSP/listMyCourse.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
