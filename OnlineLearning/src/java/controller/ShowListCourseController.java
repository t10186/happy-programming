/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accessory.Search;
import dao.CourseDAO;
import dao.SlideDAO;
import entity.Course;
import entity.Slide;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author Quynh_Nhu
 */
public class ShowListCourseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShowListCourseController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShowListCourseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //search
        String keySearch = request.getParameter("keySearch");
        String columnSear = "";
        if (keySearch != null) {
            String[] nameColumn = {"[course_title]"};
            columnSear = new Search().getSearch(nameColumn, keySearch);
        }
        SlideDAO SlideDAO = new SlideDAO();
        ArrayList<Slide> listSlide = SlideDAO.getAll();
        request.setAttribute("listSlide", listSlide);
        CourseDAO courseDAO = new CourseDAO();
        String indexPage = request.getParameter("index");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int count = courseDAO.countCourse();
        // number courses display in page
        int numberRows = 12;
        int endList = count / numberRows;
        if (count % numberRows != 0) {
            endList++;
        }
        ArrayList<Course> listCourse = new ArrayList<>();
        listCourse = courseDAO.getCourse(index, numberRows, columnSear);
        request.setAttribute("listCourses", listCourse);
        request.setAttribute("endList", endList);
        request.setAttribute("index", index);
        request.getRequestDispatcher("JSP/courses.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
