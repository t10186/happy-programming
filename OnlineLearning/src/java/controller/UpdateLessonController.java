/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.LessonDAO;
import entity.Lesson;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Luu Duc Thang
 */
@WebServlet(name = "UpdateLessonController", urlPatterns = {"/update-lesson"})
public class UpdateLessonController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateLessonController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateLessonController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //        processRequest(request, response);
        int Lesson_id = Integer.parseInt(request.getParameter("lesson_id"));

        LessonDAO lessonDAO = new LessonDAO();
        Lesson lesson = lessonDAO.getLessonByID(Lesson_id);
        request.setAttribute("lesson", lesson);
        if(lesson.getLessonTime() != null){
        String lesson_time = lesson.getLessonTime();

        //Lấy ra giờ
        String[] lesson_time_split_hours = lesson_time.split("H");
        int hours = Integer.parseInt(lesson_time_split_hours[0]);
        request.setAttribute("hours", hours);

        //Lấy ra phút
        String lesson_time_minute = lesson_time_split_hours[1];
        String[] lesson_time_split_minute = lesson_time_minute.split("P");
        int minute = Integer.parseInt(lesson_time_split_minute[0]);
        request.setAttribute("minute", minute);

        //Lấy ra giây
        String lesson_time_Second = lesson_time_split_minute[1];
        String[] lesson_time_split_second = lesson_time_Second.split("S");
        int second = Integer.parseInt(lesson_time_split_second[0]);
        request.setAttribute("second", second);
        }
        request.setAttribute("course_id", lesson.getCourseID());
        request.getRequestDispatcher("JSP/updateLesson.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
