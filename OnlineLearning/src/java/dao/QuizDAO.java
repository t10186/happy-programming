/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Quiz;
import entity.QuizAnswer;
import entity.QuizHistory;
import entity.QuizQuestion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phong Linh
 */
public class QuizDAO {

    private final Connection _conn;

    public QuizDAO() {
        _conn = new BaseDAO().getConnection();
    }

    public boolean Close() {
        if (_conn == null) {
            return true;
        }
        try {
            _conn.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    private boolean isValid() {
        try {
            return !_conn.isClosed() && _conn.isValid(3);
        } catch (SQLException e) {
            return false;
        }
    }

    public Quiz getQuiz(int id) {
        if (!isValid()) {
            return null;
        }
        try {
            String sql = "SELECT id, title, content, [length], [max_point], passRate, [subject], [type]\n"
                    + "FROM Quiz\n"
                    + "WHERE id = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Quiz(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("content"),
                        rs.getInt("length"),
                        rs.getFloat("max_point"),
                        getQuestionCount(rs.getInt("id")),
                        rs.getFloat("passRate"),
                        rs.getInt("subject"),
                        rs.getInt("type"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public int getQuizDuration(int quizId) {
        if (!isValid()) {
            return -1;
        }
        try {
            String sql = "SELECT [length] FROM Quiz where id = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("length");
            }
        } catch (SQLException e) {
        }
        return -1;
    }

    public QuizHistory getHistory(int quizId) {
        if (!isValid()) {
            return null;
        }
        try {
            String sql = "SELECT id, taker, point, takeTime, finishTime\n"
                    + "FROM QuizHistory\n"
                    + "WHERE Quiz_id = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int historyId = rs.getInt("id");
                QuizHistory quizHistory = new QuizHistory();
                quizHistory.setId(historyId);
                quizHistory.setQuizId(quizId);
                quizHistory.setTaker(rs.getString("taker"));
                quizHistory.setPoint(rs.getFloat("point"));
                quizHistory.setTakeTime(rs.getTimestamp("takeTime"));
                quizHistory.setFinishTime(rs.getTimestamp("finishTime"));
                List<QuizQuestion> questionList = getQuestions(quizId);

                String sql_detail = "SELECT question_id, marked, answer_id\n"
                        + "FROM QuizHistoryDetail\n"
                        + "WHERE id = ?";
                PreparedStatement ps_detail = _conn.prepareStatement(sql_detail);
                ps_detail.setInt(1, historyId);
                ResultSet rs_detail = ps_detail.executeQuery();
                while (rs_detail.next()) {
                    int questionId = rs_detail.getInt("question_id");
                    int answerId = rs_detail.getInt("answer_id");
                    for (QuizQuestion question : questionList) {
                        if (question.getId() == questionId) {
                            question.setMarked(rs_detail.getBoolean("marked"));
                            if (answerId > 0) {
                                question.setChoosenAnswer(answerId);
                            }
                        }
                    }
                }
                quizHistory.setQuestionList(questionList);
                //checkUnanswered(quizId, quizHistory);
                return quizHistory;
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public int getQuestionCount(int quizId) {
        if (!isValid()) {
            return -1;
        }
        try {
            String sql = "SELECT COUNT(DISTINCT questionId) as [count] FROM Quiz_Question WHERE quizId = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (SQLException e) {
        }
        return -1;
    }

    public float getQuizMaxPoint(int quizId) {
        if (!isValid()) {
            return -1;
        }
        try {
            String sql = "SELECT max_point FROM Quiz WHERE id = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, quizId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getFloat("max_point");
            }
        } catch (SQLException e) {
        }
        return -1;
    }

    public boolean submitQuestionHandle(String taker, int questionId, int answerId, boolean marked) {
        if (!isValid()) {
            return false;
        }
        try {
            if (answerId <= 0) {
                //delete handle
                String sqlDel = "DELETE FROM Quiz_Handle WHERE taker = ? AND Question_id = ?";
                PreparedStatement ps = _conn.prepareStatement(sqlDel);
                ps.setString(1, taker);
                ps.setInt(2, questionId);
                ps.executeUpdate();
                return true;
            }
            String sqlExist = "SELECT * FROM Quiz_Handle WHERE taker = ? AND Question_id = ?";
            PreparedStatement ps1 = _conn.prepareStatement(sqlExist);
            ps1.setString(1, taker);
            ps1.setInt(2, questionId);
            ResultSet result = ps1.executeQuery();
            if (result.next()) {
                //existed
                String sql = "UPDATE Quiz_Handle SET answer_id = ?, marked = ? WHERE taker = ? AND Question_id = ?";
                PreparedStatement ps = _conn.prepareStatement(sql);
                ps.setInt(1, answerId);
                ps.setBoolean(2, marked);
                ps.setString(3, taker);
                ps.setInt(4, questionId);
                if (ps.executeUpdate() > 0) {
                    return true;
                }
            } else {
                String sql = "INSERT INTO Quiz_Handle(taker, Question_id, answer_id, marked) VALUES (?, ?, ?, ?)";
                PreparedStatement ps = _conn.prepareStatement(sql);
                ps.setString(1, taker);
                ps.setInt(2, questionId);
                ps.setInt(3, answerId);
                ps.setBoolean(4, marked);
                if (ps.executeUpdate() > 0) {
                    return true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    public boolean finalizeQuiz(String taker, int quizId, long startTime, long submitTime) {
        if (!isValid()) {
            return false;
        }
        try {
            String sql =    "SELECT qTable.questionId, aTable.answer_id, aTable.marked FROM \n" +
                            "(SELECT questionId FROM Quiz_Question WHERE quizId = ?) as qTable LEFT JOIN\n" +
                            "(SELECT * FROM Quiz_Handle WHERE taker = ? \n" +
                            "	AND submitTime > ? AND submitTime < ?) as aTable\n" +
                            "ON qTable.questionId = aTable.Question_id";
            PreparedStatement ps = _conn.prepareStatement(sql);
            String sqlCorrect = "SELECT correct FROM QuestionAnswer WHERE id = ?";
            PreparedStatement psCorrect = _conn.prepareStatement(sqlCorrect);
            String sqlHistory = "INSERT INTO QuizHistory VALUES (?, ?, ?, ?, ?)";
            PreparedStatement psHistory = _conn.prepareStatement(sqlHistory, Statement.RETURN_GENERATED_KEYS);
            String sqlDetail = "INSERT INTO QuizHistoryDetail VALUES (?, ?, ?, ?)";
            PreparedStatement psDetail = _conn.prepareStatement(sqlDetail);
            
            //delete old record
            PreparedStatement psDel1 = _conn.prepareStatement("SELECT id FROM QuizHistory WHERE taker = ? AND Quiz_id = ?");
            PreparedStatement psDel2 = _conn.prepareStatement("DELETE FROM QuizHistoryDetail WHERE id = ?");
            PreparedStatement psDel3 = _conn.prepareStatement("DELETE FROM QuizHistory WHERE id = ?");
            psDel1.setString(1, taker);
            psDel1.setInt(2, quizId);
            ResultSet rsDel = psDel1.executeQuery();
            while (rsDel.next()) {
                psDel2.setInt(1, rsDel.getInt("id"));
                psDel2.addBatch();
                psDel3.setInt(1, rsDel.getInt("id"));
                psDel3.addBatch();
            }
            psDel2.executeBatch();
            psDel3.executeBatch();
            
            //set history
            Timestamp startTimestamp = new Timestamp(startTime);
            psHistory.setString(1, taker);
            psHistory.setInt(2, quizId);
            psHistory.setFloat(3, 0);
            psHistory.setTimestamp(4, startTimestamp);
            psHistory.setTimestamp(5, new Timestamp(submitTime));
            psHistory.executeUpdate();
            ResultSet historyRs = psHistory.getGeneratedKeys();
            historyRs.next();
            int historyId = historyRs.getInt(1);
            
            int quizDur = getQuizDuration(quizId);
            ps.setInt(1, quizId);
            ps.setString(2, taker);
            ps.setTimestamp(3, startTimestamp);
            ps.setTimestamp(4, new Timestamp(startTime + quizDur*1000));
            ResultSet rs = ps.executeQuery();
            int correct = 0, total = 0;
            while (rs.next()) {
                total++;
                psDetail.setInt(1, historyId);
                psDetail.setInt(2, rs.getInt("questionId"));
                psDetail.setBoolean(3, rs.getBoolean("marked"));
                int answerId = rs.getInt("answer_id");
                if (rs.wasNull()) {
                    psDetail.setInt(4, 0);
                    psDetail.addBatch();
                    continue;
                }
                psDetail.setInt(4, answerId);
                psDetail.addBatch();
                psCorrect.setInt(1, answerId);
                ResultSet correctRs = psCorrect.executeQuery();
                if (correctRs.next() && correctRs.getBoolean("correct")) {
                    correct++;
                }
            }
            psDetail.executeBatch();
            //update score
            float point = correct * getQuizMaxPoint(quizId) / total;
            PreparedStatement psUpd = _conn.prepareStatement("UPDATE QuizHistory SET point = ? WHERE id = ?");
            psUpd.setFloat(1, point);
            psUpd.setInt(2, historyId);
            psUpd.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean RemovePreviousHandle(String taker) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement("delete from Quiz_Handle where taker = ?");
            ps.setString(1, taker);
            if(ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
        }
        return false;
    }

    private int getCurrentIDENTITY(String table) {
        if (!isValid()) {
            return -1;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement("SELECT IDENT_CURRENT(?) [id]");
            ps.setString(1, table);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("id");
            }
        } catch (SQLException e) {
        }
        return -1;
    }

    public Quiz getQuiz_Editor(int id) {
        if (!isValid()) {
            return null;
        }
        try {
            String sql = "SELECT id, title, content, [length], [max_point]\n"
                    + "FROM Quiz\n"
                    + "WHERE lesson_id = ?";
            PreparedStatement ps = _conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Quiz(
                        rs.getInt("id"),
                        rs.getString("title"),
                        rs.getString("content"),
                        rs.getInt("length"),
                        rs.getFloat("max_point"),
                        0,
                        rs.getFloat("passRate"),
                        rs.getInt("subject"),
                        rs.getInt("type"));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public List<QuizQuestion> getQuestions(int quizId) {
        if (!isValid()) {
            return null;
        }
        try {
            List<QuizQuestion> list = new ArrayList<>();
            String sql_id = "SELECT [questionId] FROM Quiz_Question WHERE [quizId] = ? AND [status] = 1";
            String sql = "SELECT [content]\n"
                    + "    ,[explanation]\n"
                    + "    ,[subject]\n"
                    + "FROM Question\n"
                    + "WHERE [id] = ?";
            String sql_ans = "SELECT	[id],\n"
                    + "		[correct],\n"
                    + "		[content_answer]\n"
                    + "FROM [QuestionAnswer]\n"
                    + "WHERE [question_id] = ?";
            
            PreparedStatement psId = _conn.prepareStatement(sql_id);
            psId.setInt(1, quizId);
            PreparedStatement ps = _conn.prepareStatement(sql);
            PreparedStatement ps_ans = _conn.prepareStatement(sql_ans);
            ResultSet rs1 = psId.executeQuery();
            while (rs1.next()) {
                int qId = rs1.getInt("questionId");
                ps.setInt(1, qId);
                ResultSet rs2 = ps.executeQuery();
                if (rs2.next()) {
                    QuizQuestion question = new QuizQuestion();
                    question.setId(qId);
                    question.setQuestion(rs2.getString("content"));
                    question.setExplaination(rs2.getString("explanation"));
                    question.setSubject(rs2.getInt("subject"));
                    question.setChoosenAnswer(-1);
                    
                    List<QuizAnswer> answerList = new ArrayList<>();
                    ps_ans.setInt(1, qId);
                    ResultSet rs_ans = ps_ans.executeQuery();
                    while (rs_ans.next()) {
                        QuizAnswer ans = new QuizAnswer(rs_ans.getInt("id"), rs_ans.getString("content_answer"), rs_ans.getBoolean("correct"));
                        answerList.add(ans);
                    }
                    question.setAnswerList(answerList);
                    list.add(question);
                }
            }
            return list;
        } catch (SQLException e) {
        }
        return null;
    }

    public boolean updateQuizInfo(int quizID, String title, String desc, int length, int maxPoint) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement(
                    "UPDATE [Quiz]\n"
                    + "SET [title] = ?, [content] = ?, [length] = ?, [max_point] = ?\n"
                    + "WHERE [id] = ?");
            ps.setString(1, title);
            ps.setString(2, desc);
            ps.setInt(3, length);
            ps.setInt(4, maxPoint);
            ps.setInt(5, quizID);

            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
        }
        return false;
    }

    public QuizQuestion addQuestion(int quizId) {
        if (!isValid()) {
            return null;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement(
                    "INSERT INTO [Quiz_Question] (quiz_id, number_question, question, explaination)\n"
                    + "VALUES (?, ?, ?, ?)");
            ps.setInt(1, quizId);
            ps.setInt(2, 0);
            ps.setString(3, "New Question");
            ps.setString(4, "");

            if (ps.executeUpdate() > 0) {
                int id = getCurrentIDENTITY("Quiz_Question");
                List<QuizAnswer> answerList = new ArrayList<>();
                QuizAnswer correct = addAnswer(id, true, "A");
                answerList.add(correct);
                answerList.add(addAnswer(id, false, "B"));
                answerList.add(addAnswer(id, false, "C"));
                return new QuizQuestion(id, 0, "New Question", "", false, correct.getId(), answerList);
            }
            return null;
        } catch (SQLException e) {
        }
        return null;
    }

    public QuizAnswer addAnswer(int questionID, boolean correct, String content) {
        if (!isValid()) {
            return null;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement(
                    "INSERT INTO [Answer_Question] (question_id, correct, content_answer) VALUES (?, ?, ?);");
            ps.setInt(1, questionID);
            ps.setBoolean(2, correct);
            ps.setString(3, content);

            if (ps.executeUpdate() > 0) {
                return new QuizAnswer(getCurrentIDENTITY("Answer_Question"), content);
            }
            return null;
        } catch (SQLException e) {
        }
        return null;
    }

    public boolean updateQuestion(int questionID, String question, String explanation, List<QuizAnswer> answerList, int correctID) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement(
                    "UPDATE Quiz_Question\n"
                    + "SET question = ?, explaination = ?\n"
                    + "WHERE id = ?");
            ps.setString(1, question);
            ps.setString(2, explanation);
            ps.setInt(3, questionID);

            if (ps.executeUpdate() > 0) {
                //update answers
                PreparedStatement ps_a = _conn.prepareStatement(
                        "UPDATE Answer_Question\n"
                        + "SET content_answer = ?, correct = ?\n"
                        + "WHERE id = ?");
                for (QuizAnswer ans : answerList) {
                    ps_a.setString(1, ans.getAnswer());
                    ps_a.setBoolean(2, ans.getId() == correctID);
                    ps_a.setInt(3, ans.getId());
                    ps_a.addBatch();
                }
                ps_a.executeBatch();
                return true;
            }
            return false;
        } catch (SQLException e) {
        }
        return false;
    }

    public boolean deleteAnswer(int answerID) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps = _conn.prepareStatement(
                    "DELETE FROM Answer_Question\n"
                    + "WHERE id = ?");
            ps.setInt(1, answerID);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
        }
        return false;
    }

    public boolean deleteQuestion(int questionID) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps_a = _conn.prepareStatement(
                    "DELETE FROM [Answer_Question]\n"
                    + "WHERE question_id = ?");
            ps_a.setInt(1, questionID);
            ps_a.executeUpdate();

            PreparedStatement ps_q = _conn.prepareStatement(
                    "DELETE FROM [Quiz_Question]\n"
                    + "WHERE id = ?");
            ps_q.setInt(1, questionID);
            ps_q.executeUpdate();

            PreparedStatement ps_h = _conn.prepareStatement(
                    "DELETE FROM [history_quiz_detail]\n"
                    + "WHERE question_id = ?");
            ps_h.setInt(1, questionID);
            ps_h.executeUpdate();
            return true;
        } catch (SQLException e) {
        }
        return false;
    }

    public boolean deleteQuiz(int quizID) {
        if (!isValid()) {
            return false;
        }
        try {
            PreparedStatement ps_q = _conn.prepareStatement(
                    "SELECT id FROM Quiz_Question WHERE quiz_id = ?");
            ps_q.setInt(1, quizID);
            ResultSet rs_q = ps_q.executeQuery();
            while (rs_q.next()) {
                deleteQuestion(rs_q.getInt("id"));
            }
            PreparedStatement ps_h = _conn.prepareStatement(
                    "DELETE FROM history_quiz\n"
                    + "WHERE Quiz_id = ?");
            ps_h.setInt(1, quizID);
            ps_h.executeUpdate();

            PreparedStatement ps_z = _conn.prepareStatement(
                    "DELETE FROM Quiz\n"
                    + "WHERE id = ?");
            ps_z.setInt(1, quizID);
            ps_z.executeUpdate();
            return true;
        } catch (SQLException e) {
        }
        return false;
    }
}
