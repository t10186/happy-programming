/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Blogs;
import entity.Post;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class PostDAO {

    public ArrayList<Post> getAllBlogs() {
        ArrayList<Post> listPost = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT  [id]\n"
                    + "      ,[account_id]\n"
                    + "      ,[title]\n"
                    + "      ,[detail]\n"
                    + "      ,[category]\n"
                    + "      ,[thumbnail]\n"
                    + "      ,[date_create_post]\n"
                    + "      ,[post_status]\n"
                    + "  FROM [OnlineLearning].[dbo].[Post]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Post post = new Post();
                post.setPost_Id(rs.getInt("id"));
                post.setAuthor_id(rs.getString("account_id"));
                post.setDetail(rs.getString("detail"));
                post.setTimeCreate(rs.getDate("date_create_post"));
                post.setTitle(rs.getString("title"));
                post.setThumbnail(rs.getString("thumbnail"));
                post.setStatus(rs.getInt("post_status"));
                listPost.add(post);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listPost;

    }

    public int insert(Post post) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Post]\n"
                    + "           ([account_id]\n"
                    + "           ,[title]\n"
                    + "           ,[detail]\n"
                    + "           ,[category]\n"
                    + "           ,[thumbnail]\n"
                    + "           ,[date_create_post]\n"
                    + "           ,[post_status])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?,?,getdate(),?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, Integer.valueOf(post.getAuthor_id()));
            ps.setString(2, post.getTitle());
            ps.setString(3, post.getDetail());
            ps.setString(4, post.getCategory());
            ps.setString(5, post.getThumbnail());
            ps.setInt(6, post.getStatus());

            ps.execute();
            connection.close();
            return 1;
        } catch (SQLException e) {
        }
        return 0;
    }

    public ArrayList<Post> getBlogsWithPagging(int page, int PAGE_SIZE) {
        ArrayList<Post> list = new ArrayList<>();
        try {
            String sql = "select *  from Post order by date_create_post desc\n"
                    + "offset (?-1)*? row fetch next ? rows only";

            Connection connection = new BaseDAO().getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, page);
            ps.setInt(2, PAGE_SIZE);
            ps.setInt(3, PAGE_SIZE);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Post post = new Post();
                post.setPost_Id(rs.getInt("id"));
                post.setAuthor_id(rs.getString("account_id"));
                post.setDetail(rs.getString("detail"));
                post.setTimeCreate(rs.getDate("date_create_post"));
                post.setTitle(rs.getString("title"));
                post.setThumbnail(rs.getString("thumbnail"));
                post.setStatus(rs.getInt("post_status"));
                post.setCategory(rs.getString("category"));
                list.add(post);
            }
            connection.close();
        } catch (Exception ex) {
        }
        return list;
    }

    public int getTotalBlogs() {
        try {
            String sql = "select count(id) from post ";

            Connection connection = new BaseDAO().getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public Post getPostById(int id) {

        Post post = new Post();

        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select * from Post where id =?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                post.setPost_Id(rs.getInt("id"));
                post.setAuthor_id(rs.getString("account_id"));
                post.setDetail(rs.getString("detail"));
                post.setTimeCreate(rs.getDate("date_create_post"));
                post.setTitle(rs.getString("title"));
                post.setThumbnail(rs.getString("thumbnail"));
                post.setStatus(rs.getInt("post_status"));
                post.setCategory(rs.getString("category"));
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return post;

    }

    public void changeStatus(int idpost, int statusUpdate) {

        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "update Post set post_status=? where id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusUpdate);
            ps.setInt(2, idpost);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
        }

    }

    public void updatePost(Post post) {
        try {
            Connection connection = new BaseDAO().getConnection();
            String sql = "UPDATE [dbo].[Post]\n"
                    + "   SET [account_id] = ?\n"
                    + "      ,[title] = ?\n"
                    + "      ,[detail] = ?\n"
                    + "      ,[category] = ?\n"
                    + "      ,[thumbnail] = ?\n"
                    + "      ,[date_create_post] = getdate()\n"
                    + "      ,[post_status] = 1\n"
                    + " WHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, Integer.valueOf(post.getAuthor_id()));
            ps.setString(2, post.getTitle());
            ps.setString(3, post.getDetail());
            ps.setString(4, post.getCategory());
            ps.setString(5, post.getThumbnail());
            ps.setInt(6, post.getPost_Id());
            ps.executeUpdate();
            connection.close();

        } catch (SQLException e) {
        }

    }



    public ArrayList<Post> getAllPostActive() {
        ArrayList<Post> list = new ArrayList<>();
        try {
            String sql = "SELECT TOP 3 * from Post where post_status =1 order by date_create_post desc ";

            Connection connection = new BaseDAO().getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Post post = new Post();
                post.setPost_Id(rs.getInt("id"));
                post.setAuthor_id(rs.getString("account_id"));
                post.setDetail(rs.getString("detail"));
                post.setTimeCreate(rs.getDate("date_create_post"));
                post.setTitle(rs.getString("title"));
                post.setThumbnail(rs.getString("thumbnail"));
                post.setStatus(rs.getInt("post_status"));
                post.setCategory(rs.getString("category"));
                list.add(post);
            }
            connection.close();
        } catch (Exception ex) {
        }
        return list;
    }
    public static void main(String[] args) {
        PostDAO a = new PostDAO();
        ArrayList<Post> list = new ArrayList<>();
        list = a.getAllPostActive();
        System.out.println(list.size());
    }
}


