/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Account;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class AccountDAO {

    // get account by username and password
    public Account getAccountByUserPass(String username, String password) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select[account_id],[role_id],[username],[password],[email],[date_register]"
                    + "from [dbo].[Account] where username=? and password=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("account_id"));
                acc.setRoleID(rs.getInt("role_id"));
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                acc.setDateRegister(rs.getDate("date_register"));
                return acc;
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return null;
    }

    public boolean createAccount(Account acc, int phone) {
        InformationDAO inforDAO = new InformationDAO();
        Connection connection = new BaseDAO().getConnection();
        try {
            // create account has role_id default = 1 (customer)
            String sql = "INSERT INTO [dbo].[Account]\n"
                    + "           ([role_id],[username],[password],[email])\n"
                    + "     VALUES\n"
                    + "           (1,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, acc.getUsername());
            ps.setString(2, acc.getPassword());
            ps.setString(3, acc.getEmail());
            ps.execute();
            int accID = getIDByUserName(acc.getUsername(), connection);
            inforDAO.createInformation(accID, phone, connection);
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private int getIDByUserName(String username, Connection connection) {
        try {
            String sql = "select [account_id] from [dbo].[Account] where [username]=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("account_id");
            }
        } catch (SQLException ex) {
        }
        return 0;
    }

    public String getUsername(String email) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select [username] from Account where [email]=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("[username]");
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return null;
    }

    public void changePassword(String username, String password) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + "   SET [password] = ?\n"
                    + " WHERE [username] = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, password);
            ps.setString(2, username);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
        }
    }

    public void changeRole(int role, String username) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + "   SET [role_id] = ? \n"
                    + " where username=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, role);
            ps.setString(2, username);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
        }
    }

    public Account getAccountById(int id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [account_id],[role_id],[username],[password],"
                    + "[email],[date_register],[account_status]\n"
                    + "  FROM [dbo].[Account]  where [account_id]=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account acc = new Account();
                acc.setId(rs.getInt("account_id"));
                acc.setRoleID(rs.getInt("role_id"));
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                acc.setDateRegister(rs.getDate("date_register"));
                return acc;
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return null;
    }

    public boolean UpdateRole(int accID, int roleID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "UPDATE [dbo].[Account]\n"
                    + "   SET [role_id] = ?\n"
                    + " WHERE account_id = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, roleID);
            ps.setInt(2, accID);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public ArrayList<String> listEmailOfExpert() {
        Connection connection = new BaseDAO().getConnection();
        ArrayList<String> listEmail = new ArrayList<>();
        try {
            String sql = "SELECT [email]\n"
                    + "  FROM [dbo].[Account]\n"
                    + "  where role_id = 3";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                listEmail.add(rs.getString("email"));
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return listEmail;
    }

    public Date getLastDateRegis() {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select top 1 date_register from Account order by date_register";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getDate("date_register");
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return null;
    }

    private int getDataAccRegis(Date start, Date end, Connection connection) {
        try {
            String sql = "select count(account_id) as total  from Account where date_register between ? and ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setDate(1, new java.sql.Date(start.getTime()));
            ps.setDate(2, new java.sql.Date(end.getTime()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();
        } catch (SQLException ex) {
            return -1;
        }
        return 0;
    }

    public ArrayList<Integer> getDataAccRegis(int diffMonth, Date dateLast) {
        Connection connection = new BaseDAO().getConnection();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateLast);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        ArrayList<Integer> listData = new ArrayList<>();
        try {
            for (int i = 0; i < 12; i++) {
                String date_start = year + "-" + (month) + "-01";
                month += diffMonth;
                if(month > 12){
                    month %= 12;
                    year++;
                }
                String date_end = year + "-" + (month) + "-01";
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date dateStart = formatter.parse(date_start);
                Date dateEnd = formatter.parse(date_end);
                listData.add(getDataAccRegis(dateStart, dateEnd, connection));
            }
            connection.close();
        } catch (SQLException | ParseException e) {
            return null;
        }
        return listData;
    }
}
