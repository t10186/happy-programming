/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.ImgOfSlide;
import entity.Slide;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author baobao
 */
public class SlideDAO {

    public static ArrayList<Slide> getAll() {
        Connection connection = new BaseDAO().getConnection();
        ArrayList<Slide> listSlide = new ArrayList<>();
        try {
            String sql = "select * from Slide";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Slide slide = new Slide();
                slide.setId(rs.getInt("slide_id"));
                slide.setTimeCreate(rs.getDate("date_create_slide"));
                slide.setName(rs.getString("slide_title"));
                slide.setDescription(rs.getString("slide_description"));
                slide.setStatus(rs.getInt("slide_status"));
                listSlide.add(slide);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listSlide;
    }

    public void changeStatusImg(int idImg, int slideid, int statusUpdate) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "update ImgOfSlide SET img_status = ?\n"
                    + "                   where slide_id = ? and id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusUpdate);
            ps.setInt(2, slideid);
            ps.setInt(3, idImg);

            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
        }

    }

    public void updateByID(int id, int status) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "update Slide set slide_status=? where slide_id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
        }
    }

    public int getLastSlide() {
        int index = 0;
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select top 1 slide_id from slide order by slide_id desc ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            index = rs.getInt("slide_id");
            connection.close();
        } catch (SQLException e) {
        }
        return index;
    }

    public Slide getByID(int id) {
        Connection connection = new BaseDAO().getConnection();
        Slide slide = new Slide();
        try {
            String sql = "select * from Slide where slide_id =?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                slide.setId(rs.getInt("slide_id"));

                slide.setTimeCreate(rs.getDate("date_create_slide"));
                slide.setName(rs.getString("slide_title"));
                slide.setDescription(rs.getString("slide_description"));
                slide.setStatus(rs.getInt("slide_status"));

            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return slide;
    }

    public ArrayList<ImgOfSlide> getAllImgForSlide(int id) {
        Connection connection = new BaseDAO().getConnection();
        ArrayList<ImgOfSlide> listImg = new ArrayList<>();
        try {
            String sql = "select a.id, a.img, a.img_status, a.slide_id from ImgOfSlide as a inner join slide as b on a.slide_id = b.slide_id\n"
                    + "where b.slide_id = ? and a.img_status =1 ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ImgOfSlide img = new ImgOfSlide();
                img.setId(rs.getInt("id"));
                img.setSlide_id(rs.getInt("slide_id"));
                img.setImg(rs.getString("img"));
                img.setStatus(rs.getInt("img_status"));
                listImg.add(img);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listImg;
    }

    public ArrayList<ImgOfSlide> getAllImg(int id) {
        Connection connection = new BaseDAO().getConnection();
        ArrayList<ImgOfSlide> listImg = new ArrayList<>();
        try {
            String sql = "select a.id, a.img, a.img_status, a.slide_id from ImgOfSlide as a inner join slide as b on a.slide_id = b.slide_id\n"
                    + "where b.slide_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ImgOfSlide img = new ImgOfSlide();
                img.setId(rs.getInt("id"));
                img.setSlide_id(rs.getInt("slide_id"));
                img.setImg(rs.getString("img"));
                img.setStatus(rs.getInt("img_status"));
                listImg.add(img);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listImg;
    }

    public static void main(String[] args) {
        SlideDAO a = new SlideDAO();
        ArrayList<Slide> listSlide = new ArrayList<>();
        listSlide = a.getAll();
        ArrayList<ImgOfSlide> listaSlide = new ArrayList<>();
        listaSlide = a.getAllImg(1);
        System.out.println(listaSlide.size());
        for (ImgOfSlide imgOfSlide : listaSlide) {
            System.out.println(imgOfSlide.getImg());
        }

    }

    public void changeDetailSlide(String newName, int slideid, String newDescription) {

        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "update Slide SET slide_title = ?, slide_description = ?\n"
                    + "                   where slide_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, newName);
            ps.setString(2, newDescription);
            ps.setInt(3, slideid);
            ps.executeUpdate();

            connection.close();
        } catch (Exception e) {
        }

    }

    public void AddNewImg(String img, int slideid) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[ImgOfSlide]\n"
                    + "           ([slide_id]\n"
                    + "           ,[img]\n"
                    + "           ,[img_status])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,1)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(2, img);
            ps.setInt(1, slideid);
            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
        }

    }

    public void createSlide(Slide slide) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Slide]\n"
                    + "           ([date_create_slide]\n"
                    + "           ,[slide_title]\n"
                    + "           ,[slide_description])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           )";
            PreparedStatement ps = connection.prepareStatement(sql);
            // ps.setString(1, slide.getImage());
            ps.setDate(1, (Date) slide.getTimeCreate());
            ps.setString(2, slide.getName());
            ps.setString(3, slide.getDescription());
            ps.executeUpdate();
            int idSlide = getLastSlide();
            AddNewImg(slide.getImg(), idSlide);
            connection.close();
        } catch (SQLException e) {
        }
    }
}
