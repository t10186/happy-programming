/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Account;
import entity.Account_Lesson;
import entity.Course;
import entity.Lesson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class LessonDAO {
    

    public ArrayList<Account_Lesson> getAllByIDUnlock(int CourseID, int account_id) {
        ArrayList<Account_Lesson> listLesson = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select lesson_id,c.course_id,lesson_unit,lesson_title,lesson_description,\n"
                    + "lesson_content,lesson_video,date_create_lesson,\n"
                    + "c.lesson_unlock,lesson_unit,CONVERT(bit,case when lesson_unlock >= lesson_unit then 1 else 0 end) as \n"
                    + "check_unlock from lesson as a left join Course as b on a.course_id = b.course_id left join Account_Course as c on b.course_id = c.course_id where\n"
                    + " c.account_id = ? and a.course_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account_id);
            ps.setInt(2, CourseID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Account_Lesson account_Lesson = new Account_Lesson();
                Lesson lesson = new Lesson();
                Account account = new Account();

                lesson.setId(rs.getInt("lesson_id"));
                lesson.setCourseID(rs.getInt("course_id"));
                lesson.setUnit(rs.getInt("lesson_unit"));
                lesson.setTitle(rs.getString("lesson_title"));
                lesson.setDetail(rs.getString("lesson_description"));
                lesson.setContent(rs.getString("lesson_content"));
                lesson.setLinkVideo(rs.getString("lesson_video"));
                lesson.setDateCreate(rs.getDate("date_create_lesson"));

                account_Lesson.setLesson(lesson);
                account_Lesson.setAccount(account);
                account_Lesson.setLesson_unlock(rs.getBoolean("check_unlock"));
                listLesson.add(account_Lesson);

            }
            connection.close();
        } catch (SQLException e) {
        }
        return listLesson;
    }

//    public static void main(String[] args) {
//        LessonDAO lessonDAO = new LessonDAO();
//        ArrayList<Account_Lesson> listLesson = new ArrayList<>();
//        listLesson.
//        System.out.println(new LessonDAO().getAllByIDUnlock(1, 4));
//    }

    public boolean addLessonAccount(int accountID, int lessonID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Account_Lesson]\n"
                    + "           ([account_id],[lesson_id])\n"
                    + "     VALUES\n"
                    + "           (?,?) ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, lessonID);
            ps.execute();
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public ArrayList<Lesson> getAllByID(int CourseID) {
        ArrayList<Lesson> listLesson = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = " SELECT [lesson_id],[course_id],[lesson_unit],[lesson_title],[lesson_description],\n"
                    + "                    [lesson_content],[lesson_video],[date_create_lesson],[lesson_status]\n"
                    + "                    FROM [dbo].[lesson] where\n"
                    + "                    [course_id] = ? order by [lesson_unit]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, CourseID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("lesson_id"));
                lesson.setCourseID(rs.getInt("course_id"));
                lesson.setUnit(rs.getInt("lesson_unit"));
                lesson.setTitle(rs.getString("lesson_title"));
                lesson.setDetail(rs.getString("lesson_description"));
                lesson.setContent(rs.getString("lesson_content"));
                lesson.setLinkVideo(rs.getString("lesson_video"));
                lesson.setDateCreate(rs.getDate("date_create_lesson"));
                listLesson.add(lesson);
            }
            connection.close();
        } catch (SQLException e) {
        }
        return listLesson;
    }

    public Boolean checkUnLockLesson(int account_id, int lesson_id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select lesson_unlock from Account_Lesson where account_id = ? and lesson_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, account_id);
            ps.setInt(2, lesson_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("lesson_unlock");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return null;
    }

    public String getVideoByID(int lessonID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [link_video] FROM [dbo].[lesson] where "
                    + "[id] = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lessonID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("link_video");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return "";
    }

    public Lesson getFirstLesson(int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT top 1 [id],[course_id],[unit],[title],"
                    + "[detail],[content],[link_video] FROM [dbo].[lesson] where "
                    + "[course_id] = ? order by [unit]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("id"));
                lesson.setCourseID(rs.getInt("course_id"));
                lesson.setUnit(rs.getInt("unit"));
                lesson.setTitle(rs.getString("title"));
                lesson.setDetail(rs.getString("detail"));
                lesson.setContent(rs.getString("content"));
                lesson.setLinkVideo(rs.getString("link_video"));
                return lesson;
            }
            connection.close();
        } catch (SQLException e) {
        }
        return null;
    }

    public Lesson getLessonByID(int lessonID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [lesson_id],[course_id],[lesson_unit],[lesson_title],[lesson_description],\n"
                    + "                    [lesson_content],[lesson_time],[lesson_video],[date_create_lesson],[lesson_status]\n"
                    + "		     FROM [dbo].[lesson] where\n"
                    + "                    [lesson_id] = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lessonID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("lesson_id"));
                lesson.setCourseID(rs.getInt("course_id"));
                lesson.setUnit(rs.getInt("lesson_unit"));
                lesson.setTitle(rs.getString("lesson_title"));
                lesson.setDetail(rs.getString("lesson_description"));
                lesson.setContent(rs.getString("lesson_content"));
                lesson.setLessonTime(rs.getString("lesson_time"));
                lesson.setLinkVideo(rs.getString("lesson_video"));
                lesson.setDateCreate(rs.getDate("date_create_lesson"));
                return lesson;
            }
            connection.close();
        } catch (SQLException e) {
        }
        return null;
    }

    public int edit(Lesson lesson) {
        Connection connection = new BaseDAO().getConnection();

        try {
            String sql = "UPDATE [dbo].[lesson]\n"
                    + "   SET [unit] = ?\n"
                    + "      ,[title] = ?\n"
                    + "      ,[detail] = ?\n"
                    + "      ,[content] = ?\n"
                    + "      ,[link_video] = ?\n"
                    + " WHERE [id] = ? ";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lesson.getUnit());
            ps.setString(2, lesson.getTitle());
            ps.setString(3, lesson.getDetail());
            ps.setString(4, lesson.getContent());
            ps.setString(5, lesson.getLinkVideo());
            ps.setInt(6, lesson.getId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public void deteleLesson(Lesson lesson) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "delete from lesson where id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lesson.getId());
            ps.executeUpdate();
            connection.close();
        } catch (Exception e) {
        }
    }

    public void addLesson(Lesson lesson) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[lesson]\n"
                    + "           ([course_id]\n"
                    + "           ,[unit]\n"
                    + "           ,[title]\n"
                    + "           ,[detail]\n"
                    + "           ,[content]\n"
                    + "           ,[link_video])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lesson.getCourseID());
            ps.setInt(2, lesson.getUnit());
            ps.setString(3, lesson.getTitle());
            ps.setString(4, lesson.getDetail());
            ps.setString(5, lesson.getContent());
            ps.setString(6, lesson.getLinkVideo());
            ps.executeQuery();
//            Course newCourse = findCourse(course);
            connection.close();
        } catch (SQLException e) {
        }
    }

    public ArrayList<Lesson> getAllLessonHidden() {
        ArrayList<Lesson> listLesson = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [lesson_id]\n"
                    + "      ,[course_id]\n"
                    + "      ,[lesson_unit]\n"
                    + "      ,[lesson_title]\n"
                    + "      ,[lesson_description]\n"
                    + "      ,[lesson_content]\n"
                    + "      ,[lesson_video]\n"
                    + "      ,[lesson_time]\n"
                    + "      ,[date_create_lesson]\n"
                    + "      ,[lesson_status]\n"
                    + "  FROM [dbo].[lesson] where [lesson_status] = 'false'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("lesson_id"));
                lesson.setCourseID(rs.getInt("course_id"));
                lesson.setUnit(rs.getInt("lesson_unit"));
                lesson.setTitle(rs.getString("lesson_title"));
                lesson.setDetail(rs.getString("lesson_description"));
                lesson.setContent(rs.getString("lesson_content"));
                lesson.setLinkVideo(rs.getString("lesson_video"));
                lesson.setLessonTime(rs.getString("lesson_time"));
                lesson.setDateCreate(rs.getDate("date_create_lesson"));
                lesson.setStatus(rs.getBoolean("lesson_status"));
                listLesson.add(lesson);
            }
            connection.close();
        } catch (SQLException e) {
        }
        return listLesson;
    }

    public int updateLesson(Lesson lesson) {
        Connection connection = new BaseDAO().getConnection();

        try {
            String sql = "UPDATE [dbo].[lesson]\n"
                    + "   SET \n"
                    + "      [lesson_unit] = ?\n"
                    + "      ,[lesson_title] = ?\n"
                    + "      ,[lesson_description] =  ?\n"
                    + "      ,[lesson_content] = ?\n"
                    + "      ,[lesson_video] = ?\n"
                    + "      ,[lesson_time] = ?\n"
                    + "      ,[date_create_lesson] = ?\n"
                    + "      ,[lesson_status] = ?\n"
                    + " WHERE lesson_id = ?";

            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, lesson.getUnit());
            ps.setString(2, lesson.getTitle());
            ps.setString(3, lesson.getDetail());
            ps.setString(4, lesson.getContent());
            ps.setString(5, lesson.getLinkVideo());
            ps.setString(6, lesson.getLessonTime());
            ps.setDate(7, lesson.getDateCreate());
            ps.setBoolean(8, lesson.isStatus());
            ps.setInt(9, lesson.getId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public ArrayList<Lesson> getLessonAllByIDCourse(int course_id) {
        ArrayList<Lesson> listLesson = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select b.lesson_id,b.lesson_unit,b.lesson_title,b.lesson_time,b.date_create_lesson,b.lesson_status from\n"
                    + "Course as a left join lesson as b on a.course_id=b.course_id where a.course_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, course_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Lesson lesson = new Lesson();
                lesson.setId(rs.getInt("lesson_id"));
                lesson.setUnit(rs.getInt("lesson_unit"));
                lesson.setTitle(rs.getString("lesson_title"));
                lesson.setLessonTime(rs.getString("lesson_time"));
                lesson.setDateCreate(rs.getDate("date_create_lesson"));
                lesson.setStatus(rs.getBoolean("lesson_status"));
                listLesson.add(lesson);
            }
            connection.close();
        } catch (SQLException e) {
        }
        return listLesson;
    }

}
