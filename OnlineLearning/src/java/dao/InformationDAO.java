/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Information;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class InformationDAO {

    public ArrayList<Information> getAll() {
        ArrayList<Information> listInformations = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[country_id]\n"
                    + "      ,[displayname]\n"
                    + "      ,[full_name]\n"
                    + "      ,[address]\n"
                    + "      ,[phone]\n"
                    + "      ,[gender]\n"
                    + "      ,[DOB]\n"
                    + "      ,[avatar]\n"
                    + "  FROM [dbo].[information]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Information information = new Information();
                information.setId(rs.getInt("id"));
                information.setCountryID(rs.getInt("country_id"));
                information.setDisplayName(rs.getString("displayname"));
                information.setFullName(rs.getString("full_name"));
                information.setAddress(rs.getString("address"));
                information.setPhone(rs.getInt("phone"));
                information.setGender(rs.getString("gender"));
                information.setDOB(rs.getDate("DOB"));
                information.setAvatar(rs.getString("avatar"));
                listInformations.add(information);
            }
        } catch (Exception e) {
            return null;
        }
        return listInformations;
    }

    protected boolean createInformation(int id, int phone, Connection connection) {
        try {
            String sql = "INSERT INTO [dbo].[Information]\n"
                    + "     ([infor_id],[phone])\n"
                    + "     VALUES(?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setInt(2, phone);
            ps.execute();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }

    public Information getInformation(int id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [infor_id],[country_id],[display_name],[full_name],\n"
                    + "	   [address],[phone],[gender],[DOB],[infor_image]\n"
                    + "  FROM [dbo].[Information] where [infor_id] = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Information infor = new Information();
                infor.setId(id);
                infor.setCountryID(rs.getInt("country_id"));
                infor.setDisplayName(rs.getString("display_name"));
                infor.setFullName(rs.getString("full_name"));
                infor.setAddress(rs.getString("address"));
                infor.setPhone(rs.getInt("phone"));
                infor.setGender(rs.getString("gender"));
                infor.setDOB(rs.getDate("DOB"));
                infor.setAvatar(rs.getString("infor_image"));
                return infor;
            }
            connection.close();

        } catch (SQLException e) {
        }
        return null;
    }
    
    public void updateInformation(Information information) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "UPDATE [dbo].[Information]\n"
                    + "   SET [country_id] = ?\n"
                    + "      ,[full_name] = ?\n"
                    + "      ,[address] = ?\n"
                    + "      ,[phone] = ?\n"
                    + "      ,[gender] = ?\n"
                    + "      ,[DOB] = ?\n"
                    + "      ,[infor_image] = ?\n"
                    + "  WHERE [infor_id] = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, information.getCountryID());
            ps.setString(2, information.getFullName());
            ps.setString(3, information.getAddress());
            ps.setInt(4, information.getPhone());
            ps.setString(5, information.getGender());
            ps.setDate(6, information.getDOB());
            ps.setString(7, information.getAvatar());
            ps.setInt(8, information.getId());
            ps.execute();
            connection.close();
        } catch (SQLException e) {
        }
    }
    

    public void deleteInfomation(Information information) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "DELETE FROM [dbo].[information]\n"
                    + "      WHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, information.getId());
            ps.executeUpdate();
            connection.close();
        } catch (SQLException e) {
        }
    }

    public String getAvatar(int id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select avatar FROM [dbo].[information] wHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("avatar");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return "";
    }

}
