/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Course;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Luu Duc Thang
 */
public class CourseAccountDAO {

    public int lesson_unlock(int courseID, int accountID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = " select lesson_unlock from Account_Course where account_id = ? and course_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, courseID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("lesson_unlock");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return 0;
    }

    public int countResCourse(int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select count(Account_id) as total,b.title from Account_Course as a left join"
                    + " Course as b on a.Course_id = b.id left join "
                    + "Account as c on a.Account_id=c.id where a.Course_id = ? and c.role_id =1 group by b.title";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return 0;
    }

    public boolean checkExistCourse(int accountID, int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [Account_id],[Course_id]\n"
                    + "  FROM [dbo].[Account_Course]\n"
                    + "  where [Account_id] = ? and [Course_id] = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, courseID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            return false;
        }
        return false;
    }

    public boolean addCourseAccount(int accountID, int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Account_Course]\n"
                    + "           ([Account_id],[Course_id])\n"
                    + "     VALUES\n"
                    + "           (?,?) ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, courseID);
            ps.execute();
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    public HashMap<Course, Integer> getAllCourseByAccountRes(int top) {
        HashMap<Course, Integer> listCoursePopular = new HashMap<Course, Integer>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select top " + top + " count(c.account_id) as total,b.course_id, b.course_title,b.course_description,b.price,b.course_image,b.date_create_course,b.course_status\n"
                    + "from Account_Course as a left join Course as b on a.course_id = b.course_id left join \n"
                    + "Account as c on a.account_id=c.account_id where c.role_id = 3 group by b.course_id, b.course_title,\n"
                    + "b.course_description,b.price,b.course_image,b.date_create_course,b.course_status \n"
                    + "order by count(c.account_id) desc";
            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, top);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(new CourseDAO().getRequiment(rs.getInt("course_id"), connection));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setStatus(rs.getBoolean("course_status"));
                listCoursePopular.put(course, rs.getInt("total"));
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCoursePopular;
    }

    public HashMap<Course, Integer> getAllExpertByAccountRes(int top) {
        HashMap<Course, Integer> listCoursePopular = new HashMap<Course, Integer>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select top " + top + " \n"
                    + "count(c.account_id) as total,d.full_name,d.display_name,d.infor_image,d.phone,d.address,b.course_title\n"
                    + "from Account_Course as a left join Course as b on a.course_id = b.course_id left join\n"
                    + "Account as c on a.account_id=c.account_id join Information as d on c.account_id = d.infor_id where c.role_id = 3\n"
                    + "group by d.full_name,d.display_name,d.infor_image,d.phone,d.address,b.course_title\n"
                    + "order by count(c.account_id) desc";
            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, top);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(new CourseDAO().getRequiment(rs.getInt("course_id"), connection));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setStatus(rs.getBoolean("course_status"));
                listCoursePopular.put(course, rs.getInt("total"));
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCoursePopular;
    }

    public int updateUnlock_Lesson(int accountID, int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = " update Account_Course set lesson_unlock = lesson_unlock + 1 where account_id = ? and course_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, courseID);
            return ps.executeUpdate();
        } catch (SQLException e) {
        }
        return 0;
    }

    public static void main(String[] args) {
        CourseAccountDAO c = new CourseAccountDAO();
        c.updateUnlock_Lesson(4, 1);
    }

}
