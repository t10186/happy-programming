/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Blogs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class BlogsDAO {

    public ArrayList<Blogs> getAllBlogsSearch(String keyword) {
        ArrayList<Blogs> listBlogs = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select * from blogs where title like ? or\n"
                    + "category like ? or detail like ? or\n"
                    + "date_create_blog like ? and blog_status =1";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + keyword + "%");
            ps.setString(2, "%" + keyword + "%");
            ps.setString(3, "%" + keyword + "%");
            ps.setString(4, "%" + keyword + "%");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blogs blogs = new Blogs();
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));

                listBlogs.add(blogs);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listBlogs;
    }

    public ArrayList<Blogs> getTop4AllBlogs(int top) {
        ArrayList<Blogs> listBlogs = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select top "+top+" * from Blogs order by date_create_blog desc";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blogs blogs = new Blogs();
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));
                listBlogs.add(blogs);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listBlogs;
    }

    public static void main(String[] args) {
        System.out.println(new BlogsDAO().getTop4AllBlogs(4));
    }
    
    public ArrayList<Blogs> getAllBlogs() {
        ArrayList<Blogs> listBlogs = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select * from Blogs order by timeCreate desc";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blogs blogs = new Blogs();
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));
                listBlogs.add(blogs);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listBlogs;
    }

    public Blogs getBlogsById(int id) {
        Blogs blogs = new Blogs();

        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "/****** Script for SelectTopNRows command from SSMS  ******/\n"
                    + "SELECT  [id]\n"
                    + "      ,[account_id]\n"
                    + "      ,[title]\n"
                    + "      ,[detail]\n"
                    + "      ,[category]\n"
                    + "      ,[thumbnail]\n"
                    + "      ,[date_create_blog]\n"
                    + "      ,[blog_status]\n"
                    + "  FROM [OnlineLearning].[dbo].[Blogs] \n"
                    + "  where id = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));

            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return blogs;
    }

    public ArrayList getListBlogsById(int id) {
        Blogs blogs = new Blogs();
        ArrayList<Blogs> list = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "/****** Script for SelectTopNRows command from SSMS  ******/\n"
                    + "SELECT  [id]\n"
                    + "      ,[account_id]\n"
                    + "      ,[title]\n"
                    + "      ,[detail]\n"
                    + "      ,[category]\n"
                    + "      ,[thumbnail]\n"
                    + "      ,[date_create_blog]\n"
                    + "      ,[blog_status]\n"
                    + "  FROM [OnlineLearning].[dbo].[Blogs] \n"
                    + "  where account_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));
                list.add(blogs);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return list;
    }

    public ArrayList<Blogs> getBlogsWithPagging(int page, int PAGE_SIZE) {
        ArrayList<Blogs> list = new ArrayList<>();
        try {
            String sql = "select *  from Blogs  where blog_status =1 order by date_create_blog desc\n"
                    + "offset (?-1)*? row fetch next ? rows only";

            Connection connection = new BaseDAO().getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, page);
            ps.setInt(2, PAGE_SIZE);
            ps.setInt(3, PAGE_SIZE);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blogs blogs = new Blogs();
                blogs.setId(rs.getString("id"));
                blogs.setAccount_id(rs.getString("Account_id"));
                blogs.setDetail(rs.getString("detail"));
                blogs.setTimeCreate(rs.getDate("date_create_blog"));
                blogs.setTitle(rs.getString("title"));
                blogs.setThumbnail(rs.getString("thumbnail"));
                blogs.setCategory(rs.getString("category"));
                blogs.setStatus(rs.getInt("blog_status"));
                list.add(blogs);

            }
            connection.close();
        } catch (Exception ex) {
        }
        return list;
    }

    public int getTotalBlogs() {
        try {
            String sql = "select count(id) from blogs where blog_status =1 ";

            Connection connection = new BaseDAO().getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public void insert(Blogs blogs) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Blogs]\n"
                    + "           ([account_id]\n"
                    + "           ,[title]\n"
                    + "           ,[detail]\n"
                    + "           ,[category]\n"
                    + "           ,[thumbnail]\n"
                    + "           ,[date_create_blog]\n"
                    + "           ,[blog_status])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,getdate()\n"
                    + "           ,1)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, blogs.Account_id);
            ps.setString(2, blogs.title);
            ps.setString(3, blogs.detail);
            ps.setString(4, blogs.category);
            ps.setString(5, blogs.thumbnail);

            ps.execute();
            connection.close();

        } catch (SQLException e) {

        }

    }

    public void changeStatus(int idblog, int statusUpdate) {

        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "update Blogs set blog_status=? where id=?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusUpdate);
            ps.setInt(2, idblog);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
        }

    }

    public void changeDetailBlog(Blogs blog, int id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "UPDATE [dbo].[Blogs]\n"
                    + "   SET \n"
                    + "		[title] = ?\n"
                    + "      ,[detail] = ?\n"
                    + "      ,[category] = ?\n"
                    + "      ,[thumbnail] = ?\n"
                    + "      ,[date_create_blog] = getdate()\n"
                    + "      \n"
                    + " WHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, blog.title);
            ps.setString(2, blog.detail);
            ps.setString(3, blog.category);
            ps.setString(4, blog.thumbnail);
            ps.setInt(5, id);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException ex) {
        }

    }
}
