/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Course;
import entity.Requiment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class CourseDAO {
    
     public int total_Lesson(int courseID) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = " select count(lesson_id) as total from lesson as a left join Course as b on "
                    + "a.course_id = b.course_id where b.course_id = ? group by b.course_id,b.course_title";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();
        } catch (SQLException e) {
        }
        return 0;
    }
     
     public static void main(String[] args) {
         double a=1.0;
         double b=5.0;
         double c =Math.round((a / b) * 100);
         System.out.println(c);
    }

    public ArrayList<Course> getAllByID(int accountID, int index, int numberRows, String columnSreach, String filter) {
        Connection connection = new BaseDAO().getConnection();
        ArrayList<Course> listCourse = new ArrayList<>();
        try {
            String search = "";
            if (!columnSreach.isEmpty()) {
                search = " and (" + columnSreach + ")";
            }
            String sql = "SELECT [Course].[course_id],[course_title],[course_description]\n"
                    + "      ,[price],[course_image],[date_create_course],[course_status] \n"
                    + "	   FROM [dbo].[Course] left join [Account_Course] "
                    + " on [Course].[course_id] = [Account_Course].course_id\n"
                    + "	   where [Account_Course].[account_id] = ? "
                    + search + "order by [course_id]\n"
                    + "OFFSET  ? ROWS\n"
                    + "FETCH NEXT ? ROWS ONLY";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ps.setInt(2, (index - 1) * numberRows);
            ps.setInt(3, numberRows);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setProcess(getProcess(connection, course.getId(), accountID));
                if (null == filter) {
                    listCourse.add(course);
                } else {
                    switch (filter) {
                        case "done":
                            if (course.getProcess() == 100) {
                                listCourse.add(course);
                            }
                            break;
                        case "not":
                            if (course.getProcess() < 100) {
                                listCourse.add(course);
                            }
                            break;
                        default:
                            listCourse.add(course);
                            break;
                    }
                }

            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCourse;
    }

    private int getProcess(Connection connection, int courseID, int accountID) {
        try {
            String sql = "select top 1 convert(int,(lesson_unlock*100)/lesson_unit) as process from lesson left join "
                    + "Course on lesson.course_id = Course.course_id left join\n"
                    + "Account_Course on Course.course_id = Account_Course.course_id where Course.course_id = ? and "
                    + "Account_Course.account_id = ? order by lesson_unit desc";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ps.setInt(2, accountID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("process");
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public int countCourseByID(int accountID, String columnSreach) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String search = "";
            if (!columnSreach.isEmpty()) {
                search = " and (" + columnSreach + ")";
            }
            String sql = "SELECT count([Course].[course_id]) as total\n"
                    + "	   FROM [dbo].[Course] left join [Account_Course] "
                    + " on [Course].[course_id] = [Account_Course].course_id\n"
                    + "	   where [Account_Course].[account_id] = ? " + search;
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();
        } catch (SQLException e) {
            return 0;
        }
        return 0;
    }

    public Course getCourseByID(int courseID) {
        Connection connection = new BaseDAO().getConnection();
        Course course = new Course();
        try {
            String sql = "SELECT [course_id],[course_title],[course_description]\n"
                    + "     ,[price],[course_image],[date_create_course],[course_status] \n"
                    + "	   FROM [dbo].[Course]\n"
                    + "	   where [course_id] = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return course;
    }

    public ArrayList<Course> getAllCourseOfDevoloper(int accID, int roleID) {
        ArrayList<Course> listCourse = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "";
            if (roleID == 2) {
                sql = "SELECT [course_id],[course_title],[course_description]\n"
                        + "  ,[price],[course_image],[date_create_course],[course_status]\n"
                        + "  FROM [dbo].[Course]";
            } else if (roleID == 3) {
                sql = "SELECT [Course].[course_id],[course_title],[course_description]\n"
                        + "  ,[price],[course_image],[date_create_course],[course_status]\n"
                        + "  FROM [dbo].[Course] right join [dbo].[Account_Course] \n"
                        + "  on [Course].[course_id] =  [Account_Course].[course_id]\n"
                        + "  where [Account_Course].[account_id] = ?";
            }
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                course.setStatus(rs.getBoolean("course_status"));
                listCourse.add(course);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCourse;
    }

    public ArrayList<Course> getAllCourseOfDevoloper(int accID, int roleID, int index, int numberRows, String columnSreach) {
        ArrayList<Course> listCourse = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String search = "";
            if (!columnSreach.isEmpty()) {
                if (roleID == 2) {
                    search = " where " + columnSreach;
                } else {
                    search = " and(" + columnSreach + ")";
                }
            }
            String sql = "";
            if (roleID == 2) {
                sql = "SELECT [course_id],[course_title],[course_description]\n"
                        + "  ,[price],[course_image],[date_create_course],[course_status]\n"
                        + "  FROM [dbo].[Course]" + search
                        + "order by [Course].[course_id]\n"
                        + "OFFSET  ? ROWS\n"
                        + "FETCH NEXT ? ROWS ONLY";
            } else if (roleID == 3) {
                sql = "SELECT [Course].[course_id],[course_title],[course_description]\n"
                        + "  ,[price],[course_image],[date_create_course],[course_status]\n"
                        + "  FROM [dbo].[Course] right join [dbo].[Account_Course] \n"
                        + "  on [Course].[course_id] =  [Account_Course].[course_id]\n"
                        + "  where [Account_Course].[account_id] = ?" + search
                        + "order by [Course].[course_id]\n"
                        + "OFFSET  ? ROWS\n"
                        + "FETCH NEXT ? ROWS ONLY";
            }
            PreparedStatement ps = connection.prepareStatement(sql);
            if (roleID == 3) {
                ps.setInt(1, accID);
                ps.setInt(2, (index - 1) * numberRows);
                ps.setInt(3, numberRows);
            } else {
                ps.setInt(1, (index - 1) * numberRows);
                ps.setInt(2, numberRows);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                course.setStatus(rs.getBoolean("course_status"));
                listCourse.add(course);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCourse;
    }

    public int countCourseDevoloper(int accID, int roleID, String columnSreach) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "";
            String search = "";
            if (!columnSreach.isEmpty()) {
                if (roleID == 2) {
                    search = " where " + columnSreach;
                } else {
                    search = " and( " + columnSreach + " )";
                }
            }
            if (roleID == 2) {
                sql = "select count(course_id) as total from Course" + search;
            } else if (roleID == 3) {
                sql = "select count(Account_Course.course_id) as total \n"
                        + "from Course left join Account_Course \n"
                        + "on Course.course_id = Account_Course.course_id where Account_Course.account_id = ?" + search;
            }
            PreparedStatement ps = connection.prepareStatement(sql);
            if (roleID == 3) {
                ps.setInt(1, accID);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();
        } catch (SQLException e) {
            return 0;
        }
        return 0;
    }

    public ArrayList<Course> getAllCourseActive() {
        ArrayList<Course> listCourse = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [course_id],[course_title],[course_description],[requirement_id]\n"
                    + "      ,[price],[course_image],[date_create_course],[course_status] \n"
                    + "	   FROM [dbo].[Course]\n"
                    + "	   where [course_status] = 'true' ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(getRequiment(rs.getInt("requirement_id"), connection));
                listCourse.add(course);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCourse;
    }

    public void deteleCourse(int id) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "DELETE FROM [dbo].[Course]\n"
                    + " WHERE course_id  = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            connection.close();
        } catch (SQLException e) {
        }
    }

    public boolean insert(Course course, String emailDevolop, String[] arrRequirement) {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "INSERT INTO [dbo].[Course]\n"
                    + "   ([course_title],[course_description],[price],[course_image])\n"
                    + "     VALUES(?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, course.getTitle());
            ps.setString(2, course.getDescription());
            ps.setInt(3, course.getPrice());
            ps.setString(4, course.getImageCourse());
            ps.execute();
            int courseID = getLastCourseID(connection);
            insertRequiment(connection, arrRequirement,courseID);
            new CourseAccountDAO().addCourseAccount(emailDevolop, courseID);
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private boolean insertRequiment(Connection connection, String[] arrRequirement,int courseID) {
        try {
            String sql = "INSERT INTO [dbo].[Requirement]\n"
                    + "           ([course_id],[content])\n"
                    + "     VALUES\n"
                    + "           (?,?)"; 
            for (int i = 1; i < arrRequirement.length; i++) {
                sql += ",(?,?)";
            }
            PreparedStatement ps = connection.prepareStatement(sql);
            int j = 1;
            for (int i = 0; i < arrRequirement.length; i++) {
                ps.setInt(j, courseID);
                j++;
                ps.setString(j, arrRequirement[i]);
                j++;
            }
            ps.execute();
            connection.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        CourseDAO c = new CourseDAO();
        Connection connection = new BaseDAO().getConnection();
        System.out.println(c.getLastCourseID(connection));
    }

    private int getLastCourseID(Connection connection) {
        try {
            String sql = "SELECT top 1 [course_id] \n"
                    + "  FROM [dbo].[Course] order by course_id desc";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("course_id");
            }
        } catch (SQLException e) {
            return 0;
        }
        return 0;
    }
    

    public int updaterq(int courseID) {
        Connection connection = new BaseDAO().getConnection();

        try {
            String sql = "UPDATE [OnlineLearning].[dbo].[Course]\n"
                    + "   SET \n"
                    + "      [approve] = '1'\n"
                    + " WHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            ps.execute();
            return 1;
        } catch (SQLException e) {
        }
        return 0;
    }

    public int deleterq(int courseID) {
        Connection connection = new BaseDAO().getConnection();

        try {
            String sqlPre = "DELETE FROM [dbo].[Account_Course]\n"
                    + " WHERE Course_id = ?";
            String sql = "delete from Course where id = ?";
            PreparedStatement psPre = connection.prepareStatement(sqlPre);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, courseID);
            psPre.setInt(1, courseID);
            psPre.execute();
            ps.execute();
            return 1;
        } catch (SQLException e) {
        }
        return 0;
    }

    public ArrayList<Course> getCourse(int index, int numberRows, String columnSreach) {
        ArrayList<Course> listCourse = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();

        try {
            String search = "";
            if (!columnSreach.isEmpty()) {
                search = " and " + columnSreach;
            }
            String sql = "select [course_id],[course_title],[course_description]\n"
                    + ",[price],[course_image],[date_create_course]\n"
                    + "from [dbo].[course] where [course_status] = 'true'\n"
                    + search + "order by [course_id]\n"
                    + "OFFSET  ? ROWS\n"
                    + "FETCH NEXT ? ROWS ONLY";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * numberRows);
            ps.setInt(2, numberRows);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                listCourse.add(course);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCourse;
    }

    protected ArrayList<Requiment> getRequiment(int requimentID, Connection conneciton) {
        ArrayList<Requiment> listRequiments = new ArrayList<>();
        try {
            String sql = "SELECT [course_id]\n"
                    + "      ,[content]\n"
                    + "  FROM [dbo].[Requirement] where [course_id] = ?";
            PreparedStatement ps = conneciton.prepareStatement(sql);
            ps.setInt(1, requimentID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Requiment requiment = new Requiment();
                requiment.setId(requimentID);
                requiment.setContent(rs.getString("content"));
                listRequiments.add(requiment);
            }
        } catch (SQLException e) {
            return null;
        }
        return listRequiments;
    }

    public int countCourse() {
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select count(*) from  [dbo].[course] where [course_status] = 'true'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            connection.close();
        } catch (SQLException ex) {
        }
        return 0;
    }

    public ArrayList<Course> getTop4Course(int top) {
        ArrayList<Course> courseLists = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = " SELECT top "+top+" [course_id],[course_title],[course_description]\n"
                    + ",[price] ,[course_image],[date_create_course]\n"
                    + ",[course_status] FROM [dbo].[Course] where [course_status]='true' order by course_id desc";
            PreparedStatement ps = connection.prepareStatement(sql);
//            ps.setInt(1, top);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Course course = new Course();
                course.setId(rs.getInt("course_id"));
                course.setTitle(rs.getString("course_title"));
                course.setDescription(rs.getString("course_description"));
                course.setRequirement(getRequiment(rs.getInt("course_id"), connection));
                course.setPrice(rs.getInt("price"));
                course.setImageCourse(rs.getString("course_image"));
                course.setDateCreateCourse(rs.getDate("date_create_course"));
                course.setStatus(rs.getBoolean("course_status"));
                courseLists.add(course);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return courseLists;
    }
    


}
