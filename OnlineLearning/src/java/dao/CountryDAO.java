/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Country;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class CountryDAO {

    public ArrayList<Country> getAllCountry() {
        ArrayList<Country> listCountry = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [country_id]\n"
                    + "      ,[country_name]\n"
                    + "  FROM [dbo].[Country]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Country country = new Country();
                country.setId(rs.getInt("country_id"));
                country.setName(rs.getString("country_name"));
                listCountry.add(country);
            }
            connection.close();
        } catch (SQLException e) {
            return null;
        }
        return listCountry;
    }
}
