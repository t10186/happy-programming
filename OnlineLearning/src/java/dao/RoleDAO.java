/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class RoleDAO {

    public ArrayList<Role> getAll() {
        ArrayList<Role> listRole = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "SELECT [role_id]\n"
                    + "  ,[role_name]\n"
                    + "  FROM [dbo].[Role]";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Role role = new Role();
                role.setId(rs.getInt("role_id"));
                role.setName(rs.getString("role_name"));
                listRole.add(role);
            }
            connection.close();
        } catch (SQLException e) {
        }
        return listRole;
    }

}
