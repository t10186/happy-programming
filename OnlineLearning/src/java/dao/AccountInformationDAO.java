/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import entity.Account;
import entity.AccountInformation;
import entity.Country;
import entity.Information;
import entity.Role;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Luu Duc Thang
 */
public class AccountInformationDAO {

    public ArrayList<AccountInformation> getAll(int index, int numberRows,String columnSreach) {
        ArrayList<AccountInformation> listAllaccInfor = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
             String search = "";
            if (!columnSreach.isEmpty()) {
                search = " and (" + columnSreach + ")";
            }
            String sql = "select [account_id],[Account].[role_id],[role_name],[username],[country_name],[DOB]\n"
                    + ",[email],[date_register],[full_name],[phone],[address],[infor_image]\n"
                    + ",[gender] FROM [dbo].[Account] left join [dbo].[Information] \n"
                    + "on [Account].[account_id] = [Information].[infor_id] \n"
                    + "left join [dbo].[Role] on [Account].[role_id] = [Role].[role_id] \n"
                    + "left join [dbo].[Country] on [Country].[country_id] = [Information].[country_id] " 
                    +" where [Account].[role_id] != 2 "+search 
                    + "order by [Account].[account_id]\n"
                    + "OFFSET  ? ROWS\n"
                    + "FETCH NEXT ? ROWS ONLY";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * numberRows);
            ps.setInt(2, numberRows);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountInformation accInfor = new AccountInformation();
                Account acc = new Account();
                Role role = new Role();
                Country country = new Country();
                Information infor = new Information();
                acc.setId(rs.getInt("account_id"));
                acc.setUsername(rs.getString("username"));
                acc.setEmail(rs.getString("email"));
                acc.setRoleID(rs.getInt("role_id"));
                acc.setDateRegister(rs.getDate("date_register"));
                infor.setFullName(rs.getString("full_name"));
                infor.setPhone(rs.getInt("phone"));
                infor.setAddress(rs.getString("address"));
                infor.setDOB(rs.getDate("DOB"));
                infor.setGender(rs.getString("gender"));
                infor.setAvatar(rs.getString("infor_image"));
                role.setName(rs.getString("role_name"));
                country.setName(rs.getString("country_name"));
                accInfor.setAccount(acc);
                accInfor.setInformation(infor);
                accInfor.setRole(role);
                accInfor.setCountry(country);
                listAllaccInfor.add(accInfor);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listAllaccInfor;
    }
    
    
    public int countUser(String columnSreach){
        Connection connection = new BaseDAO().getConnection();
        try {
            String search = "";
            if (!columnSreach.isEmpty()) {
                search = " and (" + columnSreach + ")";
            }
            String sql = "select count(account_id) as total from Account" + search;
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("total");
            }
            connection.close();

        } catch (SQLException e) {
            return 0;
        }
        return 0;
    }
    public static void main(String[] args) {
        AccountInformationDAO a = new AccountInformationDAO();
        System.out.println(a.getAll(1, 3, ""));
    }


    public ArrayList<AccountInformation> getAllExpert() {
        ArrayList<AccountInformation> listAllaccInfor = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select Account.id , Account.role_id ,information.country_id,information.displayname,information.full_name,\n"
                    + "information.address, information.phone , information.gender,information.DOB,information.avatar from information \n"
                    + "right JOIN Account on information.id = Account.id left JOIN  role on role.id = Account.role_id \n"
                    + "left join country on country.id = information.country_id  where   Account.role_id = 3 ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountInformation accInfor = new AccountInformation();
                Account acc = new Account();
                Information infor = new Information();
                Role role = new Role();

                // set id
                acc.setId(rs.getInt("id"));
                infor.setId(rs.getInt("id"));
                role.setId(rs.getInt("role_id"));

                // information
                infor.setDisplayName(rs.getString("displayname"));
                infor.setFullName(rs.getString("full_name"));
                infor.setAddress(rs.getString("address"));
                infor.setPhone(rs.getInt("phone"));
                infor.setGender(rs.getString("gender"));
                infor.setDOB(rs.getDate("DOB"));
                infor.setAvatar(rs.getString("avatar"));

                accInfor.setAccount(acc);
                accInfor.setInformation(infor);
                accInfor.setRole(role);
                listAllaccInfor.add(accInfor);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listAllaccInfor;
    }

    public ArrayList<AccountInformation> getAllAccountSearch(String keyword) {
        ArrayList<AccountInformation> listAllaccInfor = new ArrayList<>();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select Account.id , Account.role_id , Account.username , Account.password ,\n"
                    + "                    Account.email,information.country_id,information.displayname\n"
                    + "                 ,information.full_name,information.address, information.phone , information.gender,\n"
                    + "                    information.DOB,information.avatar, role.name as name_role,country.name as name_country \n"
                    + "                   from information  right JOIN Account on information.id = Account.id \n"
                    + "                    left JOIN  role on role.id = Account.role_id\n"
                    + "                    left join country on country.id = information.country_id where Account.username like ? or information.full_name like ? or\n"
                    + "					information.address like ? or information.phone like ? or Account.email like ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + keyword + "%");
            ps.setString(2, "%" + keyword + "%");
            ps.setString(3, "%" + keyword + "%");
            ps.setString(4, "%" + keyword + "%");
            ps.setString(5, "%" + keyword + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AccountInformation accInfor = new AccountInformation();
                Account acc = new Account();
                Information infor = new Information();
                Role role = new Role();
                Country country = new Country();

                // set id
                acc.setId(rs.getInt("id"));
                infor.setId(rs.getInt("id"));
                role.setId(rs.getInt("role_id"));
                country.setId(rs.getInt("country_id"));

                // data
                // account
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                // information

                infor.setDisplayName(rs.getString("displayname"));
                infor.setFullName(rs.getString("full_name"));
                infor.setAddress(rs.getString("address"));
                infor.setPhone(rs.getInt("phone"));
                infor.setGender(rs.getString("gender"));
                infor.setDOB(rs.getDate("DOB"));
                infor.setAvatar(rs.getString("avatar"));

                // role
                role.setName(rs.getString("name_role"));

                // country
                country.setName(rs.getString("name_country"));

                accInfor.setAccount(acc);
                accInfor.setInformation(infor);
                accInfor.setRole(role);
                accInfor.setCountry(country);

                listAllaccInfor.add(accInfor);
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return listAllaccInfor;
    }

    public AccountInformation getbyId(int id) {
        AccountInformation accInfor = new AccountInformation();
        Connection connection = new BaseDAO().getConnection();
        try {
            String sql = "select Account.id , Account.role_id , Account.username , Account.password , \n"
                    + "Account.email,information.country_id,information.displayname\n"
                    + ",information.full_name,information.address, information.phone , information.gender,\n"
                    + "information.DOB,information.avatar, role.name as name_role,country.name as name_country \n"
                    + "from information  right JOIN Account on information.id = Account.id \n"
                    + "left JOIN  role on role.id = Account.role_id\n"
                    + "left join country on country.id = information.country_id where Account.id =?  ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                Account acc = new Account();
                Information infor = new Information();
                Role role = new Role();
                Country country = new Country();

                // set id
                acc.setId(rs.getInt("id"));
                infor.setId(rs.getInt("id"));
                role.setId(rs.getInt("role_id"));
                country.setId(rs.getInt("country_id"));

                // data
                // account
                acc.setUsername(rs.getString("username"));
                acc.setPassword(rs.getString("password"));
                acc.setEmail(rs.getString("email"));
                // information

                infor.setDisplayName(rs.getString("displayname"));
                infor.setFullName(rs.getString("full_name"));
                infor.setAddress(rs.getString("address"));
                infor.setPhone(rs.getInt("phone"));
                infor.setGender(rs.getString("gender"));
                infor.setDOB(rs.getDate("DOB"));
                infor.setAvatar(rs.getString("avatar"));

                // role
                role.setName(rs.getString("name_role"));

                // country
                country.setName(rs.getString("name_country"));

                accInfor.setAccount(acc);
                accInfor.setInformation(infor);
                accInfor.setRole(role);
                accInfor.setCountry(country);

                return accInfor;
            }
            connection.close();

        } catch (SQLException e) {
            return null;
        }
        return accInfor;
    }
    //test connection 

}
