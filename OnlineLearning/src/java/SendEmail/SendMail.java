/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SendEmail;

import entity.User;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

/**
 *
 * @author Admin
 */
public class SendMail {
    
    public boolean sendVerify(User user) {
        boolean test = false;
        String toEmail = user.getEmail();
        try {
            //Creating properties
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Utils.EMAIL, Utils.PASSWORD);
                }
            });

            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(session);
            //Setting sender address
            mm.setFrom(new InternetAddress(Utils.EMAIL));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
            //Adding subject
            mm.setSubject("Verify your email");
            //Adding message
            mm.setText("Successful! Verify your email to login" +user.getCode());
            //Sending email
            Transport.send(mm);
            test = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return test;

    }

    public String getRandom() {
       Random rnd = new Random();
       int number = rnd.nextInt(9999);
       return String.format("%06d", number);
    }
    
     public static void sendmail(String email, String subject, String text) {
        try {
            //Creating properties
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");
            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(Utils.EMAIL, Utils.PASSWORD);
                }
            });

            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(session);
            //Setting sender address
            mm.setFrom(new InternetAddress(Utils.EMAIL));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            //Adding subject
            mm.setSubject(subject);
            //Adding message
            mm.setText(text);
            //Sending email
            Transport.send(mm);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
