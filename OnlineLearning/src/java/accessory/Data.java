/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accessory;

import dao.AccountDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class Data {

    private ArrayList<Integer> dataPhase(Date dateLast) {
        int numberPhase = 12;
        Date date = new Date();
        long diff = date.getTime() - dateLast.getTime();
        long day = (diff / (1000 * 60 * 60 * 24));
        int diffMonth = (int) (day / 365 + 1);
        AccountDAO accDAO = new AccountDAO();
        ArrayList<Integer> list = accDAO.getDataAccRegis(diffMonth, dateLast);
        return list;
    }

    public ArrayList<Integer> getPhaseAccRegis() {
        AccountDAO accDAO = new AccountDAO();
        return dataPhase(accDAO.getLastDateRegis());
    }

    public ArrayList<Integer> getPhaseAccRegisThisYear(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int year = cal.get(Calendar.YEAR);
        String date = year + "-01-01";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStart = null;
        try {
            dateStart = formatter.parse(date); 
        } catch (ParseException e) {
        }
        
        return dataPhase(dateStart);
    }
    
}
