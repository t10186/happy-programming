/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accessory;

import dao.AccountDAO;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Luu Duc Thang
 */
public class Phase {

    private ArrayList<String> divisionPhase(Date dateLast) {
        String[] arrList = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        // number phase;
        int numberPhase = 12;
        Date date = new Date();
        long diff = date.getTime() - dateLast.getTime();
        long day = (diff / (1000 * 60 * 60 * 24));
        int diffMonth = (int) (day / 365 + 1);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateLast);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < numberPhase; i++) {
            list.add(arrList[month] + year);
            month = month + diffMonth;
            if (month >= 12) {
                month = month % 12;
                year++;
            }
        }
        return list;
    }

    public ArrayList<String> getPhaseAccRegis() {
        AccountDAO accDAO = new AccountDAO();
        return divisionPhase(accDAO.getLastDateRegis());
    }

    public ArrayList<String> getPhaseCourRegis() {
        AccountDAO accDAO = new AccountDAO();
        return divisionPhase(accDAO.getLastDateRegis()); 
    }
}
