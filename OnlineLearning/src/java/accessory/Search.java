/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accessory;

import java.util.regex.Pattern;

/**
 *
 * @author Luu Duc Thang
 */
public class Search {
    public String getSearch(String[] nameColumn, String keySearch) {
        String result = "";
        System.out.println(keySearch);
        for (int i = 0; i < nameColumn.length; i++) {
            result += nameColumn[i] + " like "+"'%"+keySearch+"%'";
            if(nameColumn.length - 1 != i){
                result += " or ";
            }
        }
        return result;
    }
}
